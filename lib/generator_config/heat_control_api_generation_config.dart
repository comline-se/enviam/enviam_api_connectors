import 'package:openapi_generator_annotations/openapi_generator_annotations.dart';

@Openapi(
    additionalProperties: AdditionalProperties(
        pubName: 'heat_control_api',
        pubAuthor: 'emt@kiwigrid.com',
        pubVersion: '0.0.1'),
    inputSpecFile: 'lib/generator_config/heat-control-openapi-spec.yaml',
    alwaysRun: false,
    generatorName: Generator.dart,
    outputDirectory: 'lib/heat_control_api')
class HeatControlOpenApiConfig extends OpenapiGeneratorConfig {}
