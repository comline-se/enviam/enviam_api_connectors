import 'package:openapi_generator_annotations/openapi_generator_annotations.dart';

@Openapi(
    additionalProperties: AdditionalProperties(
        pubName: 'enviam_backend_api',
        pubAuthor: 'Gerd.Szelig@gisa.de',
        pubVersion: '0.0.1'),
    inputSpecFile: 'lib/generator_config/enviam-backend-openapi-spec.yaml',
    alwaysRun: false,
    generatorName: Generator.dart,
    outputDirectory: 'lib/enviam_backend_api')
class EnviamBackendOpenApiConfig extends OpenapiGeneratorConfig {}
