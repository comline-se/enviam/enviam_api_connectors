import 'package:openapi_generator_annotations/openapi_generator_annotations.dart';

@Openapi(
    additionalProperties: AdditionalProperties(
        pubName: 'hems_api',
        pubAuthor: 'team-homer@kiwigrid.com',
        pubVersion: '0.0.1'),
    inputSpecFile: 'lib/generator_config/hems-openapi-spec.yaml',
    alwaysRun: false,
    generatorName: Generator.dart,
    outputDirectory: 'lib/hems_api')
class HeatControlOpenApiConfig extends OpenapiGeneratorConfig {}
