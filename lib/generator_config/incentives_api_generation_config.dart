import 'package:openapi_generator_annotations/openapi_generator_annotations.dart';

@Openapi(
    additionalProperties: AdditionalProperties(
        pubName: 'incentives_api',
        pubAuthor: 'team-homer@kiwigrid.com',
        pubVersion: '0.0.1'),
    inputSpecFile: 'lib/generator_config/incentives_openapi-spec.yaml',
    alwaysRun: false,
    generatorName: Generator.dart,
    outputDirectory: 'lib/incentives_api')
class HeatControlOpenApiConfig extends OpenapiGeneratorConfig {}
