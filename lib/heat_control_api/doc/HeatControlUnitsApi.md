# heat_control_api.api.HeatControlUnitsApi

## Load the API package
```dart
import 'package:heat_control_api/api.dart';
```

All URIs are relative to *http://localhost/api/user*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createHolidayProgramForControlUnit**](HeatControlUnitsApi.md#createholidayprogramforcontrolunit) | **POST** /heatControlUnits/{guid}/holidayPrograms | 
[**createTimeProgramForControlUnit**](HeatControlUnitsApi.md#createtimeprogramforcontrolunit) | **POST** /heatControlUnits/{guid}/timePrograms | 
[**deleteTimeProgramForControlUnit**](HeatControlUnitsApi.md#deletetimeprogramforcontrolunit) | **DELETE** /heatControlUnits/{guid}/timePrograms/{id} | 
[**getApartmentForControlUnit**](HeatControlUnitsApi.md#getapartmentforcontrolunit) | **GET** /heatControlUnits/{guid}/apartment | 
[**getControlUnitById**](HeatControlUnitsApi.md#getcontrolunitbyid) | **GET** /heatControlUnits/{guid} | get detailed information about the control unit
[**getControlUnitInfo**](HeatControlUnitsApi.md#getcontrolunitinfo) | **GET** /heatControlUnits/{guid}/info | 
[**getControlUnits**](HeatControlUnitsApi.md#getcontrolunits) | **GET** /heatControlUnits | get all control units for the given user
[**getDefaultTimeProgram**](HeatControlUnitsApi.md#getdefaulttimeprogram) | **GET** /heatControlUnits/{guid}/timePrograms/template | 
[**getHeatLevelMapping**](HeatControlUnitsApi.md#getheatlevelmapping) | **GET** /heatLevelMapping | get heat Level Mapping
[**getHolidayProgramForControlUnitById**](HeatControlUnitsApi.md#getholidayprogramforcontrolunitbyid) | **GET** /heatControlUnits/{guid}/holidayPrograms/{id} | 
[**getHolidayProgramsForControlUnit**](HeatControlUnitsApi.md#getholidayprogramsforcontrolunit) | **GET** /heatControlUnits/{guid}/holidayPrograms | 
[**getRoomsForControlUnit**](HeatControlUnitsApi.md#getroomsforcontrolunit) | **GET** /heatControlUnits/{guid}/rooms | 
[**getTimeProgramForControlUnitById**](HeatControlUnitsApi.md#gettimeprogramforcontrolunitbyid) | **GET** /heatControlUnits/{guid}/timePrograms/{id} | 
[**getTimeProgramsForControlUnit**](HeatControlUnitsApi.md#gettimeprogramsforcontrolunit) | **GET** /heatControlUnits/{guid}/timePrograms | 
[**setModeForRoom**](HeatControlUnitsApi.md#setmodeforroom) | **POST** /heatControlUnits/{guid}/rooms/{roomId}/mode/{mode} | 
[**setNameForRoom**](HeatControlUnitsApi.md#setnameforroom) | **POST** /heatControlUnits/{guid}/rooms/{roomId}/name | 
[**setOperationModeForApartment**](HeatControlUnitsApi.md#setoperationmodeforapartment) | **POST** /heatControlUnits/{guid}/apartment/mode/{mode} | 
[**setTimeProgramForApartment**](HeatControlUnitsApi.md#settimeprogramforapartment) | **POST** /heatControlUnits/{guid}/apartment/timeProgram/{timeProgramId} | 
[**setTimeProgramForRoom**](HeatControlUnitsApi.md#settimeprogramforroom) | **POST** /heatControlUnits/{guid}/rooms/{roomId}/timeProgram/{timeProgramId} | 
[**switchHeatControlOff**](HeatControlUnitsApi.md#switchheatcontroloff) | **GET** /heatControlUnits/{guid}/switchOff | 
[**switchHeatControlOn**](HeatControlUnitsApi.md#switchheatcontrolon) | **GET** /heatControlUnits/{guid}/switchOn | 
[**updateHolidayProgramForControlUnit**](HeatControlUnitsApi.md#updateholidayprogramforcontrolunit) | **PUT** /heatControlUnits/{guid}/holidayPrograms/{id} | 
[**updateTimeProgramForControlUnit**](HeatControlUnitsApi.md#updatetimeprogramforcontrolunit) | **PUT** /heatControlUnits/{guid}/timePrograms/{id} | 


# **createHolidayProgramForControlUnit**
> createHolidayProgramForControlUnit(guid, body)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();
final guid = guid_example; // String | ID of the control Unit
final body = HolidayProgram(); // HolidayProgram | body for the time program

try { 
    api_instance.createHolidayProgramForControlUnit(guid, body);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->createHolidayProgramForControlUnit: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **body** | [**HolidayProgram**](HolidayProgram.md)| body for the time program | 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createTimeProgramForControlUnit**
> createTimeProgramForControlUnit(guid, body)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();
final guid = guid_example; // String | ID of the control Unit
final body = TimeProgram(); // TimeProgram | body for the time program

try { 
    api_instance.createTimeProgramForControlUnit(guid, body);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->createTimeProgramForControlUnit: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **body** | [**TimeProgram**](TimeProgram.md)| body for the time program | 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteTimeProgramForControlUnit**
> deleteTimeProgramForControlUnit(guid, id)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();
final guid = guid_example; // String | ID of the control Unit
final id = 789; // int | ID of the time program

try { 
    api_instance.deleteTimeProgramForControlUnit(guid, id);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->deleteTimeProgramForControlUnit: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **id** | **int**| ID of the time program | 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getApartmentForControlUnit**
> Apartment getApartmentForControlUnit(guid)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();
final guid = guid_example; // String | ID of the control Unit

try { 
    final result = api_instance.getApartmentForControlUnit(guid);
    print(result);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->getApartmentForControlUnit: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 

### Return type

[**Apartment**](Apartment.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getControlUnitById**
> ControlUnit getControlUnitById(guid)

get detailed information about the control unit

### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();
final guid = guid_example; // String | ID of the control Unit

try { 
    final result = api_instance.getControlUnitById(guid);
    print(result);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->getControlUnitById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 

### Return type

[**ControlUnit**](ControlUnit.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getControlUnitInfo**
> ControlUnitInfo getControlUnitInfo(guid)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();
final guid = guid_example; // String | ID of the control Unit

try { 
    final result = api_instance.getControlUnitInfo(guid);
    print(result);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->getControlUnitInfo: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 

### Return type

[**ControlUnitInfo**](ControlUnitInfo.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getControlUnits**
> List<ControlUnit> getControlUnits()

get all control units for the given user

### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();

try { 
    final result = api_instance.getControlUnits();
    print(result);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->getControlUnits: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List<ControlUnit>**](ControlUnit.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getDefaultTimeProgram**
> TimeProgram getDefaultTimeProgram(guid)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();
final guid = guid_example; // String | ID of the control Unit

try { 
    final result = api_instance.getDefaultTimeProgram(guid);
    print(result);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->getDefaultTimeProgram: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 

### Return type

[**TimeProgram**](TimeProgram.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getHeatLevelMapping**
> List<HeatLevelMapping> getHeatLevelMapping()

get heat Level Mapping

### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();

try { 
    final result = api_instance.getHeatLevelMapping();
    print(result);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->getHeatLevelMapping: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List<HeatLevelMapping>**](HeatLevelMapping.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getHolidayProgramForControlUnitById**
> HolidayProgram getHolidayProgramForControlUnitById(guid, id)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();
final guid = guid_example; // String | ID of the control Unit
final id = 789; // int | ID of the holiday program

try { 
    final result = api_instance.getHolidayProgramForControlUnitById(guid, id);
    print(result);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->getHolidayProgramForControlUnitById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **id** | **int**| ID of the holiday program | 

### Return type

[**HolidayProgram**](HolidayProgram.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getHolidayProgramsForControlUnit**
> List<HolidayProgram> getHolidayProgramsForControlUnit(guid)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();
final guid = guid_example; // String | ID of the control Unit

try { 
    final result = api_instance.getHolidayProgramsForControlUnit(guid);
    print(result);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->getHolidayProgramsForControlUnit: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 

### Return type

[**List<HolidayProgram>**](HolidayProgram.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getRoomsForControlUnit**
> List<Room> getRoomsForControlUnit(guid)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();
final guid = guid_example; // String | ID of the control Unit

try { 
    final result = api_instance.getRoomsForControlUnit(guid);
    print(result);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->getRoomsForControlUnit: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 

### Return type

[**List<Room>**](Room.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getTimeProgramForControlUnitById**
> TimeProgram getTimeProgramForControlUnitById(guid, id)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();
final guid = guid_example; // String | ID of the control Unit
final id = 789; // int | ID of the time program

try { 
    final result = api_instance.getTimeProgramForControlUnitById(guid, id);
    print(result);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->getTimeProgramForControlUnitById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **id** | **int**| ID of the time program | 

### Return type

[**TimeProgram**](TimeProgram.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getTimeProgramsForControlUnit**
> List<TimeProgram> getTimeProgramsForControlUnit(guid)



Returns the time programs for the room with the given ID

### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();
final guid = guid_example; // String | ID of the control Unit

try { 
    final result = api_instance.getTimeProgramsForControlUnit(guid);
    print(result);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->getTimeProgramsForControlUnit: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 

### Return type

[**List<TimeProgram>**](TimeProgram.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **setModeForRoom**
> setModeForRoom(guid, roomId, mode, level)



Sets the operation mode for the room with the given ID

### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();
final guid = guid_example; // String | ID of the control Unit
final roomId = 56; // int | id of the room as represented on the device (0-15)
final mode = ; // OperationMode | mode to set
final level = 1.2; // double | heat level which is used if manual operation mode is selected

try { 
    api_instance.setModeForRoom(guid, roomId, mode, level);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->setModeForRoom: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **roomId** | **int**| id of the room as represented on the device (0-15) | 
 **mode** | [**OperationMode**](.md)| mode to set | 
 **level** | **double**| heat level which is used if manual operation mode is selected | [optional] 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **setNameForRoom**
> setNameForRoom(guid, roomId, setRoomNameRequestBody)



Sets the human readable name for the room with the given ID

### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();
final guid = guid_example; // String | ID of the control Unit
final roomId = 56; // int | id of the room as represented on the device (0-15)
final setRoomNameRequestBody = SetRoomNameRequestBody(); // SetRoomNameRequestBody | body for the name

try { 
    api_instance.setNameForRoom(guid, roomId, setRoomNameRequestBody);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->setNameForRoom: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **roomId** | **int**| id of the room as represented on the device (0-15) | 
 **setRoomNameRequestBody** | [**SetRoomNameRequestBody**](SetRoomNameRequestBody.md)| body for the name | 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **setOperationModeForApartment**
> setOperationModeForApartment(guid, mode, level)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();
final guid = guid_example; // String | ID of the control Unit
final mode = ; // OperationMode | active time program id
final level = 1.2; // double | heat level which is used if manual operation mode is selected

try { 
    api_instance.setOperationModeForApartment(guid, mode, level);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->setOperationModeForApartment: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **mode** | [**OperationMode**](.md)| active time program id | 
 **level** | **double**| heat level which is used if manual operation mode is selected | [optional] 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **setTimeProgramForApartment**
> setTimeProgramForApartment(guid, timeProgramId)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();
final guid = guid_example; // String | ID of the control Unit
final timeProgramId = 789; // int | active time program id

try { 
    api_instance.setTimeProgramForApartment(guid, timeProgramId);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->setTimeProgramForApartment: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **timeProgramId** | **int**| active time program id | 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **setTimeProgramForRoom**
> setTimeProgramForRoom(guid, roomId, timeProgramId)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();
final guid = guid_example; // String | ID of the control Unit
final roomId = 56; // int | id of the room as represented on the device (0-15)
final timeProgramId = 789; // int | active time program id

try { 
    api_instance.setTimeProgramForRoom(guid, roomId, timeProgramId);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->setTimeProgramForRoom: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **roomId** | **int**| id of the room as represented on the device (0-15) | 
 **timeProgramId** | **int**| active time program id | 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **switchHeatControlOff**
> switchHeatControlOff(guid)



Stores the current operation mode of room 0 and sets the operation mode of room 0 to \"OFF\". i.e. switches the heat control devices for the entire apartment off.

### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();
final guid = guid_example; // String | ID of the control Unit

try { 
    api_instance.switchHeatControlOff(guid);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->switchHeatControlOff: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **switchHeatControlOn**
> switchHeatControlOn(guid)



Restores the last operation mode for room 0. i.e. switches the heat control for the apartment back on.

### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();
final guid = guid_example; // String | ID of the control Unit

try { 
    api_instance.switchHeatControlOn(guid);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->switchHeatControlOn: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateHolidayProgramForControlUnit**
> updateHolidayProgramForControlUnit(guid, id, body)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();
final guid = guid_example; // String | ID of the control Unit
final id = 789; // int | ID of the holiday program
final body = HolidayProgram(); // HolidayProgram | body for the time program

try { 
    api_instance.updateHolidayProgramForControlUnit(guid, id, body);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->updateHolidayProgramForControlUnit: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **id** | **int**| ID of the holiday program | 
 **body** | [**HolidayProgram**](HolidayProgram.md)| body for the time program | 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateTimeProgramForControlUnit**
> updateTimeProgramForControlUnit(guid, id, body)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = HeatControlUnitsApi();
final guid = guid_example; // String | ID of the control Unit
final id = 789; // int | ID of the time program
final body = TimeProgram(); // TimeProgram | body for the time program

try { 
    api_instance.updateTimeProgramForControlUnit(guid, id, body);
} catch (e) {
    print('Exception when calling HeatControlUnitsApi->updateTimeProgramForControlUnit: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **id** | **int**| ID of the time program | 
 **body** | [**TimeProgram**](TimeProgram.md)| body for the time program | 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

