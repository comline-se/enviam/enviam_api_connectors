# heat_control_api.model.Room

## Load the model package
```dart
import 'package:heat_control_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **String** |  | 
**roomId** | **int** |  | 
**operationMode** | [**OperationMode**](OperationMode.md) |  | [optional] 
**heatLevel** | **double** |  | [optional] 
**effectiveHeatLevel** | **double** | The effective heat level represents the heat level that is actually in effect for the room. It can be the same as the heat level (if the room is in operation mode MANUELL for example), but in general it can differ from it. The effective heat level dependes on the operation mode of the room, if the current time falls into a holiday period, and also on the configuration of room 0 (if the room is in mode ZENTRAL). | [optional] 
**activeTimeProgramId** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


