# heat_control_api.model.ControlUnitInfo

## Load the model package
```dart
import 'package:heat_control_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**serial** | **String** |  | [optional] 
**guid** | **String** |  | [optional] 
**gatewayReference** | **String** |  | 
**operationMode** | [**OperationMode**](OperationMode.md) |  | [optional] 
**heatLevel** | **double** |  | [optional] 
**holidayActive** | **bool** | Indicates if the device has a currently active holiday program | [optional] 
**activeHolidayProgramId** | **int** |  | [optional] 
**activeTimeProgramId** | **int** |  | [optional] 
**state** | **String** |  | [optional] 
**timePrograms** | [**List<TimeProgram>**](TimeProgram.md) |  | [optional] [default to const []]
**rooms** | [**List<Room>**](Room.md) |  | [optional] [default to const []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


