# heat_control_api.model.HeatLevelMapping

## Load the model package
```dart
import 'package:heat_control_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**heatLevel** | **double** |  | [optional] 
**temperature** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


