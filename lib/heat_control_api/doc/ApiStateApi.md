# heat_control_api.api.ApiStateApi

## Load the API package
```dart
import 'package:heat_control_api/api.dart';
```

All URIs are relative to *http://localhost/api/user*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getApiState**](ApiStateApi.md#getapistate) | **GET** /apiState | gets the state of the api


# **getApiState**
> ApiState getApiState()

gets the state of the api

 With this method the client call its api state. This is the only operation where the client has to set no specific Accept-header.

### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = ApiStateApi();

try { 
    final result = api_instance.getApiState();
    print(result);
} catch (e) {
    print('Exception when calling ApiStateApi->getApiState: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiState**](ApiState.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

