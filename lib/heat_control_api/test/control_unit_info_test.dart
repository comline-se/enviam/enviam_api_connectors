//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:heat_control_api/api.dart';
import 'package:test/test.dart';

// tests for ControlUnitInfo
void main() {
  final instance = ControlUnitInfo();

  group('test ControlUnitInfo', () {
    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // String serial
    test('to test the property `serial`', () async {
      // TODO
    });

    // String guid
    test('to test the property `guid`', () async {
      // TODO
    });

    // String gatewayReference
    test('to test the property `gatewayReference`', () async {
      // TODO
    });

    // OperationMode operationMode
    test('to test the property `operationMode`', () async {
      // TODO
    });

    // double heatLevel
    test('to test the property `heatLevel`', () async {
      // TODO
    });

    // Indicates if the device has a currently active holiday program
    // bool holidayActive
    test('to test the property `holidayActive`', () async {
      // TODO
    });

    // int activeHolidayProgramId
    test('to test the property `activeHolidayProgramId`', () async {
      // TODO
    });

    // int activeTimeProgramId
    test('to test the property `activeTimeProgramId`', () async {
      // TODO
    });

    // String state
    test('to test the property `state`', () async {
      // TODO
    });

    // List<TimeProgram> timePrograms (default value: const [])
    test('to test the property `timePrograms`', () async {
      // TODO
    });

    // List<Room> rooms (default value: const [])
    test('to test the property `rooms`', () async {
      // TODO
    });


  });

}
