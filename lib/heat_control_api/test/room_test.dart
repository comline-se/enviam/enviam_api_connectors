//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:heat_control_api/api.dart';
import 'package:test/test.dart';

// tests for Room
void main() {
  final instance = Room();

  group('test Room', () {
    // int id
    test('to test the property `id`', () async {
      // TODO
    });

    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // int roomId
    test('to test the property `roomId`', () async {
      // TODO
    });

    // OperationMode operationMode
    test('to test the property `operationMode`', () async {
      // TODO
    });

    // double heatLevel
    test('to test the property `heatLevel`', () async {
      // TODO
    });

    // The effective heat level represents the heat level that is actually in effect for the room. It can be the same as the heat level (if the room is in operation mode MANUELL for example), but in general it can differ from it. The effective heat level dependes on the operation mode of the room, if the current time falls into a holiday period, and also on the configuration of room 0 (if the room is in mode ZENTRAL).
    // double effectiveHeatLevel
    test('to test the property `effectiveHeatLevel`', () async {
      // TODO
    });

    // int activeTimeProgramId
    test('to test the property `activeTimeProgramId`', () async {
      // TODO
    });


  });

}
