//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:heat_control_api/api.dart';
import 'package:test/test.dart';


/// tests for HeatControlUnitsApi
void main() {
  final instance = HeatControlUnitsApi();

  group('tests for HeatControlUnitsApi', () {
    //Future createHolidayProgramForControlUnit(String guid, HolidayProgram body) async
    test('test createHolidayProgramForControlUnit', () async {
      // TODO
    });

    //Future createTimeProgramForControlUnit(String guid, TimeProgram body) async
    test('test createTimeProgramForControlUnit', () async {
      // TODO
    });

    //Future deleteTimeProgramForControlUnit(String guid, int id) async
    test('test deleteTimeProgramForControlUnit', () async {
      // TODO
    });

    //Future<Apartment> getApartmentForControlUnit(String guid) async
    test('test getApartmentForControlUnit', () async {
      // TODO
    });

    // get detailed information about the control unit
    //
    //Future<ControlUnit> getControlUnitById(String guid) async
    test('test getControlUnitById', () async {
      // TODO
    });

    //Future<ControlUnitInfo> getControlUnitInfo(String guid) async
    test('test getControlUnitInfo', () async {
      // TODO
    });

    // get all control units for the given user
    //
    //Future<List<ControlUnit>> getControlUnits() async
    test('test getControlUnits', () async {
      // TODO
    });

    //Future<TimeProgram> getDefaultTimeProgram(String guid) async
    test('test getDefaultTimeProgram', () async {
      // TODO
    });

    // get heat Level Mapping
    //
    //Future<List<HeatLevelMapping>> getHeatLevelMapping() async
    test('test getHeatLevelMapping', () async {
      // TODO
    });

    //Future<HolidayProgram> getHolidayProgramForControlUnitById(String guid, int id) async
    test('test getHolidayProgramForControlUnitById', () async {
      // TODO
    });

    //Future<List<HolidayProgram>> getHolidayProgramsForControlUnit(String guid) async
    test('test getHolidayProgramsForControlUnit', () async {
      // TODO
    });

    //Future<List<Room>> getRoomsForControlUnit(String guid) async
    test('test getRoomsForControlUnit', () async {
      // TODO
    });

    //Future<TimeProgram> getTimeProgramForControlUnitById(String guid, int id) async
    test('test getTimeProgramForControlUnitById', () async {
      // TODO
    });

    // Returns the time programs for the room with the given ID
    //
    //Future<List<TimeProgram>> getTimeProgramsForControlUnit(String guid) async
    test('test getTimeProgramsForControlUnit', () async {
      // TODO
    });

    // Sets the operation mode for the room with the given ID
    //
    //Future setModeForRoom(String guid, int roomId, OperationMode mode, { double level }) async
    test('test setModeForRoom', () async {
      // TODO
    });

    // Sets the human readable name for the room with the given ID
    //
    //Future setNameForRoom(String guid, int roomId, SetRoomNameRequestBody setRoomNameRequestBody) async
    test('test setNameForRoom', () async {
      // TODO
    });

    //Future setOperationModeForApartment(String guid, OperationMode mode, { double level }) async
    test('test setOperationModeForApartment', () async {
      // TODO
    });

    //Future setTimeProgramForApartment(String guid, int timeProgramId) async
    test('test setTimeProgramForApartment', () async {
      // TODO
    });

    //Future setTimeProgramForRoom(String guid, int roomId, int timeProgramId) async
    test('test setTimeProgramForRoom', () async {
      // TODO
    });

    // Stores the current operation mode of room 0 and sets the operation mode of room 0 to \"OFF\". i.e. switches the heat control devices for the entire apartment off.
    //
    //Future switchHeatControlOff(String guid) async
    test('test switchHeatControlOff', () async {
      // TODO
    });

    // Restores the last operation mode for room 0. i.e. switches the heat control for the apartment back on.
    //
    //Future switchHeatControlOn(String guid) async
    test('test switchHeatControlOn', () async {
      // TODO
    });

    //Future updateHolidayProgramForControlUnit(String guid, int id, HolidayProgram body) async
    test('test updateHolidayProgramForControlUnit', () async {
      // TODO
    });

    //Future updateTimeProgramForControlUnit(String guid, int id, TimeProgram body) async
    test('test updateTimeProgramForControlUnit', () async {
      // TODO
    });

  });
}
