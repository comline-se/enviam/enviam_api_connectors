//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class ApiState {
  /// Instantiate a new enum with the provided [value].
  const ApiState._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const active = ApiState._(r'Active');
  static const deprecated = ApiState._(r'Deprecated');
  static const endOfLife = ApiState._(r'EndOfLife');
  static const null_ = ApiState._(r'null');

  /// List of all possible values in this [enum][ApiState].
  static const values = <ApiState>[
    active,
    deprecated,
    endOfLife,
    null_,
  ];

  static ApiState fromJson(dynamic value) =>
    ApiStateTypeTransformer().decode(value);

  static List<ApiState> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ApiState>[]
      : json
          .map((value) => ApiState.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [ApiState] to String,
/// and [decode] dynamic data back to [ApiState].
class ApiStateTypeTransformer {
  const ApiStateTypeTransformer._();

  factory ApiStateTypeTransformer() => _instance ??= ApiStateTypeTransformer._();

  String encode(ApiState data) => data.value;

  /// Decodes a [dynamic value][data] to a ApiState.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  ApiState decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case r'Active': return ApiState.active;
      case r'Deprecated': return ApiState.deprecated;
      case r'EndOfLife': return ApiState.endOfLife;
      case r'null': return ApiState.null_;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [ApiStateTypeTransformer] instance.
  static ApiStateTypeTransformer _instance;
}
