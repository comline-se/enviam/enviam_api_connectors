//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Geolocation {
  /// Returns a new [Geolocation] instance.
  Geolocation({
    @required this.latitude,
    @required this.longitude,
  });

  double latitude;

  double longitude;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Geolocation &&
     other.latitude == latitude &&
     other.longitude == longitude;

  @override
  int get hashCode =>
    (latitude == null ? 0 : latitude.hashCode) +
    (longitude == null ? 0 : longitude.hashCode);

  @override
  String toString() => 'Geolocation[latitude=$latitude, longitude=$longitude]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'latitude'] = latitude;
      json[r'longitude'] = longitude;
    return json;
  }

  /// Returns a new [Geolocation] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Geolocation fromJson(Map<String, dynamic> json) => json == null
    ? null
    : Geolocation(
        latitude: json[r'latitude'],
        longitude: json[r'longitude'],
    );

  static List<Geolocation> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Geolocation>[]
      : json.map((dynamic value) => Geolocation.fromJson(value)).toList(growable: true == growable);

  static Map<String, Geolocation> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Geolocation>{};
    if (json?.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = Geolocation.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of Geolocation-objects as value to a dart map
  static Map<String, List<Geolocation>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<Geolocation>>{};
    if (json?.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = Geolocation.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

