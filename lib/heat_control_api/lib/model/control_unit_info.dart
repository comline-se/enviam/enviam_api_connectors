//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ControlUnitInfo {
  /// Returns a new [ControlUnitInfo] instance.
  ControlUnitInfo({
    @required this.name,
    this.serial,
    this.guid,
    @required this.gatewayReference,
    this.operationMode,
    this.heatLevel,
    this.holidayActive,
    this.activeHolidayProgramId,
    this.activeTimeProgramId,
    this.state,
    this.timePrograms = const [],
    this.rooms = const [],
  });

  String name;

  String serial;

  String guid;

  String gatewayReference;

  OperationMode operationMode;

  double heatLevel;

  /// Indicates if the device has a currently active holiday program
  bool holidayActive;

  int activeHolidayProgramId;

  int activeTimeProgramId;

  String state;

  List<TimeProgram> timePrograms;

  List<Room> rooms;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ControlUnitInfo &&
     other.name == name &&
     other.serial == serial &&
     other.guid == guid &&
     other.gatewayReference == gatewayReference &&
     other.operationMode == operationMode &&
     other.heatLevel == heatLevel &&
     other.holidayActive == holidayActive &&
     other.activeHolidayProgramId == activeHolidayProgramId &&
     other.activeTimeProgramId == activeTimeProgramId &&
     other.state == state &&
     other.timePrograms == timePrograms &&
     other.rooms == rooms;

  @override
  int get hashCode =>
    (name == null ? 0 : name.hashCode) +
    (serial == null ? 0 : serial.hashCode) +
    (guid == null ? 0 : guid.hashCode) +
    (gatewayReference == null ? 0 : gatewayReference.hashCode) +
    (operationMode == null ? 0 : operationMode.hashCode) +
    (heatLevel == null ? 0 : heatLevel.hashCode) +
    (holidayActive == null ? 0 : holidayActive.hashCode) +
    (activeHolidayProgramId == null ? 0 : activeHolidayProgramId.hashCode) +
    (activeTimeProgramId == null ? 0 : activeTimeProgramId.hashCode) +
    (state == null ? 0 : state.hashCode) +
    (timePrograms == null ? 0 : timePrograms.hashCode) +
    (rooms == null ? 0 : rooms.hashCode);

  @override
  String toString() => 'ControlUnitInfo[name=$name, serial=$serial, guid=$guid, gatewayReference=$gatewayReference, operationMode=$operationMode, heatLevel=$heatLevel, holidayActive=$holidayActive, activeHolidayProgramId=$activeHolidayProgramId, activeTimeProgramId=$activeTimeProgramId, state=$state, timePrograms=$timePrograms, rooms=$rooms]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'name'] = name;
    if (serial != null) {
      json[r'serial'] = serial;
    }
    if (guid != null) {
      json[r'guid'] = guid;
    }
      json[r'gatewayReference'] = gatewayReference;
    if (operationMode != null) {
      json[r'operationMode'] = operationMode;
    }
    if (heatLevel != null) {
      json[r'heatLevel'] = heatLevel;
    }
    if (holidayActive != null) {
      json[r'holidayActive'] = holidayActive;
    }
    if (activeHolidayProgramId != null) {
      json[r'activeHolidayProgramId'] = activeHolidayProgramId;
    }
    if (activeTimeProgramId != null) {
      json[r'activeTimeProgramId'] = activeTimeProgramId;
    }
    if (state != null) {
      json[r'state'] = state;
    }
    if (timePrograms != null) {
      json[r'timePrograms'] = timePrograms;
    }
    if (rooms != null) {
      json[r'rooms'] = rooms;
    }
    return json;
  }

  /// Returns a new [ControlUnitInfo] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ControlUnitInfo fromJson(Map<String, dynamic> json) => json == null
    ? null
    : ControlUnitInfo(
        name: json[r'name'],
        serial: json[r'serial'],
        guid: json[r'guid'],
        gatewayReference: json[r'gatewayReference'],
        operationMode: OperationMode.fromJson(json[r'operationMode']),
        heatLevel: json[r'heatLevel'],
        holidayActive: json[r'holidayActive'],
        activeHolidayProgramId: json[r'activeHolidayProgramId'],
        activeTimeProgramId: json[r'activeTimeProgramId'],
        state: json[r'state'],
        timePrograms: TimeProgram.listFromJson(json[r'timePrograms']),
        rooms: Room.listFromJson(json[r'rooms']),
    );

  static List<ControlUnitInfo> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ControlUnitInfo>[]
      : json.map((dynamic value) => ControlUnitInfo.fromJson(value)).toList(growable: true == growable);

  static Map<String, ControlUnitInfo> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ControlUnitInfo>{};
    if (json?.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = ControlUnitInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ControlUnitInfo-objects as value to a dart map
  static Map<String, List<ControlUnitInfo>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<ControlUnitInfo>>{};
    if (json?.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = ControlUnitInfo.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

