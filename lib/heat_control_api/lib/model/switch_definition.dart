//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class SwitchDefinition {
  /// Returns a new [SwitchDefinition] instance.
  SwitchDefinition({
    this.id,
    this.switchTime,
    @required this.heatLevel,
    this.monday,
    this.tuesday,
    this.wednesday,
    this.thursday,
    this.friday,
    this.saturday,
    this.sunday,
  });

  int id;

  String switchTime;

  double heatLevel;

  bool monday;

  bool tuesday;

  bool wednesday;

  bool thursday;

  bool friday;

  bool saturday;

  bool sunday;

  @override
  bool operator ==(Object other) => identical(this, other) || other is SwitchDefinition &&
     other.id == id &&
     other.switchTime == switchTime &&
     other.heatLevel == heatLevel &&
     other.monday == monday &&
     other.tuesday == tuesday &&
     other.wednesday == wednesday &&
     other.thursday == thursday &&
     other.friday == friday &&
     other.saturday == saturday &&
     other.sunday == sunday;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (switchTime == null ? 0 : switchTime.hashCode) +
    (heatLevel == null ? 0 : heatLevel.hashCode) +
    (monday == null ? 0 : monday.hashCode) +
    (tuesday == null ? 0 : tuesday.hashCode) +
    (wednesday == null ? 0 : wednesday.hashCode) +
    (thursday == null ? 0 : thursday.hashCode) +
    (friday == null ? 0 : friday.hashCode) +
    (saturday == null ? 0 : saturday.hashCode) +
    (sunday == null ? 0 : sunday.hashCode);

  @override
  String toString() => 'SwitchDefinition[id=$id, switchTime=$switchTime, heatLevel=$heatLevel, monday=$monday, tuesday=$tuesday, wednesday=$wednesday, thursday=$thursday, friday=$friday, saturday=$saturday, sunday=$sunday]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (switchTime != null) {
      json[r'switchTime'] = switchTime;
    }
      json[r'heatLevel'] = heatLevel;
    if (monday != null) {
      json[r'monday'] = monday;
    }
    if (tuesday != null) {
      json[r'tuesday'] = tuesday;
    }
    if (wednesday != null) {
      json[r'wednesday'] = wednesday;
    }
    if (thursday != null) {
      json[r'thursday'] = thursday;
    }
    if (friday != null) {
      json[r'friday'] = friday;
    }
    if (saturday != null) {
      json[r'saturday'] = saturday;
    }
    if (sunday != null) {
      json[r'sunday'] = sunday;
    }
    return json;
  }

  /// Returns a new [SwitchDefinition] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static SwitchDefinition fromJson(Map<String, dynamic> json) => json == null
    ? null
    : SwitchDefinition(
        id: json[r'id'],
        switchTime: json[r'switchTime'],
        heatLevel: json[r'heatLevel'],
        monday: json[r'monday'],
        tuesday: json[r'tuesday'],
        wednesday: json[r'wednesday'],
        thursday: json[r'thursday'],
        friday: json[r'friday'],
        saturday: json[r'saturday'],
        sunday: json[r'sunday'],
    );

  static List<SwitchDefinition> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <SwitchDefinition>[]
      : json.map((dynamic value) => SwitchDefinition.fromJson(value)).toList(growable: true == growable);

  static Map<String, SwitchDefinition> mapFromJson(Map<String, dynamic> json) {
    final map = <String, SwitchDefinition>{};
    if (json?.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = SwitchDefinition.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of SwitchDefinition-objects as value to a dart map
  static Map<String, List<SwitchDefinition>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<SwitchDefinition>>{};
    if (json?.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = SwitchDefinition.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

