//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class TimeProgram {
  /// Returns a new [TimeProgram] instance.
  TimeProgram({
    this.id,
    @required this.name,
    this.programId,
    this.switchDefinitions = const [],
  });

  int id;

  String name;

  int programId;

  List<SwitchDefinition> switchDefinitions;

  @override
  bool operator ==(Object other) => identical(this, other) || other is TimeProgram &&
     other.id == id &&
     other.name == name &&
     other.programId == programId &&
     other.switchDefinitions == switchDefinitions;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (programId == null ? 0 : programId.hashCode) +
    (switchDefinitions == null ? 0 : switchDefinitions.hashCode);

  @override
  String toString() => 'TimeProgram[id=$id, name=$name, programId=$programId, switchDefinitions=$switchDefinitions]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
      json[r'name'] = name;
    if (programId != null) {
      json[r'programId'] = programId;
    }
    if (switchDefinitions != null) {
      json[r'switchDefinitions'] = switchDefinitions;
    }
    return json;
  }

  /// Returns a new [TimeProgram] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static TimeProgram fromJson(Map<String, dynamic> json) => json == null
    ? null
    : TimeProgram(
        id: json[r'id'],
        name: json[r'name'],
        programId: json[r'programId'],
        switchDefinitions: SwitchDefinition.listFromJson(json[r'switchDefinitions']),
    );

  static List<TimeProgram> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <TimeProgram>[]
      : json.map((dynamic value) => TimeProgram.fromJson(value)).toList(growable: true == growable);

  static Map<String, TimeProgram> mapFromJson(Map<String, dynamic> json) {
    final map = <String, TimeProgram>{};
    if (json?.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = TimeProgram.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of TimeProgram-objects as value to a dart map
  static Map<String, List<TimeProgram>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<TimeProgram>>{};
    if (json?.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = TimeProgram.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

