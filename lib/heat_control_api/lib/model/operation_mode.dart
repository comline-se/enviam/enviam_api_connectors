//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class OperationMode {
  /// Instantiate a new enum with the provided [value].
  const OperationMode._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const INEXISTENT = OperationMode._(r'INEXISTENT');
  static const false_ = OperationMode._(r'false');
  static const STANDBY = OperationMode._(r'STANDBY');
  static const MANUELL = OperationMode._(r'MANUELL');
  static const MANUELL_LOKAL = OperationMode._(r'MANUELL_LOKAL');
  static const AUTOMATIK = OperationMode._(r'AUTOMATIK');
  static const ZENTRAL = OperationMode._(r'ZENTRAL');

  /// List of all possible values in this [enum][OperationMode].
  static const values = <OperationMode>[
    INEXISTENT,
    false_,
    STANDBY,
    MANUELL,
    MANUELL_LOKAL,
    AUTOMATIK,
    ZENTRAL,
  ];

  static OperationMode fromJson(dynamic value) =>
    OperationModeTypeTransformer().decode(value);

  static List<OperationMode> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <OperationMode>[]
      : json
          .map((value) => OperationMode.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [OperationMode] to String,
/// and [decode] dynamic data back to [OperationMode].
class OperationModeTypeTransformer {
  const OperationModeTypeTransformer._();

  factory OperationModeTypeTransformer() => _instance ??= OperationModeTypeTransformer._();

  String encode(OperationMode data) => data.value;

  /// Decodes a [dynamic value][data] to a OperationMode.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  OperationMode decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case r'INEXISTENT': return OperationMode.INEXISTENT;
      case r'false': return OperationMode.false_;
      case r'STANDBY': return OperationMode.STANDBY;
      case r'MANUELL': return OperationMode.MANUELL;
      case r'MANUELL_LOKAL': return OperationMode.MANUELL_LOKAL;
      case r'AUTOMATIK': return OperationMode.AUTOMATIK;
      case r'ZENTRAL': return OperationMode.ZENTRAL;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [OperationModeTypeTransformer] instance.
  static OperationModeTypeTransformer _instance;
}
