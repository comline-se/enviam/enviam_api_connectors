//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class HeatLevelMapping {
  /// Returns a new [HeatLevelMapping] instance.
  HeatLevelMapping({
    this.heatLevel,
    this.temperature,
  });

  double heatLevel;

  double temperature;

  @override
  bool operator ==(Object other) => identical(this, other) || other is HeatLevelMapping &&
     other.heatLevel == heatLevel &&
     other.temperature == temperature;

  @override
  int get hashCode =>
    (heatLevel == null ? 0 : heatLevel.hashCode) +
    (temperature == null ? 0 : temperature.hashCode);

  @override
  String toString() => 'HeatLevelMapping[heatLevel=$heatLevel, temperature=$temperature]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (heatLevel != null) {
      json[r'heatLevel'] = heatLevel;
    }
    if (temperature != null) {
      json[r'temperature'] = temperature;
    }
    return json;
  }

  /// Returns a new [HeatLevelMapping] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static HeatLevelMapping fromJson(Map<String, dynamic> json) => json == null
    ? null
    : HeatLevelMapping(
        heatLevel: json[r'heatLevel'],
        temperature: json[r'temperature'],
    );

  static List<HeatLevelMapping> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <HeatLevelMapping>[]
      : json.map((dynamic value) => HeatLevelMapping.fromJson(value)).toList(growable: true == growable);

  static Map<String, HeatLevelMapping> mapFromJson(Map<String, dynamic> json) {
    final map = <String, HeatLevelMapping>{};
    if (json?.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = HeatLevelMapping.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of HeatLevelMapping-objects as value to a dart map
  static Map<String, List<HeatLevelMapping>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<HeatLevelMapping>>{};
    if (json?.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = HeatLevelMapping.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

