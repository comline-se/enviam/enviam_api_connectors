//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Room {
  /// Returns a new [Room] instance.
  Room({
    this.id,
    @required this.name,
    @required this.roomId,
    this.operationMode,
    this.heatLevel,
    this.effectiveHeatLevel,
    this.activeTimeProgramId,
  });

  int id;

  String name;

  int roomId;

  OperationMode operationMode;

  double heatLevel;

  /// The effective heat level represents the heat level that is actually in effect for the room. It can be the same as the heat level (if the room is in operation mode MANUELL for example), but in general it can differ from it. The effective heat level dependes on the operation mode of the room, if the current time falls into a holiday period, and also on the configuration of room 0 (if the room is in mode ZENTRAL).
  double effectiveHeatLevel;

  int activeTimeProgramId;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Room &&
     other.id == id &&
     other.name == name &&
     other.roomId == roomId &&
     other.operationMode == operationMode &&
     other.heatLevel == heatLevel &&
     other.effectiveHeatLevel == effectiveHeatLevel &&
     other.activeTimeProgramId == activeTimeProgramId;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (roomId == null ? 0 : roomId.hashCode) +
    (operationMode == null ? 0 : operationMode.hashCode) +
    (heatLevel == null ? 0 : heatLevel.hashCode) +
    (effectiveHeatLevel == null ? 0 : effectiveHeatLevel.hashCode) +
    (activeTimeProgramId == null ? 0 : activeTimeProgramId.hashCode);

  @override
  String toString() => 'Room[id=$id, name=$name, roomId=$roomId, operationMode=$operationMode, heatLevel=$heatLevel, effectiveHeatLevel=$effectiveHeatLevel, activeTimeProgramId=$activeTimeProgramId]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
      json[r'name'] = name;
      json[r'roomId'] = roomId;
    if (operationMode != null) {
      json[r'operationMode'] = operationMode;
    }
    if (heatLevel != null) {
      json[r'heatLevel'] = heatLevel;
    }
    if (effectiveHeatLevel != null) {
      json[r'effectiveHeatLevel'] = effectiveHeatLevel;
    }
    if (activeTimeProgramId != null) {
      json[r'activeTimeProgramId'] = activeTimeProgramId;
    }
    return json;
  }

  /// Returns a new [Room] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Room fromJson(Map<String, dynamic> json) => json == null
    ? null
    : Room(
        id: json[r'id'],
        name: json[r'name'],
        roomId: json[r'roomId'],
        operationMode: OperationMode.fromJson(json[r'operationMode']),
        heatLevel: json[r'heatLevel'],
        effectiveHeatLevel: json[r'effectiveHeatLevel'],
        activeTimeProgramId: json[r'activeTimeProgramId'],
    );

  static List<Room> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Room>[]
      : json.map((dynamic value) => Room.fromJson(value)).toList(growable: true == growable);

  static Map<String, Room> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Room>{};
    if (json?.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = Room.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of Room-objects as value to a dart map
  static Map<String, List<Room>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<Room>>{};
    if (json?.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = Room.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

