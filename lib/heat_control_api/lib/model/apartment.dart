//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Apartment {
  /// Returns a new [Apartment] instance.
  Apartment({
    @required this.name,
    @required this.activeTimeProgram,
    this.heatLevel,
  });

  String name;

  String activeTimeProgram;

  double heatLevel;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Apartment &&
     other.name == name &&
     other.activeTimeProgram == activeTimeProgram &&
     other.heatLevel == heatLevel;

  @override
  int get hashCode =>
    (name == null ? 0 : name.hashCode) +
    (activeTimeProgram == null ? 0 : activeTimeProgram.hashCode) +
    (heatLevel == null ? 0 : heatLevel.hashCode);

  @override
  String toString() => 'Apartment[name=$name, activeTimeProgram=$activeTimeProgram, heatLevel=$heatLevel]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'name'] = name;
      json[r'activeTimeProgram'] = activeTimeProgram;
    if (heatLevel != null) {
      json[r'heatLevel'] = heatLevel;
    }
    return json;
  }

  /// Returns a new [Apartment] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Apartment fromJson(Map<String, dynamic> json) => json == null
    ? null
    : Apartment(
        name: json[r'name'],
        activeTimeProgram: json[r'activeTimeProgram'],
        heatLevel: json[r'heatLevel'],
    );

  static List<Apartment> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Apartment>[]
      : json.map((dynamic value) => Apartment.fromJson(value)).toList(growable: true == growable);

  static Map<String, Apartment> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Apartment>{};
    if (json?.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = Apartment.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of Apartment-objects as value to a dart map
  static Map<String, List<Apartment>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<Apartment>>{};
    if (json?.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = Apartment.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

