//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ControlUnit {
  /// Returns a new [ControlUnit] instance.
  ControlUnit({
    this.id,
    this.name,
    this.serial,
    this.guid,
    this.state,
    this.gatewayReference,
    this.firmwareVersion,
    this.geolocation,
  });

  int id;

  String name;

  String serial;

  String guid;

  String state;

  String gatewayReference;

  String firmwareVersion;

  Geolocation geolocation;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ControlUnit &&
     other.id == id &&
     other.name == name &&
     other.serial == serial &&
     other.guid == guid &&
     other.state == state &&
     other.gatewayReference == gatewayReference &&
     other.firmwareVersion == firmwareVersion &&
     other.geolocation == geolocation;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (serial == null ? 0 : serial.hashCode) +
    (guid == null ? 0 : guid.hashCode) +
    (state == null ? 0 : state.hashCode) +
    (gatewayReference == null ? 0 : gatewayReference.hashCode) +
    (firmwareVersion == null ? 0 : firmwareVersion.hashCode) +
    (geolocation == null ? 0 : geolocation.hashCode);

  @override
  String toString() => 'ControlUnit[id=$id, name=$name, serial=$serial, guid=$guid, state=$state, gatewayReference=$gatewayReference, firmwareVersion=$firmwareVersion, geolocation=$geolocation]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    if (serial != null) {
      json[r'serial'] = serial;
    }
    if (guid != null) {
      json[r'guid'] = guid;
    }
    if (state != null) {
      json[r'state'] = state;
    }
    if (gatewayReference != null) {
      json[r'gatewayReference'] = gatewayReference;
    }
    if (firmwareVersion != null) {
      json[r'firmwareVersion'] = firmwareVersion;
    }
    if (geolocation != null) {
      json[r'geolocation'] = geolocation;
    }
    return json;
  }

  /// Returns a new [ControlUnit] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ControlUnit fromJson(Map<String, dynamic> json) => json == null
    ? null
    : ControlUnit(
        id: json[r'id'],
        name: json[r'name'],
        serial: json[r'serial'],
        guid: json[r'guid'],
        state: json[r'state'],
        gatewayReference: json[r'gatewayReference'],
        firmwareVersion: json[r'firmwareVersion'],
        geolocation: Geolocation.fromJson(json[r'geolocation']),
    );

  static List<ControlUnit> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ControlUnit>[]
      : json.map((dynamic value) => ControlUnit.fromJson(value)).toList(growable: true == growable);

  static Map<String, ControlUnit> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ControlUnit>{};
    if (json?.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = ControlUnit.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ControlUnit-objects as value to a dart map
  static Map<String, List<ControlUnit>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<ControlUnit>>{};
    if (json?.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = ControlUnit.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

