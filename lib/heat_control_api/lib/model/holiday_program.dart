//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class HolidayProgram {
  /// Returns a new [HolidayProgram] instance.
  HolidayProgram({
    this.id,
    @required this.name,
    this.programId,
    this.heatLevel,
    this.startDate,
    this.endDate,
    this.active,
  });

  int id;

  String name;

  int programId;

  double heatLevel;

  DateTime startDate;

  DateTime endDate;

  bool active;

  @override
  bool operator ==(Object other) => identical(this, other) || other is HolidayProgram &&
     other.id == id &&
     other.name == name &&
     other.programId == programId &&
     other.heatLevel == heatLevel &&
     other.startDate == startDate &&
     other.endDate == endDate &&
     other.active == active;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (programId == null ? 0 : programId.hashCode) +
    (heatLevel == null ? 0 : heatLevel.hashCode) +
    (startDate == null ? 0 : startDate.hashCode) +
    (endDate == null ? 0 : endDate.hashCode) +
    (active == null ? 0 : active.hashCode);

  @override
  String toString() => 'HolidayProgram[id=$id, name=$name, programId=$programId, heatLevel=$heatLevel, startDate=$startDate, endDate=$endDate, active=$active]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
      json[r'name'] = name;
    if (programId != null) {
      json[r'programId'] = programId;
    }
    if (heatLevel != null) {
      json[r'heatLevel'] = heatLevel;
    }
    if (startDate != null) {
      json[r'startDate'] = startDate.toUtc().toIso8601String();
    }
    if (endDate != null) {
      json[r'endDate'] = endDate.toUtc().toIso8601String();
    }
    if (active != null) {
      json[r'active'] = active;
    }
    return json;
  }

  /// Returns a new [HolidayProgram] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static HolidayProgram fromJson(Map<String, dynamic> json) => json == null
    ? null
    : HolidayProgram(
        id: json[r'id'],
        name: json[r'name'],
        programId: json[r'programId'],
        heatLevel: json[r'heatLevel'],
        startDate: json[r'startDate'] == null
          ? null
          : DateTime.parse(json[r'startDate']),
        endDate: json[r'endDate'] == null
          ? null
          : DateTime.parse(json[r'endDate']),
        active: json[r'active'],
    );

  static List<HolidayProgram> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <HolidayProgram>[]
      : json.map((dynamic value) => HolidayProgram.fromJson(value)).toList(growable: true == growable);

  static Map<String, HolidayProgram> mapFromJson(Map<String, dynamic> json) {
    final map = <String, HolidayProgram>{};
    if (json?.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = HolidayProgram.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of HolidayProgram-objects as value to a dart map
  static Map<String, List<HolidayProgram>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<HolidayProgram>>{};
    if (json?.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = HolidayProgram.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

