//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class SetRoomNameRequestBody {
  /// Returns a new [SetRoomNameRequestBody] instance.
  SetRoomNameRequestBody({
    @required this.name,
  });

  String name;

  @override
  bool operator ==(Object other) => identical(this, other) || other is SetRoomNameRequestBody &&
     other.name == name;

  @override
  int get hashCode =>
    (name == null ? 0 : name.hashCode);

  @override
  String toString() => 'SetRoomNameRequestBody[name=$name]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'name'] = name;
    return json;
  }

  /// Returns a new [SetRoomNameRequestBody] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static SetRoomNameRequestBody fromJson(Map<String, dynamic> json) => json == null
    ? null
    : SetRoomNameRequestBody(
        name: json[r'name'],
    );

  static List<SetRoomNameRequestBody> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <SetRoomNameRequestBody>[]
      : json.map((dynamic value) => SetRoomNameRequestBody.fromJson(value)).toList(growable: true == growable);

  static Map<String, SetRoomNameRequestBody> mapFromJson(Map<String, dynamic> json) {
    final map = <String, SetRoomNameRequestBody>{};
    if (json?.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = SetRoomNameRequestBody.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of SetRoomNameRequestBody-objects as value to a dart map
  static Map<String, List<SetRoomNameRequestBody>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<SetRoomNameRequestBody>>{};
    if (json?.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = SetRoomNameRequestBody.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

