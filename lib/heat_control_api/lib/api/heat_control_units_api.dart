//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class HeatControlUnitsApi {
  HeatControlUnitsApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  final ApiClient apiClient;

  /// Performs an HTTP 'POST /heatControlUnits/{guid}/holidayPrograms' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [HolidayProgram] body (required):
  ///   body for the time program
  Future<Response> createHolidayProgramForControlUnitWithHttpInfo(String guid, HolidayProgram body) async {
    // Verify required params are set.
    if (guid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guid');
    }
    if (body == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: body');
    }

    final path = r'/heatControlUnits/{guid}/holidayPrograms'
      .replaceAll('{' + 'guid' + '}', guid.toString());

    Object postBody = body;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [HolidayProgram] body (required):
  ///   body for the time program
  Future<void> createHolidayProgramForControlUnit(String guid, HolidayProgram body) async {
    final response = await createHolidayProgramForControlUnitWithHttpInfo(guid, body);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
  }

  /// Performs an HTTP 'POST /heatControlUnits/{guid}/timePrograms' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [TimeProgram] body (required):
  ///   body for the time program
  Future<Response> createTimeProgramForControlUnitWithHttpInfo(String guid, TimeProgram body) async {
    // Verify required params are set.
    if (guid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guid');
    }
    if (body == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: body');
    }

    final path = r'/heatControlUnits/{guid}/timePrograms'
      .replaceAll('{' + 'guid' + '}', guid.toString());

    Object postBody = body;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [TimeProgram] body (required):
  ///   body for the time program
  Future<void> createTimeProgramForControlUnit(String guid, TimeProgram body) async {
    final response = await createTimeProgramForControlUnitWithHttpInfo(guid, body);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
  }

  /// Performs an HTTP 'DELETE /heatControlUnits/{guid}/timePrograms/{id}' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [int] id (required):
  ///   ID of the time program
  Future<Response> deleteTimeProgramForControlUnitWithHttpInfo(String guid, int id) async {
    // Verify required params are set.
    if (guid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guid');
    }
    if (id == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: id');
    }

    final path = r'/heatControlUnits/{guid}/timePrograms/{id}'
      .replaceAll('{' + 'guid' + '}', guid.toString())
      .replaceAll('{' + 'id' + '}', id.toString());

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'DELETE',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [int] id (required):
  ///   ID of the time program
  Future<void> deleteTimeProgramForControlUnit(String guid, int id) async {
    final response = await deleteTimeProgramForControlUnitWithHttpInfo(guid, id);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
  }

  /// Performs an HTTP 'GET /heatControlUnits/{guid}/apartment' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  Future<Response> getApartmentForControlUnitWithHttpInfo(String guid) async {
    // Verify required params are set.
    if (guid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guid');
    }

    final path = r'/heatControlUnits/{guid}/apartment'
      .replaceAll('{' + 'guid' + '}', guid.toString());

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  Future<Apartment> getApartmentForControlUnit(String guid) async {
    final response = await getApartmentForControlUnitWithHttpInfo(guid);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'Apartment',) as Apartment;
        }
    return Future<Apartment>.value(null);
  }

  /// get detailed information about the control unit
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  Future<Response> getControlUnitByIdWithHttpInfo(String guid) async {
    // Verify required params are set.
    if (guid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guid');
    }

    final path = r'/heatControlUnits/{guid}'
      .replaceAll('{' + 'guid' + '}', guid.toString());

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// get detailed information about the control unit
  ///
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  Future<ControlUnit> getControlUnitById(String guid) async {
    final response = await getControlUnitByIdWithHttpInfo(guid);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'ControlUnit',) as ControlUnit;
        }
    return Future<ControlUnit>.value(null);
  }

  /// Performs an HTTP 'GET /heatControlUnits/{guid}/info' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  Future<Response> getControlUnitInfoWithHttpInfo(String guid) async {
    // Verify required params are set.
    if (guid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guid');
    }

    final path = r'/heatControlUnits/{guid}/info'
      .replaceAll('{' + 'guid' + '}', guid.toString());

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  Future<ControlUnitInfo> getControlUnitInfo(String guid) async {
    final response = await getControlUnitInfoWithHttpInfo(guid);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'ControlUnitInfo',) as ControlUnitInfo;
        }
    return Future<ControlUnitInfo>.value(null);
  }

  /// get all control units for the given user
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> getControlUnitsWithHttpInfo() async {
    final path = r'/heatControlUnits';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// get all control units for the given user
  Future<List<ControlUnit>> getControlUnits() async {
    final response = await getControlUnitsWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return (await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'List<ControlUnit>') as List)
        .cast<ControlUnit>()
        .toList(growable: false);
    }
    return Future<List<ControlUnit>>.value(null);
  }

  /// Performs an HTTP 'GET /heatControlUnits/{guid}/timePrograms/template' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  Future<Response> getDefaultTimeProgramWithHttpInfo(String guid) async {
    // Verify required params are set.
    if (guid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guid');
    }

    final path = r'/heatControlUnits/{guid}/timePrograms/template'
      .replaceAll('{' + 'guid' + '}', guid.toString());

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  Future<TimeProgram> getDefaultTimeProgram(String guid) async {
    final response = await getDefaultTimeProgramWithHttpInfo(guid);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'TimeProgram',) as TimeProgram;
        }
    return Future<TimeProgram>.value(null);
  }

  /// get heat Level Mapping
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> getHeatLevelMappingWithHttpInfo() async {
    final path = r'/heatLevelMapping';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// get heat Level Mapping
  Future<List<HeatLevelMapping>> getHeatLevelMapping() async {
    final response = await getHeatLevelMappingWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return (await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'List<HeatLevelMapping>') as List)
        .cast<HeatLevelMapping>()
        .toList(growable: false);
    }
    return Future<List<HeatLevelMapping>>.value(null);
  }

  /// Performs an HTTP 'GET /heatControlUnits/{guid}/holidayPrograms/{id}' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [int] id (required):
  ///   ID of the holiday program
  Future<Response> getHolidayProgramForControlUnitByIdWithHttpInfo(String guid, int id) async {
    // Verify required params are set.
    if (guid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guid');
    }
    if (id == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: id');
    }

    final path = r'/heatControlUnits/{guid}/holidayPrograms/{id}'
      .replaceAll('{' + 'guid' + '}', guid.toString())
      .replaceAll('{' + 'id' + '}', id.toString());

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [int] id (required):
  ///   ID of the holiday program
  Future<HolidayProgram> getHolidayProgramForControlUnitById(String guid, int id) async {
    final response = await getHolidayProgramForControlUnitByIdWithHttpInfo(guid, id);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'HolidayProgram',) as HolidayProgram;
        }
    return Future<HolidayProgram>.value(null);
  }

  /// Performs an HTTP 'GET /heatControlUnits/{guid}/holidayPrograms' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  Future<Response> getHolidayProgramsForControlUnitWithHttpInfo(String guid) async {
    // Verify required params are set.
    if (guid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guid');
    }

    final path = r'/heatControlUnits/{guid}/holidayPrograms'
      .replaceAll('{' + 'guid' + '}', guid.toString());

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  Future<List<HolidayProgram>> getHolidayProgramsForControlUnit(String guid) async {
    final response = await getHolidayProgramsForControlUnitWithHttpInfo(guid);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return (await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'List<HolidayProgram>') as List)
        .cast<HolidayProgram>()
        .toList(growable: false);
    }
    return Future<List<HolidayProgram>>.value(null);
  }

  /// Performs an HTTP 'GET /heatControlUnits/{guid}/rooms' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  Future<Response> getRoomsForControlUnitWithHttpInfo(String guid) async {
    // Verify required params are set.
    if (guid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guid');
    }

    final path = r'/heatControlUnits/{guid}/rooms'
      .replaceAll('{' + 'guid' + '}', guid.toString());

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  Future<List<Room>> getRoomsForControlUnit(String guid) async {
    final response = await getRoomsForControlUnitWithHttpInfo(guid);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return (await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'List<Room>') as List)
        .cast<Room>()
        .toList(growable: false);
    }
    return Future<List<Room>>.value(null);
  }

  /// Performs an HTTP 'GET /heatControlUnits/{guid}/timePrograms/{id}' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [int] id (required):
  ///   ID of the time program
  Future<Response> getTimeProgramForControlUnitByIdWithHttpInfo(String guid, int id) async {
    // Verify required params are set.
    if (guid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guid');
    }
    if (id == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: id');
    }

    final path = r'/heatControlUnits/{guid}/timePrograms/{id}'
      .replaceAll('{' + 'guid' + '}', guid.toString())
      .replaceAll('{' + 'id' + '}', id.toString());

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [int] id (required):
  ///   ID of the time program
  Future<TimeProgram> getTimeProgramForControlUnitById(String guid, int id) async {
    final response = await getTimeProgramForControlUnitByIdWithHttpInfo(guid, id);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'TimeProgram',) as TimeProgram;
        }
    return Future<TimeProgram>.value(null);
  }

  /// Returns the time programs for the room with the given ID
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  Future<Response> getTimeProgramsForControlUnitWithHttpInfo(String guid) async {
    // Verify required params are set.
    if (guid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guid');
    }

    final path = r'/heatControlUnits/{guid}/timePrograms'
      .replaceAll('{' + 'guid' + '}', guid.toString());

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Returns the time programs for the room with the given ID
  ///
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  Future<List<TimeProgram>> getTimeProgramsForControlUnit(String guid) async {
    final response = await getTimeProgramsForControlUnitWithHttpInfo(guid);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return (await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'List<TimeProgram>') as List)
        .cast<TimeProgram>()
        .toList(growable: false);
    }
    return Future<List<TimeProgram>>.value(null);
  }

  /// Sets the operation mode for the room with the given ID
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [int] roomId (required):
  ///   id of the room as represented on the device (0-15)
  ///
  /// * [OperationMode] mode (required):
  ///   mode to set
  ///
  /// * [double] level:
  ///   heat level which is used if manual operation mode is selected
  Future<Response> setModeForRoomWithHttpInfo(String guid, int roomId, OperationMode mode, { double level }) async {
    // Verify required params are set.
    if (guid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guid');
    }
    if (roomId == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: roomId');
    }
    if (mode == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: mode');
    }

    final path = r'/heatControlUnits/{guid}/rooms/{roomId}/mode/{mode}'
      .replaceAll('{' + 'guid' + '}', guid.toString())
      .replaceAll('{' + 'roomId' + '}', roomId.toString())
      .replaceAll('{' + 'mode' + '}', mode.toString());

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    if (level != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'level', level));
    }

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Sets the operation mode for the room with the given ID
  ///
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [int] roomId (required):
  ///   id of the room as represented on the device (0-15)
  ///
  /// * [OperationMode] mode (required):
  ///   mode to set
  ///
  /// * [double] level:
  ///   heat level which is used if manual operation mode is selected
  Future<void> setModeForRoom(String guid, int roomId, OperationMode mode, { double level }) async {
    final response = await setModeForRoomWithHttpInfo(guid, roomId, mode,  level: level );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
  }

  /// Sets the human readable name for the room with the given ID
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [int] roomId (required):
  ///   id of the room as represented on the device (0-15)
  ///
  /// * [SetRoomNameRequestBody] setRoomNameRequestBody (required):
  ///   body for the name
  Future<Response> setNameForRoomWithHttpInfo(String guid, int roomId, SetRoomNameRequestBody setRoomNameRequestBody) async {
    // Verify required params are set.
    if (guid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guid');
    }
    if (roomId == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: roomId');
    }
    if (setRoomNameRequestBody == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: setRoomNameRequestBody');
    }

    final path = r'/heatControlUnits/{guid}/rooms/{roomId}/name'
      .replaceAll('{' + 'guid' + '}', guid.toString())
      .replaceAll('{' + 'roomId' + '}', roomId.toString());

    Object postBody = setRoomNameRequestBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Sets the human readable name for the room with the given ID
  ///
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [int] roomId (required):
  ///   id of the room as represented on the device (0-15)
  ///
  /// * [SetRoomNameRequestBody] setRoomNameRequestBody (required):
  ///   body for the name
  Future<void> setNameForRoom(String guid, int roomId, SetRoomNameRequestBody setRoomNameRequestBody) async {
    final response = await setNameForRoomWithHttpInfo(guid, roomId, setRoomNameRequestBody);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
  }

  /// Performs an HTTP 'POST /heatControlUnits/{guid}/apartment/mode/{mode}' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [OperationMode] mode (required):
  ///   active time program id
  ///
  /// * [double] level:
  ///   heat level which is used if manual operation mode is selected
  Future<Response> setOperationModeForApartmentWithHttpInfo(String guid, OperationMode mode, { double level }) async {
    // Verify required params are set.
    if (guid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guid');
    }
    if (mode == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: mode');
    }

    final path = r'/heatControlUnits/{guid}/apartment/mode/{mode}'
      .replaceAll('{' + 'guid' + '}', guid.toString())
      .replaceAll('{' + 'mode' + '}', mode.toString());

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    if (level != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'level', level));
    }

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [OperationMode] mode (required):
  ///   active time program id
  ///
  /// * [double] level:
  ///   heat level which is used if manual operation mode is selected
  Future<void> setOperationModeForApartment(String guid, OperationMode mode, { double level }) async {
    final response = await setOperationModeForApartmentWithHttpInfo(guid, mode,  level: level );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
  }

  /// Performs an HTTP 'POST /heatControlUnits/{guid}/apartment/timeProgram/{timeProgramId}' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [int] timeProgramId (required):
  ///   active time program id
  Future<Response> setTimeProgramForApartmentWithHttpInfo(String guid, int timeProgramId) async {
    // Verify required params are set.
    if (guid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guid');
    }
    if (timeProgramId == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: timeProgramId');
    }

    final path = r'/heatControlUnits/{guid}/apartment/timeProgram/{timeProgramId}'
      .replaceAll('{' + 'guid' + '}', guid.toString())
      .replaceAll('{' + 'timeProgramId' + '}', timeProgramId.toString());

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [int] timeProgramId (required):
  ///   active time program id
  Future<void> setTimeProgramForApartment(String guid, int timeProgramId) async {
    final response = await setTimeProgramForApartmentWithHttpInfo(guid, timeProgramId);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
  }

  /// Performs an HTTP 'POST /heatControlUnits/{guid}/rooms/{roomId}/timeProgram/{timeProgramId}' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [int] roomId (required):
  ///   id of the room as represented on the device (0-15)
  ///
  /// * [int] timeProgramId (required):
  ///   active time program id
  Future<Response> setTimeProgramForRoomWithHttpInfo(String guid, int roomId, int timeProgramId) async {
    // Verify required params are set.
    if (guid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guid');
    }
    if (roomId == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: roomId');
    }
    if (timeProgramId == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: timeProgramId');
    }

    final path = r'/heatControlUnits/{guid}/rooms/{roomId}/timeProgram/{timeProgramId}'
      .replaceAll('{' + 'guid' + '}', guid.toString())
      .replaceAll('{' + 'roomId' + '}', roomId.toString())
      .replaceAll('{' + 'timeProgramId' + '}', timeProgramId.toString());

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [int] roomId (required):
  ///   id of the room as represented on the device (0-15)
  ///
  /// * [int] timeProgramId (required):
  ///   active time program id
  Future<void> setTimeProgramForRoom(String guid, int roomId, int timeProgramId) async {
    final response = await setTimeProgramForRoomWithHttpInfo(guid, roomId, timeProgramId);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
  }

  /// Stores the current operation mode of room 0 and sets the operation mode of room 0 to \"OFF\". i.e. switches the heat control devices for the entire apartment off.
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  Future<Response> switchHeatControlOffWithHttpInfo(String guid) async {
    // Verify required params are set.
    if (guid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guid');
    }

    final path = r'/heatControlUnits/{guid}/switchOff'
      .replaceAll('{' + 'guid' + '}', guid.toString());

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Stores the current operation mode of room 0 and sets the operation mode of room 0 to \"OFF\". i.e. switches the heat control devices for the entire apartment off.
  ///
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  Future<void> switchHeatControlOff(String guid) async {
    final response = await switchHeatControlOffWithHttpInfo(guid);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
  }

  /// Restores the last operation mode for room 0. i.e. switches the heat control for the apartment back on.
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  Future<Response> switchHeatControlOnWithHttpInfo(String guid) async {
    // Verify required params are set.
    if (guid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guid');
    }

    final path = r'/heatControlUnits/{guid}/switchOn'
      .replaceAll('{' + 'guid' + '}', guid.toString());

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Restores the last operation mode for room 0. i.e. switches the heat control for the apartment back on.
  ///
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  Future<void> switchHeatControlOn(String guid) async {
    final response = await switchHeatControlOnWithHttpInfo(guid);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
  }

  /// Performs an HTTP 'PUT /heatControlUnits/{guid}/holidayPrograms/{id}' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [int] id (required):
  ///   ID of the holiday program
  ///
  /// * [HolidayProgram] body (required):
  ///   body for the time program
  Future<Response> updateHolidayProgramForControlUnitWithHttpInfo(String guid, int id, HolidayProgram body) async {
    // Verify required params are set.
    if (guid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guid');
    }
    if (id == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: id');
    }
    if (body == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: body');
    }

    final path = r'/heatControlUnits/{guid}/holidayPrograms/{id}'
      .replaceAll('{' + 'guid' + '}', guid.toString())
      .replaceAll('{' + 'id' + '}', id.toString());

    Object postBody = body;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'PUT',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [int] id (required):
  ///   ID of the holiday program
  ///
  /// * [HolidayProgram] body (required):
  ///   body for the time program
  Future<void> updateHolidayProgramForControlUnit(String guid, int id, HolidayProgram body) async {
    final response = await updateHolidayProgramForControlUnitWithHttpInfo(guid, id, body);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
  }

  /// Performs an HTTP 'PUT /heatControlUnits/{guid}/timePrograms/{id}' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [int] id (required):
  ///   ID of the time program
  ///
  /// * [TimeProgram] body (required):
  ///   body for the time program
  Future<Response> updateTimeProgramForControlUnitWithHttpInfo(String guid, int id, TimeProgram body) async {
    // Verify required params are set.
    if (guid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guid');
    }
    if (id == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: id');
    }
    if (body == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: body');
    }

    final path = r'/heatControlUnits/{guid}/timePrograms/{id}'
      .replaceAll('{' + 'guid' + '}', guid.toString())
      .replaceAll('{' + 'id' + '}', id.toString());

    Object postBody = body;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'PUT',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Parameters:
  ///
  /// * [String] guid (required):
  ///   ID of the control Unit
  ///
  /// * [int] id (required):
  ///   ID of the time program
  ///
  /// * [TimeProgram] body (required):
  ///   body for the time program
  Future<void> updateTimeProgramForControlUnit(String guid, int id, TimeProgram body) async {
    final response = await updateTimeProgramForControlUnitWithHttpInfo(guid, id, body);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
  }
}
