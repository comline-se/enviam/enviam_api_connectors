//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

library openapi.api;

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';

part 'api_client.dart';
part 'api_helper.dart';
part 'api_exception.dart';
part 'auth/authentication.dart';
part 'auth/api_key_auth.dart';
part 'auth/oauth.dart';
part 'auth/http_basic_auth.dart';
part 'auth/http_bearer_auth.dart';

part 'api/ev_station_api.dart';
part 'api/user_api.dart';
part 'api/grid_api.dart';

part 'model/car.dart';
part 'model/device.dart';
part 'model/device_category.dart';
part 'model/device_config.dart';
part 'model/device_config_parameters.dart';
part 'model/device_config_user_importance.dart';
part 'model/device_mode_battery.dart';
part 'model/device_mode_ev_station.dart';
part 'model/device_mode_full.dart';
part 'model/device_mode_heat_pump.dart';
part 'model/device_mode_plug.dart';
part 'model/device_optimization_mode.dart';
part 'model/device_sort.dart';
part 'model/device_state.dart';
part 'model/device_state_error_list.dart';
part 'model/device_switcher.dart';
part 'model/device_type.dart';
part 'model/device_update.dart';
part 'model/ev_station.dart';
part 'model/ev_station_all_of.dart';
part 'model/ev_station_charging_mode.dart';
part 'model/ev_station_charging_mode_request.dart';
part 'model/ev_station_values.dart';
part 'model/ev_station_values_all_of.dart';
part 'model/ev_station_config.dart';
part 'model/override_state.dart';
part 'model/schedule.dart';
part 'model/scheduled_device_mode.dart';
part 'model/sun_altitudes.dart';
part 'model/switch_override.dart';
part 'model/switch_state.dart';
part 'model/switching_thresholds.dart';
part 'model/threshold_type.dart';
part 'model/time_span.dart';
part 'model/user.dart';
part 'model/fill.dart';
part 'model/graph.dart';
part 'model/timeseries.dart';
part 'model/timeseries_response.dart';
part 'model/timeseries_type.dart';
part 'model/timeseries_unit.dart';
part 'model/grid_consumption_response.dart';
part 'model/user_address.dart';
part 'model/user_country.dart';
part 'model/user_currency.dart';
part 'model/user_distances.dart';
part 'model/user_language.dart';
part 'model/user_profile.dart';
part 'model/user_temperature.dart';
part 'model/validation_error.dart';
part 'model/visible_switch_state.dart';


const _delimiters = {'csv': ',', 'ssv': ' ', 'tsv': '\t', 'pipes': '|'};
const _dateEpochMarker = 'epoch';
final _dateFormatter = DateFormat('yyyy-MM-dd');
final _regList = RegExp(r'^List<(.*)>$');
final _regSet = RegExp(r'^Set<(.*)>$');
final _regMap = RegExp(r'^Map<String,(.*)>$');

ApiClient defaultApiClient = ApiClient();
