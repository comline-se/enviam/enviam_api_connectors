//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class QueryParam {
  const QueryParam(this.name, this.value);

  final String name;
  final String value;

  @override
  String toString() => '${Uri.encodeQueryComponent(name)}=${Uri.encodeQueryComponent(value)}';
}

// Ported from the Java version.
Iterable<QueryParam> _convertParametersForCollectionFormat(
  String collectionFormat,
  String name,
  dynamic value,
) {
  final params = <QueryParam>[];

  // preconditions
  if (name != null && name.isNotEmpty && value != null) {
    if (value is List) {
      // get the collection format, default: csv
      collectionFormat = (collectionFormat == null || collectionFormat.isEmpty)
        ? 'csv'
        : collectionFormat;

      if (collectionFormat == 'multi') {
        return value.map((v) => QueryParam(name, parameterToString(v)));
      }

      final delimiter = _delimiters[collectionFormat] ?? ',';

      params.add(QueryParam(name, value.map((v) => parameterToString(v)).join(delimiter)));
    } else {
      params.add(QueryParam(name, parameterToString(value)));
    }
  }

  return params;
}

/// Format the given parameter object into a [String].
String parameterToString(dynamic value) {
  if (value == null) {
    return '';
  }
  if (value is DateTime) {
    return DateFormat('yyyy-MM-ddThh:mm', "de_DE").format(value);
  }
  if (value is DeviceCategory) {
    return DeviceCategoryTypeTransformer().encode(value).toString();
  }
  if (value is DeviceConfigParameters) {
    return DeviceConfigParametersTypeTransformer().encode(value).toString();
  }
  if (value is DeviceModeBattery) {
    return DeviceModeBatteryTypeTransformer().encode(value).toString();
  }
  if (value is DeviceModeEVStation) {
    return DeviceModeEVStationTypeTransformer().encode(value).toString();
  }
  if (value is DeviceModeFull) {
    return DeviceModeFullTypeTransformer().encode(value).toString();
  }
  if (value is DeviceModeHeatPump) {
    return DeviceModeHeatPumpTypeTransformer().encode(value).toString();
  }
  if (value is DeviceModePlug) {
    return DeviceModePlugTypeTransformer().encode(value).toString();
  }
  if (value is DeviceOptimizationMode) {
    return DeviceOptimizationModeTypeTransformer().encode(value).toString();
  }
  if (value is DeviceSort) {
    return DeviceSortTypeTransformer().encode(value).toString();
  }
  if (value is DeviceState) {
    return DeviceStateTypeTransformer().encode(value).toString();
  }
  if (value is DeviceType) {
    return DeviceTypeTypeTransformer().encode(value).toString();
  }
  if (value is EVStationChargingMode) {
    return EVStationChargingModeTypeTransformer().encode(value).toString();
  }
  if (value is ScheduledDeviceMode) {
    return ScheduledDeviceModeTypeTransformer().encode(value).toString();
  }
  if (value is SwitchState) {
    return SwitchStateTypeTransformer().encode(value).toString();
  }
  if (value is ThresholdType) {
    return ThresholdTypeTypeTransformer().encode(value).toString();
  }
  if (value is UserCountry) {
    return UserCountryTypeTransformer().encode(value).toString();
  }
  if (value is UserCurrency) {
    return UserCurrencyTypeTransformer().encode(value).toString();
  }
  if (value is UserDistances) {
    return UserDistancesTypeTransformer().encode(value).toString();
  }
  if (value is UserLanguage) {
    return UserLanguageTypeTransformer().encode(value).toString();
  }
  if (value is UserTemperature) {
    return UserTemperatureTypeTransformer().encode(value).toString();
  }
  if (value is VisibleSwitchState) {
    return VisibleSwitchStateTypeTransformer().encode(value).toString();
  }
  return value.toString();
}

/// Returns the decoded body as UTF-8 if the given headers indicate an 'application/json'
/// content type. Otherwise, returns the decoded body as decoded by dart:http package.
Future<String> _decodeBodyBytes(Response response) async {
  final contentType = response.headers['content-type'];
  return contentType != null && contentType.toLowerCase().startsWith('application/json')
    ? utf8.decode(response.bodyBytes)
    : response.body;
}
