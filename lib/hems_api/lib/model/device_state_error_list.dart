//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class DeviceStateErrorList {
  /// Returns a new [DeviceStateErrorList] instance.
  DeviceStateErrorList({
    this.timestamp,
    this.errors = const [],
  });

  String? timestamp;

  List<String> errors;

  @override
  bool operator ==(Object other) => identical(this, other) || other is DeviceStateErrorList &&
     other.timestamp == timestamp &&
     other.errors == errors;

  @override
  int get hashCode =>
    (timestamp == null ? 0 : timestamp.hashCode) +
    (errors == null ? 0 : errors.hashCode);

  @override
  String toString() => 'DeviceStateErrorList[timestamp=$timestamp, errors=$errors]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (timestamp != null) {
      json[r'timestamp'] = timestamp;
    }
    if (errors != null) {
      json[r'errors'] = errors;
    }
    return json;
  }

  /// Returns a new [DeviceStateErrorList] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static DeviceStateErrorList fromJson(Map<String, dynamic> json) =>
      DeviceStateErrorList(
        timestamp: json[r'timestamp'],
        errors: json[r'errors'] == null
          ? []
          : (json[r'errors'] as List).cast<String>(),
    );

  static List<DeviceStateErrorList>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <DeviceStateErrorList>[]
      : json.map((dynamic value) => DeviceStateErrorList.fromJson(value)).toList(growable: true == growable);

  static Map<String, DeviceStateErrorList> mapFromJson(Map<String, dynamic> json) {
    final map = <String, DeviceStateErrorList>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = DeviceStateErrorList.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of DeviceStateErrorList-objects as value to a dart map
  static Map<String, List<DeviceStateErrorList?>?> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<DeviceStateErrorList?>?>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = DeviceStateErrorList.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

