//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class EVStationValues {
  /// Returns a new [EVStationValues] instance.
  EVStationValues({
    required this.car,
    this.gridLimit,
    required this.lowestPower,
    required this.maxPower,
    this.name,
    required this.optimizationMode,
  });

  Car car;

  /// Power limit (in watt) for use in grid optimized use case.
  num? gridLimit;

  /// Lower threshold for excess charging in watt. When pv excess reaches this value, the wallbox is set to charging.
  num lowestPower;

  /// Maximum charging power in watt.
  num maxPower;

  /// The devices displayed name (changeable by the user).
  String? name;

  DeviceOptimizationMode optimizationMode;

  @override
  bool operator ==(Object other) => identical(this, other) || other is EVStationValues &&
     other.car == car &&
     other.gridLimit == gridLimit &&
     other.lowestPower == lowestPower &&
     other.maxPower == maxPower &&
     other.name == name &&
     other.optimizationMode == optimizationMode;

  @override
  int get hashCode =>
    (car == null ? 0 : car.hashCode) +
    (gridLimit == null ? 0 : gridLimit.hashCode) +
    (lowestPower == null ? 0 : lowestPower.hashCode) +
    (maxPower == null ? 0 : maxPower.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (optimizationMode == null ? 0 : optimizationMode.hashCode);

  @override
  String toString() => 'EVStationValues[car=$car, gridLimit=$gridLimit, lowestPower=$lowestPower, maxPower=$maxPower, name=$name, optimizationMode=$optimizationMode]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'car'] = car;
      json[r'grid_limit'] = gridLimit;
      json[r'lowest_power'] = lowestPower;
      json[r'max_power'] = maxPower;
    if (name != null) {
      json[r'name'] = name;
    }
      json[r'optimization_mode'] = optimizationMode;
    return json;
  }

  /// Returns a new [EVStationValues] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static EVStationValues fromJson(Map<String, dynamic> json) =>
      EVStationValues(
        car: Car.fromJson(json[r'car']),
        gridLimit: json[r'grid_limit'] == null ?
          null :
          json[r'grid_limit'].toDouble(),
        lowestPower: json[r'lowest_power'] == null ?
          null :
          json[r'lowest_power'].toDouble(),
        maxPower: json[r'max_power'] == null ?
          null :
          json[r'max_power'].toDouble(),
        name: json[r'name'],
        optimizationMode: DeviceOptimizationMode.fromJson(json[r'optimization_mode']),
    );

  static List<EVStationValues>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <EVStationValues>[]
      : json.map((dynamic value) => EVStationValues.fromJson(value)).toList(growable: true == growable);

  static Map<String, EVStationValues> mapFromJson(Map<String, dynamic> json) {
    final map = <String, EVStationValues>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = EVStationValues.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of EVStationValues-objects as value to a dart map
  static Map<String, List<EVStationValues?>?> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<EVStationValues?>?>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = EVStationValues.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

