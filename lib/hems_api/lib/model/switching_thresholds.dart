//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class SwitchingThresholds {
  /// Returns a new [SwitchingThresholds] instance.
  SwitchingThresholds({
    this.powerLevels = const [],
  });

  /// Sorted list. The accepted power levels of the device: * For switchable devices which can only be switched on an off, this value holds a single entry serving as   a threshold indicating how much power the device will consume, and at which excess level the device shall   be switched on. * For controllable devices which can be controlled in a range, this value holds two entries serving as the   minimum switch threshold and the maximum power which can be consumed by the device. Any power larger than   the maximum will be given to lower-prioritized devices (if any). * For controllable devices which accept distinct power levels (i.e. 500 - 1000 - 1500 - 2000 Watts), this   list contains all discrete power levels which are acceptable by the device, where the first entry is the   switch threshold, any the other values are the accepted levels. Any power larger than the maximum will be   given to lower-prioritized devices (if any).
  List<num> powerLevels;

  @override
  bool operator ==(Object other) => identical(this, other) || other is SwitchingThresholds &&
     other.powerLevels == powerLevels;

  @override
  int get hashCode =>
    (powerLevels == null ? 0 : powerLevels.hashCode);

  @override
  String toString() => 'SwitchingThresholds[powerLevels=$powerLevels]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'powerLevels'] = powerLevels;
    return json;
  }

  /// Returns a new [SwitchingThresholds] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static SwitchingThresholds fromJson(Map<String, dynamic> json) =>
      SwitchingThresholds(
        powerLevels: json[r'powerLevels'] == null
          ? []
          : (json[r'powerLevels'] as List).cast<num>(),
    );

  static List<SwitchingThresholds> listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? [] : <SwitchingThresholds>[]
      : json.map((dynamic value) => SwitchingThresholds.fromJson(value)).toList(growable: true == growable);

  static Map<String, SwitchingThresholds> mapFromJson(Map<String, dynamic> json) {
    final map = <String, SwitchingThresholds>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = SwitchingThresholds.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of SwitchingThresholds-objects as value to a dart map
  static Map<String, List<SwitchingThresholds>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<SwitchingThresholds>>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = SwitchingThresholds.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

