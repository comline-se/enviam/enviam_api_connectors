//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//


// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class UserDistances {
  /// Instantiate a new enum with the provided [value].
  const UserDistances._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const METRIC = UserDistances._(r'METRIC');
  static const IMPERIAL = UserDistances._(r'IMPERIAL');

  /// List of all possible values in this [enum][UserDistances].
  static const values = <UserDistances>[
    METRIC,
    IMPERIAL,
  ];

  static UserDistances fromJson(dynamic value) =>
    UserDistancesTypeTransformer().decode(value);

  static List<UserDistances?>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <UserDistances>[]
      : json
          .map((value) => UserDistances.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [UserDistances] to String,
/// and [decode] dynamic data back to [UserDistances].
class UserDistancesTypeTransformer {
  const UserDistancesTypeTransformer._();

  factory UserDistancesTypeTransformer() => _instance;

  String encode(UserDistances data) => data.value;

  /// Decodes a [dynamic value][data] to a UserDistances.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  UserDistances decode(dynamic data) {
    switch (data) {
      case r'METRIC': return UserDistances.METRIC;
      case r'IMPERIAL': return UserDistances.IMPERIAL;
      default:
        return UserDistances.METRIC;
    }
  }

  /// Singleton [UserDistancesTypeTransformer] instance.
  static UserDistancesTypeTransformer _instance = UserDistancesTypeTransformer._();
}
