//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

/// Current mode of the plug.
class DeviceModePlug {
  /// Instantiate a new enum with the provided [value].
  const DeviceModePlug._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const OFF = DeviceModePlug._(r'OFF');
  static const ON = DeviceModePlug._(r'ON');

  /// List of all possible values in this [enum][DeviceModePlug].
  static const values = <DeviceModePlug>[
    OFF,
    ON,
  ];

  static DeviceModePlug fromJson(dynamic value) =>
    DeviceModePlugTypeTransformer().decode(value);

  static List<DeviceModePlug>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <DeviceModePlug>[]
      : json
          .map((value) => DeviceModePlug.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [DeviceModePlug] to String,
/// and [decode] dynamic data back to [DeviceModePlug].
class DeviceModePlugTypeTransformer {
  const DeviceModePlugTypeTransformer._();

  factory DeviceModePlugTypeTransformer() => _instance;

  String encode(DeviceModePlug data) => data.value;

  /// Decodes a [dynamic value][data] to a DeviceModePlug.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  DeviceModePlug decode(dynamic data) {
    switch (data) {
      case r'OFF': return DeviceModePlug.OFF;
      case r'ON': return DeviceModePlug.ON;
      default:
        throw ArgumentError('Unknown enum value to decode: $data');
    }
  }

  /// Singleton [DeviceModePlugTypeTransformer] instance.
  static DeviceModePlugTypeTransformer _instance = DeviceModePlugTypeTransformer._();
}
