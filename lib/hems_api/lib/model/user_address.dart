//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//


// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class UserAddress {
  /// Returns a new [UserAddress] instance.
  UserAddress({
    this.street,
    this.number,
  });

  String? street;

  String? number;

  @override
  bool operator ==(Object other) => identical(this, other) || other is UserAddress &&
     other.street == street &&
     other.number == number;

  @override
  int get hashCode =>
    (street == null ? 0 : street.hashCode) +
    (number == null ? 0 : number.hashCode);

  @override
  String toString() => 'UserAddress[street=$street, number=$number]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (street != null) {
      json[r'street'] = street;
    }
    if (number != null) {
      json[r'number'] = number;
    }
    return json;
  }

  /// Returns a new [UserAddress] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static UserAddress fromJson(Map<String, dynamic> json) =>
      UserAddress(
        street: json[r'street'],
        number: json[r'number'],
    );

  static List<UserAddress?>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <UserAddress>[]
      : json.map((dynamic value) => UserAddress.fromJson(value)).toList(growable: true == growable);

  static Map<String, UserAddress> mapFromJson(Map<String, dynamic> json) {
    final map = <String, UserAddress>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = UserAddress.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of UserAddress-objects as value to a dart map
  static Map<String, List<UserAddress?>?> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<UserAddress?>?>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = UserAddress.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

