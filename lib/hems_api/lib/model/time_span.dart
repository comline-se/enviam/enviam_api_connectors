//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class TimeSpan {
  /// Returns a new [TimeSpan] instance.
  TimeSpan({
    required this.mode,
    required this.start,
    required this.end,
  });

  ScheduledDeviceMode mode;

  /// Time of the day in a hh:mm format. Does not include any timezone information. The time should be truncated by the seconds, e.g. 00:59:59 is included in 00:59. 
  String start;

  /// Time of the day in a hh:mm format. Does not include any timezone information. The time should be truncated by the seconds, e.g. 00:59:59 is included in 00:59. 
  String end;

  @override
  bool operator ==(Object other) => identical(this, other) || other is TimeSpan &&
     other.mode == mode &&
     other.start == start &&
     other.end == end;

  @override
  int get hashCode =>
    (mode == null ? 0 : mode.hashCode) +
    (start == null ? 0 : start.hashCode) +
    (end == null ? 0 : end.hashCode);

  @override
  String toString() => 'TimeSpan[mode=$mode, start=$start, end=$end]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'mode'] = mode;
      json[r'start'] = start;
      json[r'end'] = end;
    return json;
  }

  /// Returns a new [TimeSpan] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static TimeSpan fromJson(Map<String, dynamic> json) =>
      TimeSpan(
        mode: ScheduledDeviceMode.fromJson(json[r'mode']),
        start: json[r'start'],
        end: json[r'end'],
    );

  static List<TimeSpan> listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? [] : <TimeSpan>[]
      : json.map((dynamic value) => TimeSpan.fromJson(value)).toList(growable: true == growable);

  static Map<String, TimeSpan> mapFromJson(Map<String, dynamic> json) {
    final map = <String, TimeSpan>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = TimeSpan.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of TimeSpan-objects as value to a dart map
  static Map<String, List<TimeSpan>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<TimeSpan>>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = TimeSpan.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

