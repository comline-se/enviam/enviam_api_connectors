//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

/// The device's category
class DeviceCategory {
  /// Instantiate a new enum with the provided [value].
  const DeviceCategory._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const PLUG = DeviceCategory._(r'PLUG');
  static const HEAT_PUMP = DeviceCategory._(r'HEAT_PUMP');
  static const EV_STATION = DeviceCategory._(r'EV_STATION');
  static const SMART_HEATER = DeviceCategory._(r'SMART_HEATER');

  /// List of all possible values in this [enum][DeviceCategory].
  static const values = <DeviceCategory>[
    PLUG,
    HEAT_PUMP,
    EV_STATION,
    SMART_HEATER,
  ];

  static DeviceCategory fromJson(dynamic value) =>
    DeviceCategoryTypeTransformer().decode(value);

  static List<DeviceCategory>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <DeviceCategory>[]
      : json
          .map((value) => DeviceCategory.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [DeviceCategory] to String,
/// and [decode] dynamic data back to [DeviceCategory].
class DeviceCategoryTypeTransformer {
  const DeviceCategoryTypeTransformer._();

  factory DeviceCategoryTypeTransformer() => _instance;

  String encode(DeviceCategory data) => data.value;

  /// Decodes a [dynamic value][data] to a DeviceCategory.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  DeviceCategory decode(dynamic data) {
    switch (data) {
      case r'PLUG': return DeviceCategory.PLUG;
      case r'HEAT_PUMP': return DeviceCategory.HEAT_PUMP;
      case r'EV_STATION': return DeviceCategory.EV_STATION;
      case r'SMART_HEATER': return DeviceCategory.SMART_HEATER;
      default:
        throw ArgumentError('Unknown enum value to decode: $data');
    }
  }

  /// Singleton [DeviceCategoryTypeTransformer] instance.
  static DeviceCategoryTypeTransformer _instance = DeviceCategoryTypeTransformer._();
}
