//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//


// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class UserProfile {
  /// Returns a new [UserProfile] instance.
  UserProfile({
    this.language,
    this.currency,
    this.temperature,
    this.distances,
    this.feedInTariff,
    this.energyCostsPerKwh,
  });

  UserLanguage? language;

  UserCurrency? currency;

  UserTemperature? temperature;

  UserDistances? distances;

  // minimum: 0
  num? feedInTariff;

  // minimum: 0
  num? energyCostsPerKwh;

  @override
  bool operator ==(Object other) => identical(this, other) || other is UserProfile &&
     other.language == language &&
     other.currency == currency &&
     other.temperature == temperature &&
     other.distances == distances &&
     other.feedInTariff == feedInTariff &&
     other.energyCostsPerKwh == energyCostsPerKwh;

  @override
  int get hashCode =>
    (language == null ? 0 : language.hashCode) +
    (currency == null ? 0 : currency.hashCode) +
    (temperature == null ? 0 : temperature.hashCode) +
    (distances == null ? 0 : distances.hashCode) +
    (feedInTariff == null ? 0 : feedInTariff.hashCode) +
    (energyCostsPerKwh == null ? 0 : energyCostsPerKwh.hashCode);

  @override
  String toString() => 'UserProfile[language=$language, currency=$currency, temperature=$temperature, distances=$distances, feedInTariff=$feedInTariff, energyCostsPerKwh=$energyCostsPerKwh]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (language != null) {
      json[r'language'] = language;
    }
    if (currency != null) {
      json[r'currency'] = currency;
    }
    if (temperature != null) {
      json[r'temperature'] = temperature;
    }
    if (distances != null) {
      json[r'distances'] = distances;
    }
    if (feedInTariff != null) {
      json[r'feed_in_tariff'] = feedInTariff;
    }
    if (energyCostsPerKwh != null) {
      json[r'energy_costs_per_kwh'] = energyCostsPerKwh;
    }
    return json;
  }

  /// Returns a new [UserProfile] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static UserProfile fromJson(Map<String, dynamic> json) =>
      UserProfile(
        language: UserLanguage.fromJson(json[r'language']),
        currency: UserCurrency.fromJson(json[r'currency']),
        temperature: UserTemperature.fromJson(json[r'temperature']),
        distances: UserDistances.fromJson(json[r'distances']),
        feedInTariff: json[r'feed_in_tariff'] == null ?
          null :
          json[r'feed_in_tariff'].toDouble(),
        energyCostsPerKwh: json[r'energy_costs_per_kwh'] == null ?
          null :
          json[r'energy_costs_per_kwh'].toDouble(),
    );

  static List<UserProfile?>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <UserProfile>[]
      : json.map((dynamic value) => UserProfile.fromJson(value)).toList(growable: true == growable);

  static Map<String, UserProfile?> mapFromJson(Map<String, dynamic> json) {
    final map = <String, UserProfile?>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = UserProfile.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of UserProfile-objects as value to a dart map
  static Map<String, List<UserProfile?>?> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<UserProfile?>?>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = UserProfile.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

