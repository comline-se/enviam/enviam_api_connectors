//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//


// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

/// The mode the EV station is in indicating its current status and behavior. These modes are not optimization modes (set with the patch method) but instead these modes are delivered from the wallbox itself. 
class EVStationMode {
  /// Instantiate a new enum with the provided [value].
  const EVStationMode._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const OFFLINE = EVStationMode._(r'OFFLINE');
  static const STANDBY = EVStationMode._(r'STANDBY');
  static const CHARGING = EVStationMode._(r'CHARGING');
  static const CHARGING_PV = EVStationMode._(r'CHARGING_PV');
  static const DISCONNECTED = EVStationMode._(r'DISCONNECTED');
  static const ERROR = EVStationMode._(r'ERROR');
  static const AUTOMATIC = EVStationMode._(r'AUTOMATIC');

  /// List of all possible values in this [enum][EVStationMode].
  static const values = <EVStationMode>[
    AUTOMATIC,
    OFFLINE,
    STANDBY,
    CHARGING,
    CHARGING_PV,
    DISCONNECTED,
    ERROR,
  ];

  static EVStationMode fromJson(dynamic value) =>
    EVStationModeTypeTransformer().decode(value);

  static List<EVStationMode?>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <EVStationMode>[]
      : json
          .map((value) => EVStationMode.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [EVStationMode] to String,
/// and [decode] dynamic data back to [EVStationMode].
class EVStationModeTypeTransformer {
  const EVStationModeTypeTransformer._();

  factory EVStationModeTypeTransformer() => _instance;

  String encode(EVStationMode data) => data.value;

  /// Decodes a [dynamic value][data] to a EVStationMode.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  EVStationMode decode(dynamic data) {
    switch (data) {
      case r'OFFLINE': return EVStationMode.OFFLINE;
      case r'STANDBY': return EVStationMode.STANDBY;
      case r'CHARGING': return EVStationMode.CHARGING;
      case r'CHARGING_PV': return EVStationMode.CHARGING_PV;
      case r'DISCONNECTED': return EVStationMode.DISCONNECTED;
      case r'ERROR': return EVStationMode.ERROR;
      case r'AUTOMATIC': return EVStationMode.AUTOMATIC;
      default:
        return EVStationMode.ERROR;
    }
  }

  /// Singleton [EVStationModeTypeTransformer] instance.
  static EVStationModeTypeTransformer _instance = EVStationModeTypeTransformer._();
}
