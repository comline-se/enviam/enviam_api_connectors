//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class UserLanguage {
  /// Instantiate a new enum with the provided [value].
  const UserLanguage._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const GERMAN = UserLanguage._(r'GERMAN');
  static const ENGLISH = UserLanguage._(r'ENGLISH');
  static const FRENCH = UserLanguage._(r'FRENCH');
  static const ITALIAN = UserLanguage._(r'ITALIAN');
  static const DUTCH = UserLanguage._(r'DUTCH');
  static const SWEDISH = UserLanguage._(r'SWEDISH');
  static const SPANISH = UserLanguage._(r'SPANISH');

  /// List of all possible values in this [enum][UserLanguage].
  static const values = <UserLanguage>[
    GERMAN,
    ENGLISH,
    FRENCH,
    ITALIAN,
    DUTCH,
    SWEDISH,
    SPANISH,
  ];

  static UserLanguage fromJson(dynamic value) =>
    UserLanguageTypeTransformer().decode(value);

  static List<UserLanguage?>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <UserLanguage>[]
      : json
          .map((value) => UserLanguage.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [UserLanguage] to String,
/// and [decode] dynamic data back to [UserLanguage].
class UserLanguageTypeTransformer {
  const UserLanguageTypeTransformer._();

  factory UserLanguageTypeTransformer() => _instance;

  String encode(UserLanguage data) => data.value;

  /// Decodes a [dynamic value][data] to a UserLanguage.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  UserLanguage decode(dynamic data) {
    switch (data) {
      case r'GERMAN': return UserLanguage.GERMAN;
      case r'ENGLISH': return UserLanguage.ENGLISH;
      case r'FRENCH': return UserLanguage.FRENCH;
      case r'ITALIAN': return UserLanguage.ITALIAN;
      case r'DUTCH': return UserLanguage.DUTCH;
      case r'SWEDISH': return UserLanguage.SWEDISH;
      case r'SPANISH': return UserLanguage.SPANISH;
      default:
        return UserLanguage.GERMAN;
    }
  }

  /// Singleton [UserLanguageTypeTransformer] instance.
  static UserLanguageTypeTransformer _instance = UserLanguageTypeTransformer._();
}
