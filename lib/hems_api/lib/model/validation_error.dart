//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//


// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ValidationError {
  /// Returns a new [ValidationError] instance.
  ValidationError({
    required this.message,
    this.path,
  });

  /// Human readable error message.
  String message;

  /// Path to invalid property.
  String? path;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ValidationError &&
     other.message == message &&
     other.path == path;

  @override
  int get hashCode =>
    (message == null ? 0 : message.hashCode) +
    (path == null ? 0 : path.hashCode);

  @override
  String toString() => 'ValidationError[message=$message, path=$path]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'message'] = message;
    if (path != null) {
      json[r'path'] = path;
    }
    return json;
  }

  /// Returns a new [ValidationError] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ValidationError fromJson(Map<String, dynamic> json) =>
      ValidationError(
        message: json[r'message'],
        path: json[r'path'],
    );

  static List<ValidationError?>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <ValidationError>[]
      : json.map((dynamic value) => ValidationError.fromJson(value)).toList(growable: true == growable);

  static Map<String, ValidationError?> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ValidationError?>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = ValidationError.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ValidationError-objects as value to a dart map
  static Map<String, List<ValidationError?>?> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<ValidationError?>?>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = ValidationError.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

