//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//


// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class EVStationAllOf {
  /// Returns a new [EVStationAllOf] instance.
  EVStationAllOf({
    this.availableModes = const [],
    this.car,
    this.connectivityStatus,
    this.gridLimit,
    this.firmware,
    this.manufacturer,
    this.modelCode,
    this.serialNumber,
    this.lowestPower,
    this.maxPower,
    this.optimizationMode,
    this.powerAcInLimit,
    this.powerAcInMax,
    this.powerAcOutLimit,
    this.powerAcOutMax,
  });

  /// The modes that are available for the EV station device.
  List<DeviceModeEVStation>? availableModes;

  Car? car;

  /// Current connectivity status for the device.
  String? connectivityStatus;

  /// Power limit (in watt) for use in grid optimized use case.
  num? gridLimit;

  /// The devices firmware ID.
  String? firmware;

  /// The device manufacturer id.
  String? manufacturer;

  /// The devices ID model code.
  String? modelCode;

  /// The devices serial number ID.
  String? serialNumber;

  /// Lower threshold for excess charging in watt. When pv excess reaches this value, the wallbox is set to charging.
  num? lowestPower;

  /// Maximum charging power in watt.
  num? maxPower;

  DeviceOptimizationMode? optimizationMode;

  /// Currently set power AC input limit set for the device.
  num? powerAcInLimit;

  /// Maximum power AC input currently set for the device.
  num? powerAcInMax;

  /// Currently set power AC output value set for the device.
  num? powerAcOutLimit;

  /// Maximum power AC output currently set for the device.
  num? powerAcOutMax;

  @override
  bool operator ==(Object other) => identical(this, other) || other is EVStationAllOf &&
     other.availableModes == availableModes &&
     other.car == car &&
     other.connectivityStatus == connectivityStatus &&
     other.gridLimit == gridLimit &&
     other.firmware == firmware &&
     other.manufacturer == manufacturer &&
     other.modelCode == modelCode &&
     other.serialNumber == serialNumber &&
     other.lowestPower == lowestPower &&
     other.maxPower == maxPower &&
     other.optimizationMode == optimizationMode &&
     other.powerAcInLimit == powerAcInLimit &&
     other.powerAcInMax == powerAcInMax &&
     other.powerAcOutLimit == powerAcOutLimit &&
     other.powerAcOutMax == powerAcOutMax;

  @override
  int get hashCode =>
    (availableModes == null ? 0 : availableModes.hashCode) +
    (car == null ? 0 : car.hashCode) +
    (connectivityStatus == null ? 0 : connectivityStatus.hashCode) +
    (gridLimit == null ? 0 : gridLimit.hashCode) +
    (firmware == null ? 0 : firmware.hashCode) +
    (manufacturer == null ? 0 : manufacturer.hashCode) +
    (modelCode == null ? 0 : modelCode.hashCode) +
    (serialNumber == null ? 0 : serialNumber.hashCode) +
    (lowestPower == null ? 0 : lowestPower.hashCode) +
    (maxPower == null ? 0 : maxPower.hashCode) +
    (optimizationMode == null ? 0 : optimizationMode.hashCode) +
    (powerAcInLimit == null ? 0 : powerAcInLimit.hashCode) +
    (powerAcInMax == null ? 0 : powerAcInMax.hashCode) +
    (powerAcOutLimit == null ? 0 : powerAcOutLimit.hashCode) +
    (powerAcOutMax == null ? 0 : powerAcOutMax.hashCode);

  @override
  String toString() => 'EVStationAllOf[availableModes=$availableModes, car=$car, connectivityStatus=$connectivityStatus, gridLimit=$gridLimit, firmware=$firmware, manufacturer=$manufacturer, modelCode=$modelCode, serialNumber=$serialNumber, lowestPower=$lowestPower, maxPower=$maxPower, optimizationMode=$optimizationMode, powerAcInLimit=$powerAcInLimit, powerAcInMax=$powerAcInMax, powerAcOutLimit=$powerAcOutLimit, powerAcOutMax=$powerAcOutMax]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (availableModes != null) {
      json[r'available_modes'] = availableModes;
    }
    if (car != null) {
      json[r'car'] = car;
    }
    if (connectivityStatus != null) {
      json[r'connectivity_status'] = connectivityStatus;
    }
    if (gridLimit != null) {
      json[r'grid_limit'] = gridLimit;
    }
    if (firmware != null) {
      json[r'firmware'] = firmware;
    }
    if (manufacturer != null) {
      json[r'manufacturer'] = manufacturer;
    }
    if (modelCode != null) {
      json[r'model_code'] = modelCode;
    }
    if (serialNumber != null) {
      json[r'serial_number'] = serialNumber;
    }
    if (lowestPower != null) {
      json[r'lowest_power'] = lowestPower;
    }
    if (maxPower != null) {
      json[r'max_power'] = maxPower;
    }
    if (optimizationMode != null) {
      json[r'optimization_mode'] = optimizationMode;
    }
    if (powerAcInLimit != null) {
      json[r'power_ac_in_limit'] = powerAcInLimit;
    }
    if (powerAcInMax != null) {
      json[r'power_ac_in_max'] = powerAcInMax;
    }
    if (powerAcOutLimit != null) {
      json[r'power_ac_out_limit'] = powerAcOutLimit;
    }
    if (powerAcOutMax != null) {
      json[r'power_ac_out_max'] = powerAcOutMax;
    }
    return json;
  }

  /// Returns a new [EVStationAllOf] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static EVStationAllOf fromJson(Map<String, dynamic> json) =>
      EVStationAllOf(
        availableModes: json[r'available_modes'],
        car: Car.fromJson(json[r'car']),
        connectivityStatus: json[r'connectivity_status'],
        gridLimit: json[r'grid_limit'] == null ?
          null :
          json[r'grid_limit'].toDouble(),
        firmware: json[r'firmware'],
        manufacturer: json[r'manufacturer'],
        modelCode: json[r'model_code'],
        serialNumber: json[r'serial_number'],
        lowestPower: json[r'lowest_power'] == null ?
          null :
          json[r'lowest_power'].toDouble(),
        maxPower: json[r'max_power'] == null ?
          null :
          json[r'max_power'].toDouble(),
        optimizationMode: DeviceOptimizationMode.fromJson(json[r'optimization_mode']),
        powerAcInLimit: json[r'power_ac_in_limit'] == null ?
          null :
          json[r'power_ac_in_limit'].toDouble(),
        powerAcInMax: json[r'power_ac_in_max'] == null ?
          null :
          json[r'power_ac_in_max'].toDouble(),
        powerAcOutLimit: json[r'power_ac_out_limit'] == null ?
          null :
          json[r'power_ac_out_limit'].toDouble(),
        powerAcOutMax: json[r'power_ac_out_max'] == null ?
          null :
          json[r'power_ac_out_max'].toDouble(),
    );

  static List<EVStationAllOf?>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <EVStationAllOf>[]
      : json.map((dynamic value) => EVStationAllOf.fromJson(value)).toList(growable: true == growable);

  static Map<String, EVStationAllOf?> mapFromJson(Map<String, dynamic> json) {
    final map = <String, EVStationAllOf?>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = EVStationAllOf.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of EVStationAllOf-objects as value to a dart map
  static Map<String, List<EVStationAllOf?>?> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<EVStationAllOf?>?>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = EVStationAllOf.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

