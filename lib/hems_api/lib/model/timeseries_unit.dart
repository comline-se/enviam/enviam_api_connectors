//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

/// The unit of a timeseries:  * UNKNOWN: Unit not determineable  * WATT: Watt  * WATTHOUR: Watthours  * PERCENT: Percent  * CURRENCY: Currency 
class TimeseriesUnit {
  /// Instantiate a new enum with the provided [value].
  const TimeseriesUnit._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const UNKNOWN = TimeseriesUnit._(r'UNKNOWN');
  static const WATT = TimeseriesUnit._(r'WATT');
  static const WATTHOUR = TimeseriesUnit._(r'WATTHOUR');
  static const PERCENT = TimeseriesUnit._(r'PERCENT');
  static const CURRENCY = TimeseriesUnit._(r'CURRENCY');

  /// List of all possible values in this [enum][TimeseriesUnit].
  static const values = <TimeseriesUnit>[
    UNKNOWN,
    WATT,
    WATTHOUR,
    PERCENT,
    CURRENCY,
  ];

  static TimeseriesUnit fromJson(dynamic value) =>
    TimeseriesUnitTypeTransformer().decode(value);

  static List<TimeseriesUnit> listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? [] : <TimeseriesUnit>[]
      : json
          .map((value) => TimeseriesUnit.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [TimeseriesUnit] to String,
/// and [decode] dynamic data back to [TimeseriesUnit].
class TimeseriesUnitTypeTransformer {
  const TimeseriesUnitTypeTransformer._();

  factory TimeseriesUnitTypeTransformer() => _instance;

  String encode(TimeseriesUnit data) => data.value;

  /// Decodes a [dynamic value][data] to a TimeseriesUnit.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  TimeseriesUnit decode(dynamic data) {
    switch (data) {
      case r'UNKNOWN': return TimeseriesUnit.UNKNOWN;
      case r'WATT': return TimeseriesUnit.WATT;
      case r'WATTHOUR': return TimeseriesUnit.WATTHOUR;
      case r'PERCENT': return TimeseriesUnit.PERCENT;
      case r'CURRENCY': return TimeseriesUnit.CURRENCY;
      default:
        return TimeseriesUnit.UNKNOWN;
    }
  }

  /// Singleton [TimeseriesUnitTypeTransformer] instance.
  static TimeseriesUnitTypeTransformer _instance = TimeseriesUnitTypeTransformer._();
}
