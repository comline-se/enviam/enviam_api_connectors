//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//


// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class EVStationChargingModeRequest {
  /// Returns a new [EVStationChargingModeRequest] instance.
  EVStationChargingModeRequest({
    required this.mode,
  });

  EVStationChargingMode mode;

  @override
  bool operator ==(Object other) => identical(this, other) || other is EVStationChargingModeRequest &&
     other.mode == mode;

  @override
  int get hashCode =>
    mode.hashCode;

  @override
  String toString() => 'EVStationChargingModeRequest[mode=$mode]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'mode'] = mode;
    return json;
  }

  /// Returns a new [EVStationChargingModeRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static EVStationChargingModeRequest fromJson(Map<String, dynamic> json) =>
      EVStationChargingModeRequest(
        mode: EVStationChargingMode.fromJson(json[r'mode']),
    );

  static List<EVStationChargingModeRequest?>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <EVStationChargingModeRequest>[]
      : json.map((dynamic value) => EVStationChargingModeRequest.fromJson(value)).toList(growable: true == growable);

  static Map<String, EVStationChargingModeRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, EVStationChargingModeRequest>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = EVStationChargingModeRequest.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of EVStationChargingModeRequest-objects as value to a dart map
  static Map<String, List<EVStationChargingModeRequest?>?> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<EVStationChargingModeRequest?>?>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = EVStationChargingModeRequest.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

