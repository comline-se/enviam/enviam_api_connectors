//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class SunAltitudes {
  /// Returns a new [SunAltitudes] instance.
  SunAltitudes({
    required this.sunrise,
    required this.sunhigh,
    required this.sunset,
  });

  /// Time of the day in a hh:mm format. Does not include any timezone information. The time should be truncated by the seconds, e.g. 00:59:59 is included in 00:59. 
  String sunrise;

  /// Time of the day in a hh:mm format. Does not include any timezone information. The time should be truncated by the seconds, e.g. 00:59:59 is included in 00:59. 
  String sunhigh;

  /// Time of the day in a hh:mm format. Does not include any timezone information. The time should be truncated by the seconds, e.g. 00:59:59 is included in 00:59. 
  String sunset;

  @override
  bool operator ==(Object other) => identical(this, other) || other is SunAltitudes &&
     other.sunrise == sunrise &&
     other.sunhigh == sunhigh &&
     other.sunset == sunset;

  @override
  int get hashCode =>
    (sunrise == null ? 0 : sunrise.hashCode) +
    (sunhigh == null ? 0 : sunhigh.hashCode) +
    (sunset == null ? 0 : sunset.hashCode);

  @override
  String toString() => 'SunAltitudes[sunrise=$sunrise, sunhigh=$sunhigh, sunset=$sunset]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'sunrise'] = sunrise;
      json[r'sunhigh'] = sunhigh;
      json[r'sunset'] = sunset;
    return json;
  }

  /// Returns a new [SunAltitudes] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static SunAltitudes fromJson(Map<String, dynamic> json) =>
      SunAltitudes(
        sunrise: json[r'sunrise'],
        sunhigh: json[r'sunhigh'],
        sunset: json[r'sunset'],
    );

  static List<SunAltitudes> listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? [] : <SunAltitudes>[]
      : json.map((dynamic value) => SunAltitudes.fromJson(value)).toList(growable: true == growable);

  static Map<String, SunAltitudes> mapFromJson(Map<String, dynamic> json) {
    final map = <String, SunAltitudes>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = SunAltitudes.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of SunAltitudes-objects as value to a dart map
  static Map<String, List<SunAltitudes>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<SunAltitudes>>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = SunAltitudes.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

