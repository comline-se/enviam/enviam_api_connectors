//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class DeviceConfig {
  /// Returns a new [DeviceConfig] instance.
  DeviceConfig({
    required this.optimizationEnabled,
    required this.switchingThresholds,
    this.minRuntime = 0,
    this.mustRuntime = 0,
    this.minResttime = 0,
    this.evStationConfig,
    this.userImportance = 0,
  });

  /// Flag to indicate if the pv optimization is enabled at all.
  bool optimizationEnabled;

  SwitchingThresholds switchingThresholds;

  /// Minimal duration in minutes a device needs to run after it was turned on.
  // minimum: 0
  // maximum: 1440
  int minRuntime;

  /// Minimal duration in minutes a device needs to run over the time of one day.
  // minimum: 0
  // maximum: 1440
  int mustRuntime;

  /// Minimal duration in minutes a device needs to be off after it was turned off.
  // minimum: 0
  // maximum: 1440
  int minResttime;

  EvStationConfig? evStationConfig;

  /// Priority of the device during pv optimization, devices with higher numbers have precedence compares to other pv optimized devices with lower numbers.
  // minimum: 0
  int userImportance;

  @override
  bool operator ==(Object other) => identical(this, other) || other is DeviceConfig &&
     other.optimizationEnabled == optimizationEnabled &&
     other.switchingThresholds == switchingThresholds &&
     other.minRuntime == minRuntime &&
     other.mustRuntime == mustRuntime &&
     other.minResttime == minResttime &&
     other.evStationConfig == evStationConfig &&
     other.userImportance == userImportance;

  @override
  int get hashCode =>
    (optimizationEnabled == null ? 0 : optimizationEnabled.hashCode) +
    (switchingThresholds == null ? 0 : switchingThresholds.hashCode) +
    (minRuntime == null ? 0 : minRuntime.hashCode) +
    (mustRuntime == null ? 0 : mustRuntime.hashCode) +
    (minResttime == null ? 0 : minResttime.hashCode) +
    (evStationConfig == null ? 0 : evStationConfig.hashCode) +
    (userImportance == null ? 0 : userImportance.hashCode);

  @override
  String toString() => 'DeviceConfig[optimizationEnabled=$optimizationEnabled, switchingThresholds=$switchingThresholds, minRuntime=$minRuntime, mustRuntime=$mustRuntime, minResttime=$minResttime, evStationConfig=$evStationConfig, userImportance=$userImportance]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'optimizationEnabled'] = optimizationEnabled;
      json[r'switchingThresholds'] = switchingThresholds;
    if (minRuntime != null) {
      json[r'minRuntime'] = minRuntime;
    }
    if (mustRuntime != null) {
      json[r'mustRuntime'] = mustRuntime;
    }
    if (minResttime != null) {
      json[r'minResttime'] = minResttime;
    }
    if (evStationConfig != null) {
      json[r'evStationConfig'] = evStationConfig;
    }
    if (userImportance != null) {
      json[r'userImportance'] = userImportance;
    }
    return json;
  }

  /// Returns a new [DeviceConfig] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static DeviceConfig fromJson(Map<String, dynamic> json) =>
      DeviceConfig(
        optimizationEnabled: json[r'optimizationEnabled'],
        switchingThresholds: SwitchingThresholds.fromJson(json[r'switchingThresholds']),
        minRuntime: json[r'minRuntime'],
        mustRuntime: json[r'mustRuntime'],
        minResttime: json[r'minResttime'],
        evStationConfig: EvStationConfig.fromJson(json[r'evStationConfig']),
        userImportance: json[r'userImportance'],
    );

  static List<DeviceConfig>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <DeviceConfig>[]
      : json.map((dynamic value) => DeviceConfig.fromJson(value)).toList(growable: true == growable);

  static Map<String, DeviceConfig> mapFromJson(Map<String, dynamic> json) {
    final map = <String, DeviceConfig>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = DeviceConfig.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of DeviceConfig-objects as value to a dart map
  static Map<String, List<DeviceConfig?>?> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<DeviceConfig?>?>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = DeviceConfig.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

