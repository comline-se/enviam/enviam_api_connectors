//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

/// Switch state of the device, e.g. is it turned ON or OFF?
class SwitchState {
  /// Instantiate a new enum with the provided [value].
  const SwitchState._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const ON = SwitchState._(r'ON');
  static const OFF = SwitchState._(r'OFF');

  /// List of all possible values in this [enum][SwitchState].
  static const values = <SwitchState>[
    ON,
    OFF,
  ];

  static SwitchState fromJson(dynamic value) =>
    SwitchStateTypeTransformer().decode(value);

  static List<SwitchState>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <SwitchState>[]
      : json
          .map((value) => SwitchState.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [SwitchState] to String,
/// and [decode] dynamic data back to [SwitchState].
class SwitchStateTypeTransformer {
  const SwitchStateTypeTransformer._();

  factory SwitchStateTypeTransformer() => _instance;

  String encode(SwitchState data) => data.value;

  /// Decodes a [dynamic value][data] to a SwitchState.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  SwitchState decode(dynamic data) {
    switch (data) {
      case r'ON': return SwitchState.ON;
      case r'OFF': return SwitchState.OFF;
      default:
        throw ArgumentError('Unknown enum value to decode: $data');
    }
  }

  /// Singleton [SwitchStateTypeTransformer] instance.
  static SwitchStateTypeTransformer _instance = SwitchStateTypeTransformer._();
}
