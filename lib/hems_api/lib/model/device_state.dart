//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

/// The device state indication proper behavior or potential problems.
class DeviceState {
  /// Instantiate a new enum with the provided [value].
  const DeviceState._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const FATAL = DeviceState._(r'FATAL');
  static const ERROR = DeviceState._(r'ERROR');
  static const WARN = DeviceState._(r'WARN');
  static const OK = DeviceState._(r'OK');

  /// List of all possible values in this [enum][DeviceState].
  static const values = <DeviceState>[
    FATAL,
    ERROR,
    WARN,
    OK,
  ];

  static DeviceState fromJson(dynamic value) =>
    DeviceStateTypeTransformer().decode(value);

  static List<DeviceState?>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <DeviceState>[]
      : json
          .map((value) => DeviceState.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [DeviceState] to String,
/// and [decode] dynamic data back to [DeviceState].
class DeviceStateTypeTransformer {
  const DeviceStateTypeTransformer._();

  factory DeviceStateTypeTransformer() => _instance;

  String encode(DeviceState data) => data.value;

  /// Decodes a [dynamic value][data] to a DeviceState.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  DeviceState decode(dynamic data) {
    switch (data) {
      case r'FATAL': return DeviceState.FATAL;
      case r'ERROR': return DeviceState.ERROR;
      case r'WARN': return DeviceState.WARN;
      case r'OK': return DeviceState.OK;
      default:
        return DeviceState.ERROR;
    }
  }

  /// Singleton [DeviceStateTypeTransformer] instance.
  static DeviceStateTypeTransformer _instance = DeviceStateTypeTransformer._();
}
