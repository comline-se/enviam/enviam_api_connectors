//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class UserCurrency {
  /// Instantiate a new enum with the provided [value].
  const UserCurrency._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const EUR = UserCurrency._(r'EUR');
  static const USD = UserCurrency._(r'USD');
  static const GBP = UserCurrency._(r'GBP');
  static const AUD = UserCurrency._(r'AUD');
  static const CHF = UserCurrency._(r'CHF');
  static const SEK = UserCurrency._(r'SEK');

  /// List of all possible values in this [enum][UserCurrency].
  static const values = <UserCurrency>[
    EUR,
    USD,
    GBP,
    AUD,
    CHF,
    SEK,
  ];

  static UserCurrency fromJson(dynamic value) =>
    UserCurrencyTypeTransformer().decode(value);

  static List<UserCurrency>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <UserCurrency>[]
      : json
          .map((value) => UserCurrency.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [UserCurrency] to String,
/// and [decode] dynamic data back to [UserCurrency].
class UserCurrencyTypeTransformer {
  const UserCurrencyTypeTransformer._();

  factory UserCurrencyTypeTransformer() => _instance;

  String encode(UserCurrency data) => data.value;

  /// Decodes a [dynamic value][data] to a UserCurrency.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  UserCurrency decode(dynamic data) {
    switch (data) {
      case r'EUR': return UserCurrency.EUR;
      case r'USD': return UserCurrency.USD;
      case r'GBP': return UserCurrency.GBP;
      case r'AUD': return UserCurrency.AUD;
      case r'CHF': return UserCurrency.CHF;
      case r'SEK': return UserCurrency.SEK;
      default:
        return UserCurrency.EUR;
    }
  }

  /// Singleton [UserCurrencyTypeTransformer] instance.
  static UserCurrencyTypeTransformer _instance = UserCurrencyTypeTransformer._();
}
