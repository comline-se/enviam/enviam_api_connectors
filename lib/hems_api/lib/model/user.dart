//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//


// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class User {
  /// Returns a new [User] instance.
  User({
    this.firstName,
    this.lastName,
    this.address,
    this.zipCode,
    this.city,
    this.country,
    this.email,
  });

  String? firstName;

  String? lastName;

  UserAddress? address;

  String? zipCode;

  String? city;

  UserCountry? country;

  String? email;

  @override
  bool operator ==(Object other) => identical(this, other) || other is User &&
     other.firstName == firstName &&
     other.lastName == lastName &&
     other.address == address &&
     other.zipCode == zipCode &&
     other.city == city &&
     other.country == country &&
     other.email == email;

  @override
  int get hashCode =>
    (firstName == null ? 0 : firstName.hashCode) +
    (lastName == null ? 0 : lastName.hashCode) +
    (address == null ? 0 : address.hashCode) +
    (zipCode == null ? 0 : zipCode.hashCode) +
    (city == null ? 0 : city.hashCode) +
    (country == null ? 0 : country.hashCode) +
    (email == null ? 0 : email.hashCode);

  @override
  String toString() => 'User[firstName=$firstName, lastName=$lastName, address=$address, zipCode=$zipCode, city=$city, country=$country, email=$email]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (firstName != null) {
      json[r'first_name'] = firstName;
    }
    if (lastName != null) {
      json[r'last_name'] = lastName;
    }
    if (address != null) {
      json[r'address'] = address;
    }
    if (zipCode != null) {
      json[r'zip_code'] = zipCode;
    }
    if (city != null) {
      json[r'city'] = city;
    }
    if (country != null) {
      json[r'country'] = country;
    }
    if (email != null) {
      json[r'email'] = email;
    }
    return json;
  }

  /// Returns a new [User] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static User fromJson(Map<String, dynamic> json) =>
      User(
        firstName: json[r'first_name'],
        lastName: json[r'last_name'],
        address: UserAddress.fromJson(json[r'address']),
        zipCode: json[r'zip_code'],
        city: json[r'city'],
        country: json[r'country'],
        email: json[r'email'],
    );

  static List<User?>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <User>[]
      : json.map((dynamic value) => User.fromJson(value)).toList(growable: true == growable);

  static Map<String, User?> mapFromJson(Map<String, dynamic> json) {
    final map = <String, User?>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = User.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of User-objects as value to a dart map
  static Map<String, List<User?>?> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<User?>?>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = User.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

