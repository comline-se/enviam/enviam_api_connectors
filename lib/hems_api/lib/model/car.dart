//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Car {
  /// Returns a new [Car] instance.
  Car({
    this.name,
    this.capacity,
    this.departureTime,
  });

  /// The name of the car read by the wallbox.
  String? name;

  /// The capacity if the cars battery.
  num? capacity;

  /// The set up departure time for the car.
  num? departureTime;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Car &&
     other.name == name &&
     other.capacity == capacity &&
     other.departureTime == departureTime;

  @override
  int get hashCode =>
    (name == null ? 0 : name.hashCode) +
    (capacity == null ? 0 : capacity.hashCode) +
    (departureTime == null ? 0 : departureTime.hashCode);

  @override
  String toString() => 'Car[name=$name, capacity=$capacity, departureTime=$departureTime]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (name != null) {
      json[r'name'] = name;
    }
    if (capacity != null) {
      json[r'capacity'] = capacity;
    }
    if (departureTime != null) {
      json[r'departure_time'] = departureTime;
    }
    return json;
  }

  /// Returns a new [Car] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Car fromJson(Map<String, dynamic> json) =>
      Car(
        name: json[r'name'],
        capacity: json[r'capacity'] == null ?
          null :
          json[r'capacity'].toDouble(),
        departureTime: json[r'departure_time'],
    );

  static List<Car?>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <Car>[]
      : json.map((dynamic value) => Car.fromJson(value)).toList(growable: true == growable);

  static Map<String, Car?> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Car?>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = Car.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of Car-objects as value to a dart map
  static Map<String, List<Car?>?> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<Car?>?>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = Car.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

