//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class SwitchOverride {
  /// Returns a new [SwitchOverride] instance.
  SwitchOverride({
    required this.targetState,
    this.durationOfOverride,
  });

  SwitchState targetState;

  /// Duration of the override in minutes. After that timeframe, the device is pv optimized again.
  // minimum: 0
  // maximum: 2880
  num? durationOfOverride;

  @override
  bool operator ==(Object other) => identical(this, other) || other is SwitchOverride &&
     other.targetState == targetState &&
     other.durationOfOverride == durationOfOverride;

  @override
  int get hashCode =>
    (targetState == null ? 0 : targetState.hashCode) +
    (durationOfOverride == null ? 0 : durationOfOverride.hashCode);

  @override
  String toString() => 'SwitchOverride[targetState=$targetState, durationOfOverride=$durationOfOverride]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'targetState'] = targetState;
    if (durationOfOverride != null) {
      json[r'durationOfOverride'] = durationOfOverride;
    }
    return json;
  }

  /// Returns a new [SwitchOverride] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static SwitchOverride fromJson(Map<String, dynamic> json) =>
      SwitchOverride(
        targetState: SwitchState.fromJson(json[r'targetState']),
        durationOfOverride: json[r'durationOfOverride'] == null ?
          0 :
          json[r'durationOfOverride'].toDouble(),
    );

  static List<SwitchOverride> listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? [] : <SwitchOverride>[]
      : json.map((dynamic value) => SwitchOverride.fromJson(value)).toList(growable: true == growable);

  static Map<String, SwitchOverride> mapFromJson(Map<String, dynamic> json) {
    final map = <String, SwitchOverride>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = SwitchOverride.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of SwitchOverride-objects as value to a dart map
  static Map<String, List<SwitchOverride>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<SwitchOverride>>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = SwitchOverride.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

