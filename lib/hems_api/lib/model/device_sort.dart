//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

/// Available sort criteria for devices. Remarks:  * sort by type has defined order of DeviceType  * sort by state has defined order of DeviceState  * devices with same type/state are ordered by name if equal 
class DeviceSort {
  /// Instantiate a new enum with the provided [value].
  const DeviceSort._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const name = DeviceSort._(r'name');
  static const type = DeviceSort._(r'type');
  static const state = DeviceSort._(r'state');

  /// List of all possible values in this [enum][DeviceSort].
  static const values = <DeviceSort>[
    name,
    type,
    state,
  ];

  static DeviceSort fromJson(dynamic value) =>
    DeviceSortTypeTransformer().decode(value);

  static List<DeviceSort>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <DeviceSort>[]
      : json
          .map((value) => DeviceSort.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [DeviceSort] to String,
/// and [decode] dynamic data back to [DeviceSort].
class DeviceSortTypeTransformer {
  const DeviceSortTypeTransformer._();

  factory DeviceSortTypeTransformer() => _instance;

  String encode(DeviceSort data) => data.value;

  /// Decodes a [dynamic value][data] to a DeviceSort.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  DeviceSort decode(dynamic data) {
    switch (data) {
      case r'name': return DeviceSort.name;
      case r'type': return DeviceSort.type;
      case r'state': return DeviceSort.state;
      default:
        throw ArgumentError('Unknown enum value to decode: $data');
    }
  }

  /// Singleton [DeviceSortTypeTransformer] instance.
  static DeviceSortTypeTransformer _instance = DeviceSortTypeTransformer._();
}
