//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class DeviceConfigUserImportance {
  /// Returns a new [DeviceConfigUserImportance] instance.
  DeviceConfigUserImportance({
    required this.deviceId,
    this.userImportance = 0,
  });

  /// A unique ID for the device.
  String deviceId;

  /// Priority of the device during pv optimization, devices with higher numbers have precedence compares to other pv optimized devices with lower numbers.
  // minimum: 0
  int userImportance;

  @override
  bool operator ==(Object other) => identical(this, other) || other is DeviceConfigUserImportance &&
     other.deviceId == deviceId &&
     other.userImportance == userImportance;

  @override
  int get hashCode =>
    (deviceId == null ? 0 : deviceId.hashCode) +
    (userImportance == null ? 0 : userImportance.hashCode);

  @override
  String toString() => 'DeviceConfigUserImportance[deviceId=$deviceId, userImportance=$userImportance]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'deviceId'] = deviceId;
      json[r'userImportance'] = userImportance;
    return json;
  }

  /// Returns a new [DeviceConfigUserImportance] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static DeviceConfigUserImportance fromJson(Map<String, dynamic> json) =>
      DeviceConfigUserImportance(
        deviceId: json[r'deviceId'],
        userImportance: json[r'userImportance'],
    );

  static List<DeviceConfigUserImportance>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <DeviceConfigUserImportance>[]
      : json.map((dynamic value) => DeviceConfigUserImportance.fromJson(value)).toList(growable: true == growable);

  static Map<String, DeviceConfigUserImportance> mapFromJson(Map<String, dynamic> json) {
    final map = <String, DeviceConfigUserImportance>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = DeviceConfigUserImportance.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of DeviceConfigUserImportance-objects as value to a dart map
  static Map<String, List<DeviceConfigUserImportance?>?> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<DeviceConfigUserImportance?>?>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = DeviceConfigUserImportance.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

