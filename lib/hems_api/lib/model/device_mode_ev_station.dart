//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

/// Current mode of the EV station.
class DeviceModeEVStation {
  /// Instantiate a new enum with the provided [value].
  const DeviceModeEVStation._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const AUTOMATIC = DeviceModeEVStation._(r'AUTOMATIC');
  static const CHARGING = DeviceModeEVStation._(r'CHARGING');
  static const CHARGING_PV = DeviceModeEVStation._(r'CHARGING_PV');
  static const CHARGING_GRID = DeviceModeEVStation._(r'CHARGING_GRID');
  static const DISCHARGING = DeviceModeEVStation._(r'DISCHARGING');
  static const DISCHARGING_GRID = DeviceModeEVStation._(r'DISCHARGING_GRID');
  static const EQUALIZING_CHARGE = DeviceModeEVStation._(r'EQUALIZING_CHARGE');
  static const OFF = DeviceModeEVStation._(r'OFF');
  static const STANDBY = DeviceModeEVStation._(r'STANDBY');

  /// List of all possible values in this [enum][DeviceModeEVStation].
  static const values = <DeviceModeEVStation>[
    AUTOMATIC,
    CHARGING,
    CHARGING_PV,
    CHARGING_GRID,
    DISCHARGING,
    DISCHARGING_GRID,
    EQUALIZING_CHARGE,
    OFF,
    STANDBY,
  ];

  static DeviceModeEVStation fromJson(dynamic value) =>
    DeviceModeEVStationTypeTransformer().decode(value);

  static List<DeviceModeEVStation>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <DeviceModeEVStation>[]
      : json
          .map((value) => DeviceModeEVStation.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [DeviceModeEVStation] to String,
/// and [decode] dynamic data back to [DeviceModeEVStation].
class DeviceModeEVStationTypeTransformer {
  const DeviceModeEVStationTypeTransformer._();

  factory DeviceModeEVStationTypeTransformer() => _instance;

  String encode(DeviceModeEVStation data) => data.value;

  /// Decodes a [dynamic value][data] to a DeviceModeEVStation.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  DeviceModeEVStation decode(dynamic data) {
    switch (data) {
      case r'AUTOMATIC': return DeviceModeEVStation.AUTOMATIC;
      case r'CHARGING': return DeviceModeEVStation.CHARGING;
      case r'CHARGING_PV': return DeviceModeEVStation.CHARGING_PV;
      case r'CHARGING_GRID': return DeviceModeEVStation.CHARGING_GRID;
      case r'DISCHARGING': return DeviceModeEVStation.DISCHARGING;
      case r'DISCHARGING_GRID': return DeviceModeEVStation.DISCHARGING_GRID;
      case r'EQUALIZING_CHARGE': return DeviceModeEVStation.EQUALIZING_CHARGE;
      case r'OFF': return DeviceModeEVStation.OFF;
      case r'STANDBY': return DeviceModeEVStation.STANDBY;
      default:
          throw ArgumentError('Unknown enum value to decode: $data');
    }
  }

  /// Singleton [DeviceModeEVStationTypeTransformer] instance.
  static DeviceModeEVStationTypeTransformer _instance = DeviceModeEVStationTypeTransformer._();
}
