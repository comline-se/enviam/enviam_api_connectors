//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class DeviceSwitcher {
  /// Returns a new [DeviceSwitcher] instance.
  DeviceSwitcher({
    required this.id,
    required this.visibleName,
    required this.description,
    this.requiresOverride = false,
    required this.switchState,
    required this.thresholdType,
    this.supportedModes = const [],
    this.supportedConfigs = const [],
    this.category,
    this.overrideState,
  });

  /// A unique ID for the device.
  String id;

  /// A human readable name for the device.
  String visibleName;

  /// A human readable description for the device. E.g. the device that is served by a plug
  String description;

  /// If set, the device can only be switched with an additional override configuration.
  bool requiresOverride;

  VisibleSwitchState switchState;

  ThresholdType thresholdType;

  /// Schedule modes, that can be configured for the device.
  List<ScheduledDeviceMode>? supportedModes;

  /// Parameters of the device that can be configured.
  List<DeviceConfigParameters>? supportedConfigs;

  DeviceCategory? category;

  OverrideState? overrideState;

  @override
  bool operator ==(Object other) => identical(this, other) || other is DeviceSwitcher &&
     other.id == id &&
     other.visibleName == visibleName &&
     other.description == description &&
     other.requiresOverride == requiresOverride &&
     other.switchState == switchState &&
     other.thresholdType == thresholdType &&
     other.supportedModes == supportedModes &&
     other.supportedConfigs == supportedConfigs &&
     other.category == category &&
     other.overrideState == overrideState;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (visibleName == null ? 0 : visibleName.hashCode) +
    (description == null ? 0 : description.hashCode) +
    (requiresOverride == null ? 0 : requiresOverride.hashCode) +
    (switchState == null ? 0 : switchState.hashCode) +
    (thresholdType == null ? 0 : thresholdType.hashCode) +
    (supportedModes == null ? 0 : supportedModes.hashCode) +
    (supportedConfigs == null ? 0 : supportedConfigs.hashCode) +
    (category == null ? 0 : category.hashCode) +
    (overrideState == null ? 0 : overrideState.hashCode);

  @override
  String toString() => 'DeviceSwitcher[id=$id, visibleName=$visibleName, description=$description, requiresOverride=$requiresOverride, switchState=$switchState, thresholdType=$thresholdType, supportedModes=$supportedModes, supportedConfigs=$supportedConfigs, category=$category, overrideState=$overrideState]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'id'] = id;
      json[r'visibleName'] = visibleName;
      json[r'description'] = description;
    if (requiresOverride != null) {
      json[r'requiresOverride'] = requiresOverride;
    }
      json[r'switchState'] = switchState;
      json[r'thresholdType'] = thresholdType;
      json[r'supportedModes'] = supportedModes;
      json[r'supportedConfigs'] = supportedConfigs;
    if (category != null) {
      json[r'category'] = category;
    }
    if (overrideState != null) {
      json[r'overrideState'] = overrideState;
    }
    return json;
  }

  /// Returns a new [DeviceSwitcher] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static DeviceSwitcher fromJson(Map<String, dynamic> json) =>
      DeviceSwitcher(
        id: json[r'id'],
        visibleName: json[r'visibleName'],
        description: json[r'description'],
        requiresOverride: json[r'requiresOverride'],
        switchState: VisibleSwitchState.fromJson(json[r'switchState']),
        thresholdType: ThresholdType.fromJson(json[r'thresholdType']),
        supportedModes: ScheduledDeviceMode.listFromJson(json[r'supportedModes']),
        supportedConfigs: DeviceConfigParameters.listFromJson(json[r'supportedConfigs']),
        category: DeviceCategory.fromJson(json[r'category']),
        overrideState: OverrideState.fromJson(json[r'overrideState']),
    );

  static List<DeviceSwitcher>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <DeviceSwitcher>[]
      : json.map((dynamic value) => DeviceSwitcher.fromJson(value)).toList(growable: true == growable);

  static Map<String, DeviceSwitcher> mapFromJson(Map<String, dynamic> json) {
    final map = <String, DeviceSwitcher>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = DeviceSwitcher.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of DeviceSwitcher-objects as value to a dart map
  static Map<String, List<DeviceSwitcher?>?> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<DeviceSwitcher?>?>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = DeviceSwitcher.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

