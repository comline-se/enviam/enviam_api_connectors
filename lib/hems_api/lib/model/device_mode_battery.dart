//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

/// Current mode of the battery.
class DeviceModeBattery {
  /// Instantiate a new enum with the provided [value].
  const DeviceModeBattery._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const AUTOMATIC = DeviceModeBattery._(r'AUTOMATIC');
  static const CHARGING = DeviceModeBattery._(r'CHARGING');
  static const CHARGING_PV = DeviceModeBattery._(r'CHARGING_PV');
  static const CHARGING_GRID = DeviceModeBattery._(r'CHARGING_GRID');
  static const DISCHARGING = DeviceModeBattery._(r'DISCHARGING');
  static const EQUALIZING_CHARGE = DeviceModeBattery._(r'EQUALIZING_CHARGE');
  static const OFF = DeviceModeBattery._(r'OFF');
  static const STANDBY = DeviceModeBattery._(r'STANDBY');

  /// List of all possible values in this [enum][DeviceModeBattery].
  static const values = <DeviceModeBattery>[
    AUTOMATIC,
    CHARGING,
    CHARGING_PV,
    CHARGING_GRID,
    DISCHARGING,
    EQUALIZING_CHARGE,
    OFF,
    STANDBY,
  ];

  static DeviceModeBattery fromJson(dynamic value) =>
    DeviceModeBatteryTypeTransformer().decode(value);

  static List<DeviceModeBattery>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <DeviceModeBattery>[]
      : json
          .map((value) => DeviceModeBattery.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [DeviceModeBattery] to String,
/// and [decode] dynamic data back to [DeviceModeBattery].
class DeviceModeBatteryTypeTransformer {
  const DeviceModeBatteryTypeTransformer._();

  factory DeviceModeBatteryTypeTransformer() => _instance;

  String encode(DeviceModeBattery data) => data.value;

  /// Decodes a [dynamic value][data] to a DeviceModeBattery.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  DeviceModeBattery decode(dynamic data) {
    switch (data) {
      case r'AUTOMATIC': return DeviceModeBattery.AUTOMATIC;
      case r'CHARGING': return DeviceModeBattery.CHARGING;
      case r'CHARGING_PV': return DeviceModeBattery.CHARGING_PV;
      case r'CHARGING_GRID': return DeviceModeBattery.CHARGING_GRID;
      case r'DISCHARGING': return DeviceModeBattery.DISCHARGING;
      case r'EQUALIZING_CHARGE': return DeviceModeBattery.EQUALIZING_CHARGE;
      case r'OFF': return DeviceModeBattery.OFF;
      case r'STANDBY': return DeviceModeBattery.STANDBY;
      default:
        throw ArgumentError('Unknown enum value to decode: $data');
    }
  }

  /// Singleton [DeviceModeBatteryTypeTransformer] instance.
  static DeviceModeBatteryTypeTransformer _instance = DeviceModeBatteryTypeTransformer._();
}
