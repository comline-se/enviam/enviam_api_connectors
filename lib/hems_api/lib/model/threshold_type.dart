//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

/// Type of threshold: * RANGE defines a minimum and a maximum, and the device is only turned ON inside that * DISCRETE defines the levels of power supported by the devices, the device can only be switched OFF below the   lowest level, or for any of the given discrete power levels * FLOOR defines only a minimum, device is switched OFF below and ON above 
class ThresholdType {
  /// Instantiate a new enum with the provided [value].
  const ThresholdType._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const FLOOR = ThresholdType._(r'FLOOR');
  static const RANGE = ThresholdType._(r'RANGE');
  static const DISCRETE = ThresholdType._(r'DISCRETE');

  /// List of all possible values in this [enum][ThresholdType].
  static const values = <ThresholdType>[
    FLOOR,
    RANGE,
    DISCRETE,
  ];

  static ThresholdType fromJson(dynamic value) =>
    ThresholdTypeTypeTransformer().decode(value);

  static List<ThresholdType>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <ThresholdType>[]
      : json
          .map((value) => ThresholdType.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [ThresholdType] to String,
/// and [decode] dynamic data back to [ThresholdType].
class ThresholdTypeTypeTransformer {
  const ThresholdTypeTypeTransformer._();

  factory ThresholdTypeTypeTransformer() => _instance;

  String encode(ThresholdType data) => data.value;

  /// Decodes a [dynamic value][data] to a ThresholdType.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  ThresholdType decode(dynamic data) {
    switch (data) {
      case r'FLOOR': return ThresholdType.FLOOR;
      case r'RANGE': return ThresholdType.RANGE;
      case r'DISCRETE': return ThresholdType.DISCRETE;
      default:
        throw ArgumentError('Unknown enum value to decode: $data');
    }
  }

  /// Singleton [ThresholdTypeTypeTransformer] instance.
  static ThresholdTypeTypeTransformer _instance = ThresholdTypeTypeTransformer._();
}
