//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Timeseries {
  /// Returns a new [Timeseries] instance.
  Timeseries({
    required this.name,
    required this.aggregated,
    this.guid,
    required this.id,
    required this.unit,
    this.values = const {},
  });

  /// The name of the timeseries
  String name;

  /// The aggregated values of the timeseries
  int aggregated;

  /// The id of the related device
  String? guid;

  /// The id of the timeseries (<deviceGuid>~<tagName>)
  String id;

  TimeseriesUnit unit;

  /// List of timeseries values
  Map<String, int> values;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Timeseries &&
     other.name == name &&
     other.aggregated == aggregated &&
     other.guid == guid &&
     other.id == id &&
     other.unit == unit &&
     other.values == values;

  @override
  int get hashCode =>
    (name == null ? 0 : name.hashCode) +
    (aggregated == null ? 0 : aggregated.hashCode) +
    (guid == null ? 0 : guid.hashCode) +
    (id == null ? 0 : id.hashCode) +
    (unit == null ? 0 : unit.hashCode) +
    (values == null ? 0 : values.hashCode);

  @override
  String toString() => 'Timeseries[name=$name, aggregated=$aggregated, guid=$guid, id=$id, unit=$unit, values=$values]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'name'] = name;
      json[r'aggregated'] = aggregated;
    if (guid != null) {
      json[r'guid'] = guid;
    }
      json[r'id'] = id;
      json[r'unit'] = unit;
      json[r'values'] = values;
    return json;
  }

  /// Returns a new [Timeseries] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Timeseries fromJson(Map<String, dynamic> json) =>
      Timeseries(
        name: json[r'name'],
        aggregated: json[r'aggregated'],
        guid: json[r'guid'],
        id: json[r'id'],
        unit: TimeseriesUnit.fromJson(json[r'unit']),
        values: json[r'values'] == null
            ? {}
            : (json[r'values'] as Map).cast<String, int>(),
    );

  static List<Timeseries> listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? [] : <Timeseries>[]
      : json.map((dynamic value) => Timeseries.fromJson(value)).toList(growable: true == growable);

  static Map<String, Timeseries> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Timeseries>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = Timeseries.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of Timeseries-objects as value to a dart map
  static Map<String, List<Timeseries>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<Timeseries>>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = Timeseries.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

