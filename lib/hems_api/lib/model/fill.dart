//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

/// How to handle missing values in time series:  * none: nothing is filled  * null: fill with null values  * zero: fill with zero 
class Fill {
  /// Instantiate a new enum with the provided [value].
  const Fill._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const none = Fill._(r'none');
  static const null_ = Fill._(r'null');
  static const zero = Fill._(r'zero');

  /// List of all possible values in this [enum][Fill].
  static const values = <Fill>[
    none,
    null_,
    zero,
  ];

  static Fill fromJson(dynamic value) =>
    FillTypeTransformer().decode(value);

  static List<Fill> listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? [] : <Fill>[]
      : json
          .map((value) => Fill.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [Fill] to String,
/// and [decode] dynamic data back to [Fill].
class FillTypeTransformer {
  const FillTypeTransformer._();

  factory FillTypeTransformer() => _instance;

  String encode(Fill data) => data.value;

  /// Decodes a [dynamic value][data] to a Fill.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  Fill decode(dynamic data) {
    switch (data) {
      case r'none': return Fill.none;
      case r'null': return Fill.null_;
      case r'zero': return Fill.zero;
      default:
        return Fill.none;
    }
  }

  /// Singleton [FillTypeTransformer] instance.
  static FillTypeTransformer _instance = FillTypeTransformer._();
}
