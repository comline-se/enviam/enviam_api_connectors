//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class EvStationConfig {
  /// Returns a new [EvStationConfig] instance.
  EvStationConfig({
    required this.maximumCapacity,
    required this.maximumChargingPower,
    required this.maximumRange,
  });

  /// The maximum capacity of the battery associated with this ev-station.
  num maximumCapacity;

  /// The maximum charging power of the ev-station.
  num maximumChargingPower;

  /// The maximum range power of the car associated with this ev-station.
  num maximumRange;

  @override
  bool operator ==(Object other) => identical(this, other) || other is EvStationConfig &&
     other.maximumCapacity == maximumCapacity &&
     other.maximumChargingPower == maximumChargingPower &&
     other.maximumRange == maximumRange;

  @override
  int get hashCode =>
    (maximumCapacity == null ? 0 : maximumCapacity.hashCode) +
    (maximumChargingPower == null ? 0 : maximumChargingPower.hashCode) +
    (maximumRange == null ? 0 : maximumRange.hashCode);

  @override
  String toString() => 'EvStationConfig[maximumCapacity=$maximumCapacity, maximumChargingPower=$maximumChargingPower, maximumRange=$maximumRange]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'maximumCapacity'] = maximumCapacity;
      json[r'maximumChargingPower'] = maximumChargingPower;
      json[r'maximumRange'] = maximumRange;
    return json;
  }

  /// Returns a new [EvStationConfig] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static EvStationConfig fromJson(Map<String, dynamic> json) =>
      EvStationConfig(
        maximumCapacity: json[r'maximumCapacity'] == null ?
          null :
          json[r'maximumCapacity'].toDouble(),
        maximumChargingPower: json[r'maximumChargingPower'] == null ?
          null :
          json[r'maximumChargingPower'].toDouble(),
        maximumRange: json[r'maximumRange'] == null ?
          null :
          json[r'maximumRange'].toDouble(),
    );

  static List<EvStationConfig> listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? [] : <EvStationConfig>[]
      : json.map((dynamic value) => EvStationConfig.fromJson(value)).toList(growable: true == growable);

  static Map<String, EvStationConfig> mapFromJson(Map<String, dynamic> json) {
    final map = <String, EvStationConfig>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = EvStationConfig.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of EvStationConfig-objects as value to a dart map
  static Map<String, List<EvStationConfig>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<EvStationConfig>>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = EvStationConfig.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

