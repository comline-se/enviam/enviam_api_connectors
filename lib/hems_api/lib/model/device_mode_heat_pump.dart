//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

/// Current mode of the heat pump.
class DeviceModeHeatPump {
  /// Instantiate a new enum with the provided [value].
  const DeviceModeHeatPump._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const ACCUMULATION = DeviceModeHeatPump._(r'ACCUMULATION');
  static const ACTIVATION_RECOMMENDATION = DeviceModeHeatPump._(r'ACTIVATION_RECOMMENDATION');
  static const ACTIVE = DeviceModeHeatPump._(r'ACTIVE');
  static const UTILITY_LOCK = DeviceModeHeatPump._(r'UTILITY_LOCK');

  /// List of all possible values in this [enum][DeviceModeHeatPump].
  static const values = <DeviceModeHeatPump>[
    ACCUMULATION,
    ACTIVATION_RECOMMENDATION,
    ACTIVE,
    UTILITY_LOCK,
  ];

  static DeviceModeHeatPump fromJson(dynamic value) =>
    DeviceModeHeatPumpTypeTransformer().decode(value);

  static List<DeviceModeHeatPump>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <DeviceModeHeatPump>[]
      : json
          .map((value) => DeviceModeHeatPump.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [DeviceModeHeatPump] to String,
/// and [decode] dynamic data back to [DeviceModeHeatPump].
class DeviceModeHeatPumpTypeTransformer {
  const DeviceModeHeatPumpTypeTransformer._();

  factory DeviceModeHeatPumpTypeTransformer() => _instance;

  String encode(DeviceModeHeatPump data) => data.value;

  /// Decodes a [dynamic value][data] to a DeviceModeHeatPump.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  DeviceModeHeatPump decode(dynamic data) {
    switch (data) {
      case r'ACCUMULATION': return DeviceModeHeatPump.ACCUMULATION;
      case r'ACTIVATION_RECOMMENDATION': return DeviceModeHeatPump.ACTIVATION_RECOMMENDATION;
      case r'ACTIVE': return DeviceModeHeatPump.ACTIVE;
      case r'UTILITY_LOCK': return DeviceModeHeatPump.UTILITY_LOCK;
      default:
        throw ArgumentError('Unknown enum value to decode: $data');
    }
  }

  /// Singleton [DeviceModeHeatPumpTypeTransformer] instance.
  static DeviceModeHeatPumpTypeTransformer _instance = DeviceModeHeatPumpTypeTransformer._();
}
