//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Device {
  /// Returns a new [Device] instance.
  Device({
    required this.id,
    required this.name,
    this.mode,
    required this.stateDevice,
    required this.stateErrorList,
    required this.type,
  });

  /// A unique ID for the device.
  String id;

  /// The devices displayed name (changeable by the user).
  String name;

  DeviceModeFull? mode;

  DeviceState stateDevice;

  DeviceStateErrorList stateErrorList;

  DeviceType type;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Device &&
     other.id == id &&
     other.name == name &&
     other.mode == mode &&
     other.stateDevice == stateDevice &&
     other.stateErrorList == stateErrorList &&
     other.type == type;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (mode == null ? 0 : mode.hashCode) +
    (stateDevice == null ? 0 : stateDevice.hashCode) +
    (stateErrorList == null ? 0 : stateErrorList.hashCode) +
    (type == null ? 0 : type.hashCode);

  @override
  String toString() => 'Device[id=$id, name=$name, mode=$mode, stateDevice=$stateDevice, stateErrorList=$stateErrorList, type=$type]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'id'] = id;
      json[r'name'] = name;
    if (mode != null) {
      json[r'mode'] = mode;
    }
      json[r'state_device'] = stateDevice;
      json[r'state_error_list'] = stateErrorList;
      json[r'type'] = type;
    return json;
  }

  /// Returns a new [Device] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Device fromJson(Map<String, dynamic> json) =>
      Device(
        id: json[r'id'],
        name: json[r'name'],
        mode: DeviceModeFull.fromJson(json[r'mode']),
        stateDevice: DeviceState.fromJson(json[r'state_device']),
        stateErrorList: DeviceStateErrorList.fromJson(json[r'state_error_list']),
        type: DeviceType.fromJson(json[r'type']),
    );

  static List<Device>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <Device>[]
      : json.map((dynamic value) => Device.fromJson(value)).toList(growable: true == growable);

  static Map<String, Device> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Device>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = Device.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of Device-objects as value to a dart map
  static Map<String, List<Device?>?> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<Device?>?>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = Device.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

