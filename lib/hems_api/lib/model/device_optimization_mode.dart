//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

/// The charging optimization mode.
class DeviceOptimizationMode {
  /// Instantiate a new enum with the provided [value].
  const DeviceOptimizationMode._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const DEPARTURE_TIME = DeviceOptimizationMode._(r'DEPARTURE_TIME');
  static const POWER_CHARGING = DeviceOptimizationMode._(r'POWER_CHARGING');
  static const PV_EXCESS = DeviceOptimizationMode._(r'PV_EXCESS');
  static const GRID_OPTIMIZED = DeviceOptimizationMode._(r'GRID_OPTIMIZED');

  /// List of all possible values in this [enum][DeviceOptimizationMode].
  static const values = <DeviceOptimizationMode>[
    DEPARTURE_TIME,
    POWER_CHARGING,
    PV_EXCESS,
    GRID_OPTIMIZED,
  ];

  static DeviceOptimizationMode fromJson(dynamic value) =>
    DeviceOptimizationModeTypeTransformer().decode(value);

  static List<DeviceOptimizationMode>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
      json.isEmpty
      ? true == emptyIsNull ? null : <DeviceOptimizationMode>[]
      : json
          .map((value) => DeviceOptimizationMode.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [DeviceOptimizationMode] to String,
/// and [decode] dynamic data back to [DeviceOptimizationMode].
class DeviceOptimizationModeTypeTransformer {
  const DeviceOptimizationModeTypeTransformer._();

  factory DeviceOptimizationModeTypeTransformer() => _instance;

  String encode(DeviceOptimizationMode data) => data.value;

  /// Decodes a [dynamic value][data] to a DeviceOptimizationMode.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  DeviceOptimizationMode decode(dynamic data) {
    switch (data) {
      case r'DEPARTURE_TIME': return DeviceOptimizationMode.DEPARTURE_TIME;
      case r'POWER_CHARGING': return DeviceOptimizationMode.POWER_CHARGING;
      case r'PV_EXCESS': return DeviceOptimizationMode.PV_EXCESS;
      case r'GRID_OPTIMIZED': return DeviceOptimizationMode.GRID_OPTIMIZED;
      default:
        throw ArgumentError('Unknown enum value to decode: $data');
    }
  }

  /// Singleton [DeviceOptimizationModeTypeTransformer] instance.
  static DeviceOptimizationModeTypeTransformer _instance = DeviceOptimizationModeTypeTransformer._();
}
