//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class DeviceUpdate {
  /// Returns a new [DeviceUpdate] instance.
  DeviceUpdate({
    required this.name,
  });

  /// The devices displayed name (changeable by the user).
  String name;

  @override
  bool operator ==(Object other) => identical(this, other) || other is DeviceUpdate &&
     other.name == name;

  @override
  int get hashCode =>
    (name == null ? 0 : name.hashCode);

  @override
  String toString() => 'DeviceUpdate[name=$name]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'name'] = name;
    return json;
  }

  /// Returns a new [DeviceUpdate] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static DeviceUpdate fromJson(Map<String, dynamic> json) =>
      DeviceUpdate(
        name: json[r'name'],
    );

  static List<DeviceUpdate>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <DeviceUpdate>[]
      : json.map((dynamic value) => DeviceUpdate.fromJson(value)).toList(growable: true == growable);

  static Map<String, DeviceUpdate> mapFromJson(Map<String, dynamic> json) {
    final map = <String, DeviceUpdate>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = DeviceUpdate.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of DeviceUpdate-objects as value to a dart map
  static Map<String, List<DeviceUpdate?>?> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<DeviceUpdate?>?>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = DeviceUpdate.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

