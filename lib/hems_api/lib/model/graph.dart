//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Graph {
  /// Returns a new [Graph] instance.
  Graph({
    required this.sum,
    this.values = const {},
  });

  /// Total sum of values displayed in graph.
  // minimum: 0
  double sum;

  /// List of values.
  Map<String, double> values;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Graph &&
     other.sum == sum &&
     other.values == values;

  @override
  int get hashCode =>
    (sum.hashCode) +
    (values.hashCode);

  @override
  String toString() => 'Graph[sum=$sum, values=$values]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'sum'] = sum;
      json[r'values'] = values;
    return json;
  }

  /// Returns a new [Graph] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Graph fromJson(Map<String, dynamic> json) =>
      Graph(
        sum: json[r'sum'],
        values: json[r'values'] == null
            ? {}
            : (json[r'values'] as Map).cast<String, double>(),
    );

  static List<Graph> listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? [] : <Graph>[]
      : json.map((dynamic value) => Graph.fromJson(value)).toList(growable: true == growable);

  static Map<String, Graph> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Graph>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = Graph.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of Graph-objects as value to a dart map
  static Map<String, List<Graph>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<Graph>>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = Graph.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

