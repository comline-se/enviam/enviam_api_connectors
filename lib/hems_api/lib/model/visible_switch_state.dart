//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

/// Switch state to be visualized of the device, e.g. is it turned ON or OFF or is it LOCKED.
class VisibleSwitchState {
  /// Instantiate a new enum with the provided [value].
  const VisibleSwitchState._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const ON = VisibleSwitchState._(r'ON');
  static const OFF = VisibleSwitchState._(r'OFF');
  static const LOCKED = VisibleSwitchState._(r'LOCKED');
  static const SWITCHING = VisibleSwitchState._(r'SWITCHING');

  /// List of all possible values in this [enum][VisibleSwitchState].
  static const values = <VisibleSwitchState>[
    ON,
    OFF,
    LOCKED,
    SWITCHING,
  ];

  static VisibleSwitchState fromJson(dynamic value) =>
    VisibleSwitchStateTypeTransformer().decode(value);

  static List<VisibleSwitchState>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <VisibleSwitchState>[]
      : json
          .map((value) => VisibleSwitchState.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [VisibleSwitchState] to String,
/// and [decode] dynamic data back to [VisibleSwitchState].
class VisibleSwitchStateTypeTransformer {
  const VisibleSwitchStateTypeTransformer._();

  factory VisibleSwitchStateTypeTransformer() => _instance;

  String encode(VisibleSwitchState data) => data.value;

  /// Decodes a [dynamic value][data] to a VisibleSwitchState.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  VisibleSwitchState decode(dynamic data) {
    switch (data) {
      case r'ON': return VisibleSwitchState.ON;
      case r'OFF': return VisibleSwitchState.OFF;
      case r'LOCKED': return VisibleSwitchState.LOCKED;
      case r'SWITCHING': return VisibleSwitchState.SWITCHING;
      default:
        return VisibleSwitchState.OFF;
    }
  }

  /// Singleton [VisibleSwitchStateTypeTransformer] instance.
  static VisibleSwitchStateTypeTransformer _instance = VisibleSwitchStateTypeTransformer._();
}
