//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Schedule {
  /// Returns a new [Schedule] instance.
  Schedule({
    this.zoneId = 'Europe/Berlin',
    this.monday = const [],
    this.tuesday = const [],
    this.wednesday = const [],
    this.thursday = const [],
    this.friday = const [],
    this.saturday = const [],
    this.sunday = const [],
  });

  /// Id of the timezone (e.g. Europe/Berlin, UTC+1 or UTC+01:00) that the schedule should use.
  String zoneId;

  /// A list of timespans for one day. The span entries are not allowed to overlap.
  List<TimeSpan> monday;

  /// A list of timespans for one day. The span entries are not allowed to overlap.
  List<TimeSpan> tuesday;

  /// A list of timespans for one day. The span entries are not allowed to overlap.
  List<TimeSpan> wednesday;

  /// A list of timespans for one day. The span entries are not allowed to overlap.
  List<TimeSpan> thursday;

  /// A list of timespans for one day. The span entries are not allowed to overlap.
  List<TimeSpan> friday;

  /// A list of timespans for one day. The span entries are not allowed to overlap.
  List<TimeSpan> saturday;

  /// A list of timespans for one day. The span entries are not allowed to overlap.
  List<TimeSpan> sunday;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Schedule &&
     other.zoneId == zoneId &&
     other.monday == monday &&
     other.tuesday == tuesday &&
     other.wednesday == wednesday &&
     other.thursday == thursday &&
     other.friday == friday &&
     other.saturday == saturday &&
     other.sunday == sunday;

  @override
  int get hashCode =>
    (zoneId == null ? 0 : zoneId.hashCode) +
    (monday == null ? 0 : monday.hashCode) +
    (tuesday == null ? 0 : tuesday.hashCode) +
    (wednesday == null ? 0 : wednesday.hashCode) +
    (thursday == null ? 0 : thursday.hashCode) +
    (friday == null ? 0 : friday.hashCode) +
    (saturday == null ? 0 : saturday.hashCode) +
    (sunday == null ? 0 : sunday.hashCode);

  @override
  String toString() => 'Schedule[zoneId=$zoneId, monday=$monday, tuesday=$tuesday, wednesday=$wednesday, thursday=$thursday, friday=$friday, saturday=$saturday, sunday=$sunday]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'zoneId'] = zoneId;
    if (monday != null) {
      json[r'monday'] = monday;
    }
    if (tuesday != null) {
      json[r'tuesday'] = tuesday;
    }
    if (wednesday != null) {
      json[r'wednesday'] = wednesday;
    }
    if (thursday != null) {
      json[r'thursday'] = thursday;
    }
    if (friday != null) {
      json[r'friday'] = friday;
    }
    if (saturday != null) {
      json[r'saturday'] = saturday;
    }
    if (sunday != null) {
      json[r'sunday'] = sunday;
    }
    return json;
  }

  /// Returns a new [Schedule] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Schedule fromJson(Map<String, dynamic> json) =>
      Schedule(
        zoneId: json[r'zoneId'],
        monday: TimeSpan.listFromJson(json[r'monday']),
        tuesday: TimeSpan.listFromJson(json[r'tuesday']),
        wednesday: TimeSpan.listFromJson(json[r'wednesday']),
        thursday: TimeSpan.listFromJson(json[r'thursday']),
        friday: TimeSpan.listFromJson(json[r'friday']),
        saturday: TimeSpan.listFromJson(json[r'saturday']),
        sunday: TimeSpan.listFromJson(json[r'sunday']),
    );

  static List<Schedule> listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? [] : <Schedule>[]
      : json.map((dynamic value) => Schedule.fromJson(value)).toList(growable: true == growable);

  static Map<String, Schedule> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Schedule>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = Schedule.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of Schedule-objects as value to a dart map
  static Map<String, List<Schedule>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<Schedule>>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = Schedule.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

