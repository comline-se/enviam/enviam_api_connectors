//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

/// Supported and available device types.
class DeviceType {
  /// Instantiate a new enum with the provided [value].
  const DeviceType._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const PV = DeviceType._(r'PV');
  static const METER = DeviceType._(r'METER');
  static const INVERTER = DeviceType._(r'INVERTER');
  static const BATTERY = DeviceType._(r'BATTERY');
  static const EV_STATION = DeviceType._(r'EV_STATION');
  static const PLUG = DeviceType._(r'PLUG');
  static const HEAT_PUMP = DeviceType._(r'HEAT_PUMP');
  static const SMART_HEATER = DeviceType._(r'SMART_HEATER');
  static const HEAT_STORAGE = DeviceType._(r'HEAT_STORAGE');
  static const LOCATION = DeviceType._(r'LOCATION');
  static const ENERGY_MANAGER = DeviceType._(r'ENERGY_MANAGER');
  static const RIPPLE_CONTROL_RECEIVER = DeviceType._(r'RIPPLE_CONTROL_RECEIVER');

  /// List of all possible values in this [enum][DeviceType].
  static const values = <DeviceType>[
    PV,
    METER,
    INVERTER,
    BATTERY,
    EV_STATION,
    PLUG,
    HEAT_PUMP,
    SMART_HEATER,
    HEAT_STORAGE,
    LOCATION,
    ENERGY_MANAGER,
    RIPPLE_CONTROL_RECEIVER,
  ];

  static DeviceType fromJson(dynamic value) =>
    DeviceTypeTypeTransformer().decode(value);

  static List<DeviceType?>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <DeviceType>[]
      : json
          .map((value) => DeviceType.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [DeviceType] to String,
/// and [decode] dynamic data back to [DeviceType].
class DeviceTypeTypeTransformer {
  const DeviceTypeTypeTransformer._();

  factory DeviceTypeTypeTransformer() => _instance;

  String encode(DeviceType data) => data.value;

  /// Decodes a [dynamic value][data] to a DeviceType.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  DeviceType decode(dynamic data) {
    switch (data) {
      case r'PV': return DeviceType.PV;
      case r'METER': return DeviceType.METER;
      case r'INVERTER': return DeviceType.INVERTER;
      case r'BATTERY': return DeviceType.BATTERY;
      case r'EV_STATION': return DeviceType.EV_STATION;
      case r'PLUG': return DeviceType.PLUG;
      case r'HEAT_PUMP': return DeviceType.HEAT_PUMP;
      case r'SMART_HEATER': return DeviceType.SMART_HEATER;
      case r'HEAT_STORAGE': return DeviceType.HEAT_STORAGE;
      case r'LOCATION': return DeviceType.LOCATION;
      case r'ENERGY_MANAGER': return DeviceType.ENERGY_MANAGER;
      case r'RIPPLE_CONTROL_RECEIVER': return DeviceType.RIPPLE_CONTROL_RECEIVER;
      default:
        return DeviceType.EV_STATION;
    }
  }

  /// Singleton [DeviceTypeTypeTransformer] instance.
  static DeviceTypeTypeTransformer _instance = DeviceTypeTypeTransformer._();
}
