//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//


// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

/// These modes are the charging / optimization modes. 
class EVStationChargingMode {
  /// Instantiate a new enum with the provided [value].
  const EVStationChargingMode._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const DEPARTURE_TIME = EVStationChargingMode._(r'DEPARTURE_TIME');
  static const POWER_CHARGING = EVStationChargingMode._(r'POWER_CHARGING');
  static const PV_EXCESS = EVStationChargingMode._(r'PV_EXCESS');

  /// List of all possible values in this [enum][EVStationChargingMode].
  static const values = <EVStationChargingMode>[
    DEPARTURE_TIME,
    POWER_CHARGING,
    PV_EXCESS,
  ];

  static EVStationChargingMode fromJson(dynamic value) =>
    EVStationChargingModeTypeTransformer().decode(value);

  static List<EVStationChargingMode?>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <EVStationChargingMode>[]
      : json
          .map((value) => EVStationChargingMode.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [EVStationChargingMode] to String,
/// and [decode] dynamic data back to [EVStationChargingMode].
class EVStationChargingModeTypeTransformer {
  const EVStationChargingModeTypeTransformer._();

  factory EVStationChargingModeTypeTransformer() => _instance;

  String encode(EVStationChargingMode data) => data.value;

  /// Decodes a [dynamic value][data] to a EVStationChargingMode.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  EVStationChargingMode decode(dynamic data) {
    switch (data) {
      case r'DEPARTURE_TIME': return EVStationChargingMode.DEPARTURE_TIME;
      case r'POWER_CHARGING': return EVStationChargingMode.POWER_CHARGING;
      case r'PV_EXCESS': return EVStationChargingMode.PV_EXCESS;
      default:
        return EVStationChargingMode.POWER_CHARGING;
    }
  }

  /// Singleton [EVStationChargingModeTypeTransformer] instance.
  static EVStationChargingModeTypeTransformer _instance = EVStationChargingModeTypeTransformer._();
}
