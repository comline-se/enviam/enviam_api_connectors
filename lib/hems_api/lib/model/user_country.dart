//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class UserCountry {
  /// Instantiate a new enum with the provided [value].
  const UserCountry._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const AF = UserCountry._(r'AF');
  static const AL = UserCountry._(r'AL');
  static const DZ = UserCountry._(r'DZ');
  static const AS = UserCountry._(r'AS');
  static const AD = UserCountry._(r'AD');
  static const AO = UserCountry._(r'AO');
  static const AI = UserCountry._(r'AI');
  static const AQ = UserCountry._(r'AQ');
  static const AG = UserCountry._(r'AG');
  static const AR = UserCountry._(r'AR');
  static const AM = UserCountry._(r'AM');
  static const AW = UserCountry._(r'AW');
  static const AC = UserCountry._(r'AC');
  static const AU = UserCountry._(r'AU');
  static const AT = UserCountry._(r'AT');
  static const AZ = UserCountry._(r'AZ');
  static const BS = UserCountry._(r'BS');
  static const BH = UserCountry._(r'BH');
  static const BD = UserCountry._(r'BD');
  static const BB = UserCountry._(r'BB');
  static const BY = UserCountry._(r'BY');
  static const BE = UserCountry._(r'BE');
  static const BZ = UserCountry._(r'BZ');
  static const BJ = UserCountry._(r'BJ');
  static const BM = UserCountry._(r'BM');
  static const BT = UserCountry._(r'BT');
  static const BO = UserCountry._(r'BO');
  static const BA = UserCountry._(r'BA');
  static const BW = UserCountry._(r'BW');
  static const BR = UserCountry._(r'BR');
  static const IO = UserCountry._(r'IO');
  static const VG = UserCountry._(r'VG');
  static const BN = UserCountry._(r'BN');
  static const BG = UserCountry._(r'BG');
  static const BF = UserCountry._(r'BF');
  static const BI = UserCountry._(r'BI');
  static const KH = UserCountry._(r'KH');
  static const CM = UserCountry._(r'CM');
  static const CA = UserCountry._(r'CA');
  static const IC = UserCountry._(r'IC');
  static const CV = UserCountry._(r'CV');
  static const BQ = UserCountry._(r'BQ');
  static const KY = UserCountry._(r'KY');
  static const CF = UserCountry._(r'CF');
  static const EA = UserCountry._(r'EA');
  static const TD = UserCountry._(r'TD');
  static const CL = UserCountry._(r'CL');
  static const CN = UserCountry._(r'CN');
  static const CX = UserCountry._(r'CX');
  static const CC = UserCountry._(r'CC');
  static const CO = UserCountry._(r'CO');
  static const KM = UserCountry._(r'KM');
  static const CG = UserCountry._(r'CG');
  static const CD = UserCountry._(r'CD');
  static const CK = UserCountry._(r'CK');
  static const CR = UserCountry._(r'CR');
  static const HR = UserCountry._(r'HR');
  static const CU = UserCountry._(r'CU');
  static const CW = UserCountry._(r'CW');
  static const CY = UserCountry._(r'CY');
  static const CZ = UserCountry._(r'CZ');
  static const CI = UserCountry._(r'CI');
  static const DK = UserCountry._(r'DK');
  static const DG = UserCountry._(r'DG');
  static const DJ = UserCountry._(r'DJ');
  static const DM = UserCountry._(r'DM');
  static const DO = UserCountry._(r'DO');
  static const EC = UserCountry._(r'EC');
  static const EG = UserCountry._(r'EG');
  static const SV = UserCountry._(r'SV');
  static const GQ = UserCountry._(r'GQ');
  static const ER = UserCountry._(r'ER');
  static const EE = UserCountry._(r'EE');
  static const ET = UserCountry._(r'ET');
  static const FK = UserCountry._(r'FK');
  static const FO = UserCountry._(r'FO');
  static const FJ = UserCountry._(r'FJ');
  static const FI = UserCountry._(r'FI');
  static const FR = UserCountry._(r'FR');
  static const GF = UserCountry._(r'GF');
  static const PF = UserCountry._(r'PF');
  static const TF = UserCountry._(r'TF');
  static const GA = UserCountry._(r'GA');
  static const GM = UserCountry._(r'GM');
  static const GE = UserCountry._(r'GE');
  static const DE = UserCountry._(r'DE');
  static const GH = UserCountry._(r'GH');
  static const GI = UserCountry._(r'GI');
  static const GR = UserCountry._(r'GR');
  static const GL = UserCountry._(r'GL');
  static const GD = UserCountry._(r'GD');
  static const GP = UserCountry._(r'GP');
  static const GU = UserCountry._(r'GU');
  static const GT = UserCountry._(r'GT');
  static const GG = UserCountry._(r'GG');
  static const GN = UserCountry._(r'GN');
  static const GW = UserCountry._(r'GW');
  static const GY = UserCountry._(r'GY');
  static const HT = UserCountry._(r'HT');
  static const HN = UserCountry._(r'HN');
  static const HK = UserCountry._(r'HK');
  static const HU = UserCountry._(r'HU');
  static const IS = UserCountry._(r'IS');
  static const IN = UserCountry._(r'IN');
  static const ID = UserCountry._(r'ID');
  static const IR = UserCountry._(r'IR');
  static const IQ = UserCountry._(r'IQ');
  static const IE = UserCountry._(r'IE');
  static const IM = UserCountry._(r'IM');
  static const IL = UserCountry._(r'IL');
  static const IT = UserCountry._(r'IT');
  static const JM = UserCountry._(r'JM');
  static const JP = UserCountry._(r'JP');
  static const JE = UserCountry._(r'JE');
  static const JO = UserCountry._(r'JO');
  static const KZ = UserCountry._(r'KZ');
  static const KE = UserCountry._(r'KE');
  static const KI = UserCountry._(r'KI');
  static const XK = UserCountry._(r'XK');
  static const KW = UserCountry._(r'KW');
  static const KG = UserCountry._(r'KG');
  static const LA = UserCountry._(r'LA');
  static const LV = UserCountry._(r'LV');
  static const LB = UserCountry._(r'LB');
  static const LS = UserCountry._(r'LS');
  static const LR = UserCountry._(r'LR');
  static const LY = UserCountry._(r'LY');
  static const LI = UserCountry._(r'LI');
  static const LT = UserCountry._(r'LT');
  static const LU = UserCountry._(r'LU');
  static const MO = UserCountry._(r'MO');
  static const MK = UserCountry._(r'MK');
  static const MG = UserCountry._(r'MG');
  static const MW = UserCountry._(r'MW');
  static const MY = UserCountry._(r'MY');
  static const MV = UserCountry._(r'MV');
  static const ML = UserCountry._(r'ML');
  static const MT = UserCountry._(r'MT');
  static const MH = UserCountry._(r'MH');
  static const MQ = UserCountry._(r'MQ');
  static const MR = UserCountry._(r'MR');
  static const MU = UserCountry._(r'MU');
  static const YT = UserCountry._(r'YT');
  static const MX = UserCountry._(r'MX');
  static const FM = UserCountry._(r'FM');
  static const MD = UserCountry._(r'MD');
  static const MC = UserCountry._(r'MC');
  static const MN = UserCountry._(r'MN');
  static const ME = UserCountry._(r'ME');
  static const MS = UserCountry._(r'MS');
  static const MA = UserCountry._(r'MA');
  static const MZ = UserCountry._(r'MZ');
  static const MM = UserCountry._(r'MM');
  static const NA = UserCountry._(r'NA');
  static const NR = UserCountry._(r'NR');
  static const NP = UserCountry._(r'NP');
  static const NL = UserCountry._(r'NL');
  static const NC = UserCountry._(r'NC');
  static const NZ = UserCountry._(r'NZ');
  static const NI = UserCountry._(r'NI');
  static const NE = UserCountry._(r'NE');
  static const NG = UserCountry._(r'NG');
  static const NU = UserCountry._(r'NU');
  static const NF = UserCountry._(r'NF');
  static const KP = UserCountry._(r'KP');
  static const MP = UserCountry._(r'MP');
  static const false_ = UserCountry._(r'false');
  static const OM = UserCountry._(r'OM');
  static const PK = UserCountry._(r'PK');
  static const PW = UserCountry._(r'PW');
  static const PS = UserCountry._(r'PS');
  static const PA = UserCountry._(r'PA');
  static const PG = UserCountry._(r'PG');
  static const PY = UserCountry._(r'PY');
  static const PE = UserCountry._(r'PE');
  static const PH = UserCountry._(r'PH');
  static const PN = UserCountry._(r'PN');
  static const PL = UserCountry._(r'PL');
  static const PT = UserCountry._(r'PT');
  static const PR = UserCountry._(r'PR');
  static const QA = UserCountry._(r'QA');
  static const RO = UserCountry._(r'RO');
  static const RU = UserCountry._(r'RU');
  static const RW = UserCountry._(r'RW');
  static const RE = UserCountry._(r'RE');
  static const BL = UserCountry._(r'BL');
  static const SH = UserCountry._(r'SH');
  static const KN = UserCountry._(r'KN');
  static const LC = UserCountry._(r'LC');
  static const MF = UserCountry._(r'MF');
  static const PM = UserCountry._(r'PM');
  static const WS = UserCountry._(r'WS');
  static const SM = UserCountry._(r'SM');
  static const SA = UserCountry._(r'SA');
  static const SN = UserCountry._(r'SN');
  static const RS = UserCountry._(r'RS');
  static const SC = UserCountry._(r'SC');
  static const SL = UserCountry._(r'SL');
  static const SG = UserCountry._(r'SG');
  static const SX = UserCountry._(r'SX');
  static const SK = UserCountry._(r'SK');
  static const SI = UserCountry._(r'SI');
  static const SB = UserCountry._(r'SB');
  static const SO = UserCountry._(r'SO');
  static const ZA = UserCountry._(r'ZA');
  static const GS = UserCountry._(r'GS');
  static const KR = UserCountry._(r'KR');
  static const SS = UserCountry._(r'SS');
  static const ES = UserCountry._(r'ES');
  static const LK = UserCountry._(r'LK');
  static const VC = UserCountry._(r'VC');
  static const SD = UserCountry._(r'SD');
  static const SR = UserCountry._(r'SR');
  static const SJ = UserCountry._(r'SJ');
  static const SZ = UserCountry._(r'SZ');
  static const SE = UserCountry._(r'SE');
  static const CH = UserCountry._(r'CH');
  static const SY = UserCountry._(r'SY');
  static const ST = UserCountry._(r'ST');
  static const TW = UserCountry._(r'TW');
  static const TJ = UserCountry._(r'TJ');
  static const TZ = UserCountry._(r'TZ');
  static const TH = UserCountry._(r'TH');
  static const TL = UserCountry._(r'TL');
  static const TG = UserCountry._(r'TG');
  static const TK = UserCountry._(r'TK');
  static const TO = UserCountry._(r'TO');
  static const TT = UserCountry._(r'TT');
  static const TA = UserCountry._(r'TA');
  static const TN = UserCountry._(r'TN');
  static const TR = UserCountry._(r'TR');
  static const TM = UserCountry._(r'TM');
  static const TC = UserCountry._(r'TC');
  static const TV = UserCountry._(r'TV');
  static const UM = UserCountry._(r'UM');
  static const VI = UserCountry._(r'VI');
  static const UG = UserCountry._(r'UG');
  static const UA = UserCountry._(r'UA');
  static const AE = UserCountry._(r'AE');
  static const GB = UserCountry._(r'GB');
  static const US = UserCountry._(r'US');
  static const UY = UserCountry._(r'UY');
  static const UZ = UserCountry._(r'UZ');
  static const VU = UserCountry._(r'VU');
  static const VA = UserCountry._(r'VA');
  static const VE = UserCountry._(r'VE');
  static const VN = UserCountry._(r'VN');
  static const WF = UserCountry._(r'WF');
  static const EH = UserCountry._(r'EH');
  static const YE = UserCountry._(r'YE');
  static const ZM = UserCountry._(r'ZM');
  static const ZW = UserCountry._(r'ZW');
  static const AX = UserCountry._(r'AX');

  /// List of all possible values in this [enum][UserCountry].
  static const values = <UserCountry>[
    AF,
    AL,
    DZ,
    AS,
    AD,
    AO,
    AI,
    AQ,
    AG,
    AR,
    AM,
    AW,
    AC,
    AU,
    AT,
    AZ,
    BS,
    BH,
    BD,
    BB,
    BY,
    BE,
    BZ,
    BJ,
    BM,
    BT,
    BO,
    BA,
    BW,
    BR,
    IO,
    VG,
    BN,
    BG,
    BF,
    BI,
    KH,
    CM,
    CA,
    IC,
    CV,
    BQ,
    KY,
    CF,
    EA,
    TD,
    CL,
    CN,
    CX,
    CC,
    CO,
    KM,
    CG,
    CD,
    CK,
    CR,
    HR,
    CU,
    CW,
    CY,
    CZ,
    CI,
    DK,
    DG,
    DJ,
    DM,
    DO,
    EC,
    EG,
    SV,
    GQ,
    ER,
    EE,
    ET,
    FK,
    FO,
    FJ,
    FI,
    FR,
    GF,
    PF,
    TF,
    GA,
    GM,
    GE,
    DE,
    GH,
    GI,
    GR,
    GL,
    GD,
    GP,
    GU,
    GT,
    GG,
    GN,
    GW,
    GY,
    HT,
    HN,
    HK,
    HU,
    IS,
    IN,
    ID,
    IR,
    IQ,
    IE,
    IM,
    IL,
    IT,
    JM,
    JP,
    JE,
    JO,
    KZ,
    KE,
    KI,
    XK,
    KW,
    KG,
    LA,
    LV,
    LB,
    LS,
    LR,
    LY,
    LI,
    LT,
    LU,
    MO,
    MK,
    MG,
    MW,
    MY,
    MV,
    ML,
    MT,
    MH,
    MQ,
    MR,
    MU,
    YT,
    MX,
    FM,
    MD,
    MC,
    MN,
    ME,
    MS,
    MA,
    MZ,
    MM,
    NA,
    NR,
    NP,
    NL,
    NC,
    NZ,
    NI,
    NE,
    NG,
    NU,
    NF,
    KP,
    MP,
    false_,
    OM,
    PK,
    PW,
    PS,
    PA,
    PG,
    PY,
    PE,
    PH,
    PN,
    PL,
    PT,
    PR,
    QA,
    RO,
    RU,
    RW,
    RE,
    BL,
    SH,
    KN,
    LC,
    MF,
    PM,
    WS,
    SM,
    SA,
    SN,
    RS,
    SC,
    SL,
    SG,
    SX,
    SK,
    SI,
    SB,
    SO,
    ZA,
    GS,
    KR,
    SS,
    ES,
    LK,
    VC,
    SD,
    SR,
    SJ,
    SZ,
    SE,
    CH,
    SY,
    ST,
    TW,
    TJ,
    TZ,
    TH,
    TL,
    TG,
    TK,
    TO,
    TT,
    TA,
    TN,
    TR,
    TM,
    TC,
    TV,
    UM,
    VI,
    UG,
    UA,
    AE,
    GB,
    US,
    UY,
    UZ,
    VU,
    VA,
    VE,
    VN,
    WF,
    EH,
    YE,
    ZM,
    ZW,
    AX,
  ];

  static UserCountry fromJson(dynamic value) =>
    UserCountryTypeTransformer().decode(value);

  static List<UserCountry>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <UserCountry>[]
      : json
          .map((value) => UserCountry.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [UserCountry] to String,
/// and [decode] dynamic data back to [UserCountry].
class UserCountryTypeTransformer {
  const UserCountryTypeTransformer._();

  factory UserCountryTypeTransformer() => _instance;

  String encode(UserCountry data) => data.value;

  /// Decodes a [dynamic value][data] to a UserCountry.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  UserCountry decode(dynamic data) {
    switch (data) {
      case r'AF': return UserCountry.AF;
      case r'AL': return UserCountry.AL;
      case r'DZ': return UserCountry.DZ;
      case r'AS': return UserCountry.AS;
      case r'AD': return UserCountry.AD;
      case r'AO': return UserCountry.AO;
      case r'AI': return UserCountry.AI;
      case r'AQ': return UserCountry.AQ;
      case r'AG': return UserCountry.AG;
      case r'AR': return UserCountry.AR;
      case r'AM': return UserCountry.AM;
      case r'AW': return UserCountry.AW;
      case r'AC': return UserCountry.AC;
      case r'AU': return UserCountry.AU;
      case r'AT': return UserCountry.AT;
      case r'AZ': return UserCountry.AZ;
      case r'BS': return UserCountry.BS;
      case r'BH': return UserCountry.BH;
      case r'BD': return UserCountry.BD;
      case r'BB': return UserCountry.BB;
      case r'BY': return UserCountry.BY;
      case r'BE': return UserCountry.BE;
      case r'BZ': return UserCountry.BZ;
      case r'BJ': return UserCountry.BJ;
      case r'BM': return UserCountry.BM;
      case r'BT': return UserCountry.BT;
      case r'BO': return UserCountry.BO;
      case r'BA': return UserCountry.BA;
      case r'BW': return UserCountry.BW;
      case r'BR': return UserCountry.BR;
      case r'IO': return UserCountry.IO;
      case r'VG': return UserCountry.VG;
      case r'BN': return UserCountry.BN;
      case r'BG': return UserCountry.BG;
      case r'BF': return UserCountry.BF;
      case r'BI': return UserCountry.BI;
      case r'KH': return UserCountry.KH;
      case r'CM': return UserCountry.CM;
      case r'CA': return UserCountry.CA;
      case r'IC': return UserCountry.IC;
      case r'CV': return UserCountry.CV;
      case r'BQ': return UserCountry.BQ;
      case r'KY': return UserCountry.KY;
      case r'CF': return UserCountry.CF;
      case r'EA': return UserCountry.EA;
      case r'TD': return UserCountry.TD;
      case r'CL': return UserCountry.CL;
      case r'CN': return UserCountry.CN;
      case r'CX': return UserCountry.CX;
      case r'CC': return UserCountry.CC;
      case r'CO': return UserCountry.CO;
      case r'KM': return UserCountry.KM;
      case r'CG': return UserCountry.CG;
      case r'CD': return UserCountry.CD;
      case r'CK': return UserCountry.CK;
      case r'CR': return UserCountry.CR;
      case r'HR': return UserCountry.HR;
      case r'CU': return UserCountry.CU;
      case r'CW': return UserCountry.CW;
      case r'CY': return UserCountry.CY;
      case r'CZ': return UserCountry.CZ;
      case r'CI': return UserCountry.CI;
      case r'DK': return UserCountry.DK;
      case r'DG': return UserCountry.DG;
      case r'DJ': return UserCountry.DJ;
      case r'DM': return UserCountry.DM;
      case r'DO': return UserCountry.DO;
      case r'EC': return UserCountry.EC;
      case r'EG': return UserCountry.EG;
      case r'SV': return UserCountry.SV;
      case r'GQ': return UserCountry.GQ;
      case r'ER': return UserCountry.ER;
      case r'EE': return UserCountry.EE;
      case r'ET': return UserCountry.ET;
      case r'FK': return UserCountry.FK;
      case r'FO': return UserCountry.FO;
      case r'FJ': return UserCountry.FJ;
      case r'FI': return UserCountry.FI;
      case r'FR': return UserCountry.FR;
      case r'GF': return UserCountry.GF;
      case r'PF': return UserCountry.PF;
      case r'TF': return UserCountry.TF;
      case r'GA': return UserCountry.GA;
      case r'GM': return UserCountry.GM;
      case r'GE': return UserCountry.GE;
      case r'DE': return UserCountry.DE;
      case r'GH': return UserCountry.GH;
      case r'GI': return UserCountry.GI;
      case r'GR': return UserCountry.GR;
      case r'GL': return UserCountry.GL;
      case r'GD': return UserCountry.GD;
      case r'GP': return UserCountry.GP;
      case r'GU': return UserCountry.GU;
      case r'GT': return UserCountry.GT;
      case r'GG': return UserCountry.GG;
      case r'GN': return UserCountry.GN;
      case r'GW': return UserCountry.GW;
      case r'GY': return UserCountry.GY;
      case r'HT': return UserCountry.HT;
      case r'HN': return UserCountry.HN;
      case r'HK': return UserCountry.HK;
      case r'HU': return UserCountry.HU;
      case r'IS': return UserCountry.IS;
      case r'IN': return UserCountry.IN;
      case r'ID': return UserCountry.ID;
      case r'IR': return UserCountry.IR;
      case r'IQ': return UserCountry.IQ;
      case r'IE': return UserCountry.IE;
      case r'IM': return UserCountry.IM;
      case r'IL': return UserCountry.IL;
      case r'IT': return UserCountry.IT;
      case r'JM': return UserCountry.JM;
      case r'JP': return UserCountry.JP;
      case r'JE': return UserCountry.JE;
      case r'JO': return UserCountry.JO;
      case r'KZ': return UserCountry.KZ;
      case r'KE': return UserCountry.KE;
      case r'KI': return UserCountry.KI;
      case r'XK': return UserCountry.XK;
      case r'KW': return UserCountry.KW;
      case r'KG': return UserCountry.KG;
      case r'LA': return UserCountry.LA;
      case r'LV': return UserCountry.LV;
      case r'LB': return UserCountry.LB;
      case r'LS': return UserCountry.LS;
      case r'LR': return UserCountry.LR;
      case r'LY': return UserCountry.LY;
      case r'LI': return UserCountry.LI;
      case r'LT': return UserCountry.LT;
      case r'LU': return UserCountry.LU;
      case r'MO': return UserCountry.MO;
      case r'MK': return UserCountry.MK;
      case r'MG': return UserCountry.MG;
      case r'MW': return UserCountry.MW;
      case r'MY': return UserCountry.MY;
      case r'MV': return UserCountry.MV;
      case r'ML': return UserCountry.ML;
      case r'MT': return UserCountry.MT;
      case r'MH': return UserCountry.MH;
      case r'MQ': return UserCountry.MQ;
      case r'MR': return UserCountry.MR;
      case r'MU': return UserCountry.MU;
      case r'YT': return UserCountry.YT;
      case r'MX': return UserCountry.MX;
      case r'FM': return UserCountry.FM;
      case r'MD': return UserCountry.MD;
      case r'MC': return UserCountry.MC;
      case r'MN': return UserCountry.MN;
      case r'ME': return UserCountry.ME;
      case r'MS': return UserCountry.MS;
      case r'MA': return UserCountry.MA;
      case r'MZ': return UserCountry.MZ;
      case r'MM': return UserCountry.MM;
      case r'NA': return UserCountry.NA;
      case r'NR': return UserCountry.NR;
      case r'NP': return UserCountry.NP;
      case r'NL': return UserCountry.NL;
      case r'NC': return UserCountry.NC;
      case r'NZ': return UserCountry.NZ;
      case r'NI': return UserCountry.NI;
      case r'NE': return UserCountry.NE;
      case r'NG': return UserCountry.NG;
      case r'NU': return UserCountry.NU;
      case r'NF': return UserCountry.NF;
      case r'KP': return UserCountry.KP;
      case r'MP': return UserCountry.MP;
      case r'false': return UserCountry.false_;
      case r'OM': return UserCountry.OM;
      case r'PK': return UserCountry.PK;
      case r'PW': return UserCountry.PW;
      case r'PS': return UserCountry.PS;
      case r'PA': return UserCountry.PA;
      case r'PG': return UserCountry.PG;
      case r'PY': return UserCountry.PY;
      case r'PE': return UserCountry.PE;
      case r'PH': return UserCountry.PH;
      case r'PN': return UserCountry.PN;
      case r'PL': return UserCountry.PL;
      case r'PT': return UserCountry.PT;
      case r'PR': return UserCountry.PR;
      case r'QA': return UserCountry.QA;
      case r'RO': return UserCountry.RO;
      case r'RU': return UserCountry.RU;
      case r'RW': return UserCountry.RW;
      case r'RE': return UserCountry.RE;
      case r'BL': return UserCountry.BL;
      case r'SH': return UserCountry.SH;
      case r'KN': return UserCountry.KN;
      case r'LC': return UserCountry.LC;
      case r'MF': return UserCountry.MF;
      case r'PM': return UserCountry.PM;
      case r'WS': return UserCountry.WS;
      case r'SM': return UserCountry.SM;
      case r'SA': return UserCountry.SA;
      case r'SN': return UserCountry.SN;
      case r'RS': return UserCountry.RS;
      case r'SC': return UserCountry.SC;
      case r'SL': return UserCountry.SL;
      case r'SG': return UserCountry.SG;
      case r'SX': return UserCountry.SX;
      case r'SK': return UserCountry.SK;
      case r'SI': return UserCountry.SI;
      case r'SB': return UserCountry.SB;
      case r'SO': return UserCountry.SO;
      case r'ZA': return UserCountry.ZA;
      case r'GS': return UserCountry.GS;
      case r'KR': return UserCountry.KR;
      case r'SS': return UserCountry.SS;
      case r'ES': return UserCountry.ES;
      case r'LK': return UserCountry.LK;
      case r'VC': return UserCountry.VC;
      case r'SD': return UserCountry.SD;
      case r'SR': return UserCountry.SR;
      case r'SJ': return UserCountry.SJ;
      case r'SZ': return UserCountry.SZ;
      case r'SE': return UserCountry.SE;
      case r'CH': return UserCountry.CH;
      case r'SY': return UserCountry.SY;
      case r'ST': return UserCountry.ST;
      case r'TW': return UserCountry.TW;
      case r'TJ': return UserCountry.TJ;
      case r'TZ': return UserCountry.TZ;
      case r'TH': return UserCountry.TH;
      case r'TL': return UserCountry.TL;
      case r'TG': return UserCountry.TG;
      case r'TK': return UserCountry.TK;
      case r'TO': return UserCountry.TO;
      case r'TT': return UserCountry.TT;
      case r'TA': return UserCountry.TA;
      case r'TN': return UserCountry.TN;
      case r'TR': return UserCountry.TR;
      case r'TM': return UserCountry.TM;
      case r'TC': return UserCountry.TC;
      case r'TV': return UserCountry.TV;
      case r'UM': return UserCountry.UM;
      case r'VI': return UserCountry.VI;
      case r'UG': return UserCountry.UG;
      case r'UA': return UserCountry.UA;
      case r'AE': return UserCountry.AE;
      case r'GB': return UserCountry.GB;
      case r'US': return UserCountry.US;
      case r'UY': return UserCountry.UY;
      case r'UZ': return UserCountry.UZ;
      case r'VU': return UserCountry.VU;
      case r'VA': return UserCountry.VA;
      case r'VE': return UserCountry.VE;
      case r'VN': return UserCountry.VN;
      case r'WF': return UserCountry.WF;
      case r'EH': return UserCountry.EH;
      case r'YE': return UserCountry.YE;
      case r'ZM': return UserCountry.ZM;
      case r'ZW': return UserCountry.ZW;
      case r'AX': return UserCountry.AX;
      default:
        return UserCountry.DE;
    }
  }

  /// Singleton [UserCountryTypeTransformer] instance.
  static UserCountryTypeTransformer _instance = UserCountryTypeTransformer._();
}