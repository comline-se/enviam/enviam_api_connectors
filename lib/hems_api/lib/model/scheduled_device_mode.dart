//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

/// Mode that is configured for the device. It can either be: * ON - turned on * OFF - turned off * RESET - not optimized, but free to be switched by the user. It puts the device into a (device)specific state   at the beginning of the timeframe(f.e. off for plug, charge for electric vehicle charging stations) and   leave it untouched from there on
class ScheduledDeviceMode {
  /// Instantiate a new enum with the provided [value].
  const ScheduledDeviceMode._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const ON = ScheduledDeviceMode._(r'ON');
  static const OFF = ScheduledDeviceMode._(r'OFF');
  static const RESET = ScheduledDeviceMode._(r'RESET');

  /// List of all possible values in this [enum][ScheduledDeviceMode].
  static const values = <ScheduledDeviceMode>[
    ON,
    OFF,
    RESET,
  ];

  static ScheduledDeviceMode fromJson(dynamic value) =>
    ScheduledDeviceModeTypeTransformer().decode(value);

  static List<ScheduledDeviceMode>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <ScheduledDeviceMode>[]
      : json
          .map((value) => ScheduledDeviceMode.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [ScheduledDeviceMode] to String,
/// and [decode] dynamic data back to [ScheduledDeviceMode].
class ScheduledDeviceModeTypeTransformer {
  const ScheduledDeviceModeTypeTransformer._();

  factory ScheduledDeviceModeTypeTransformer() => _instance;

  String encode(ScheduledDeviceMode data) => data.value;

  /// Decodes a [dynamic value][data] to a ScheduledDeviceMode.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  ScheduledDeviceMode decode(dynamic data) {
    switch (data) {
      case r'ON': return ScheduledDeviceMode.ON;
      case r'OFF': return ScheduledDeviceMode.OFF;
      case r'RESET': return ScheduledDeviceMode.RESET;
      default:
        throw ArgumentError('Unknown enum value to decode: $data');

    }
  }

  /// Singleton [ScheduledDeviceModeTypeTransformer] instance.
  static ScheduledDeviceModeTypeTransformer _instance = ScheduledDeviceModeTypeTransformer._();
}
