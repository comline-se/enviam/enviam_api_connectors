//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

/// Configurable parameters of the device. Can be: * MIN_RUNTIME - minimum duration a device needs to run after it was turned ON * MIN_RESTTIME - minimum duration a device needs to stay OFF after it was turned OFF * MUST_RUNTIME- minimum duration a device needs to run over the course of a day(can be summed up)
class DeviceConfigParameters {
  /// Instantiate a new enum with the provided [value].
  const DeviceConfigParameters._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const MIN_RUNTIME = DeviceConfigParameters._(r'MIN_RUNTIME');
  static const MIN_RESTTIME = DeviceConfigParameters._(r'MIN_RESTTIME');
  static const MUST_RUNTIME = DeviceConfigParameters._(r'MUST_RUNTIME');

  /// List of all possible values in this [enum][DeviceConfigParameters].
  static const values = <DeviceConfigParameters>[
    MIN_RUNTIME,
    MIN_RESTTIME,
    MUST_RUNTIME,
  ];

  static DeviceConfigParameters fromJson(dynamic value) =>
    DeviceConfigParametersTypeTransformer().decode(value);

  static List<DeviceConfigParameters>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <DeviceConfigParameters>[]
      : json
          .map((value) => DeviceConfigParameters.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [DeviceConfigParameters] to String,
/// and [decode] dynamic data back to [DeviceConfigParameters].
class DeviceConfigParametersTypeTransformer {
  const DeviceConfigParametersTypeTransformer._();

  factory DeviceConfigParametersTypeTransformer() => _instance;

  String encode(DeviceConfigParameters data) => data.value;

  /// Decodes a [dynamic value][data] to a DeviceConfigParameters.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  DeviceConfigParameters decode(dynamic data) {
    switch (data) {
      case r'MIN_RUNTIME': return DeviceConfigParameters.MIN_RUNTIME;
      case r'MIN_RESTTIME': return DeviceConfigParameters.MIN_RESTTIME;
      case r'MUST_RUNTIME': return DeviceConfigParameters.MUST_RUNTIME;
      default:
        throw ArgumentError('Unknown enum value to decode: $data');
    }
  }

  /// Singleton [DeviceConfigParametersTypeTransformer] instance.
  static DeviceConfigParametersTypeTransformer _instance = DeviceConfigParametersTypeTransformer._();
}
