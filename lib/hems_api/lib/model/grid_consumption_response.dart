//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class GridConsumptionResponse {
  /// Returns a new [GridConsumptionResponse] instance.
  GridConsumptionResponse({
    required this.consumption,
  });

  Graph consumption;

  @override
  bool operator ==(Object other) => identical(this, other) || other is GridConsumptionResponse &&
     other.consumption == consumption;

  @override
  int get hashCode =>
    (consumption == null ? 0 : consumption.hashCode);

  @override
  String toString() => 'GridConsumptionResponse[consumption=$consumption]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'consumption'] = consumption;
    return json;
  }

  /// Returns a new [GridConsumptionResponse] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static GridConsumptionResponse fromJson(Map<String, dynamic> json) =>
      GridConsumptionResponse(
        consumption: Graph.fromJson(json[r'consumption']),
    );

  static List<GridConsumptionResponse> listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? [] : <GridConsumptionResponse>[]
      : json.map((dynamic value) => GridConsumptionResponse.fromJson(value)).toList(growable: true == growable);

  static Map<String, GridConsumptionResponse> mapFromJson(Map<String, dynamic> json) {
    final map = <String, GridConsumptionResponse>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = GridConsumptionResponse.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of GridConsumptionResponse-objects as value to a dart map
  static Map<String, List<GridConsumptionResponse>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<GridConsumptionResponse>>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = GridConsumptionResponse.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

