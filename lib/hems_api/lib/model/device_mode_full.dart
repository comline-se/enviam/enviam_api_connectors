//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

/// Current mode of the device.
class DeviceModeFull {
  /// Instantiate a new enum with the provided [value].
  const DeviceModeFull._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const ACCUMULATION = DeviceModeFull._(r'ACCUMULATION');
  static const ACTIVATION_RECOMMENDATION = DeviceModeFull._(r'ACTIVATION_RECOMMENDATION');
  static const ACTIVE = DeviceModeFull._(r'ACTIVE');
  static const AUTOMATIC = DeviceModeFull._(r'AUTOMATIC');
  static const CHARGING = DeviceModeFull._(r'CHARGING');
  static const CHARGING_PV = DeviceModeFull._(r'CHARGING_PV');
  static const CHARGING_GRID = DeviceModeFull._(r'CHARGING_GRID');
  static const DISCHARGING = DeviceModeFull._(r'DISCHARGING');
  static const DISCHARGING_GRID = DeviceModeFull._(r'DISCHARGING_GRID');
  static const DISCONNECTED = DeviceModeFull._(r'DISCONNECTED');
  static const ERROR = DeviceModeFull._(r'ERROR');
  static const EQUALIZING_CHARGE = DeviceModeFull._(r'EQUALIZING_CHARGE');
  static const OFF = DeviceModeFull._(r'OFF');
  static const ON = DeviceModeFull._(r'ON');
  static const STANDBY = DeviceModeFull._(r'STANDBY');
  static const UTILITY_LOCK = DeviceModeFull._(r'UTILITY_LOCK');

  /// List of all possible values in this [enum][DeviceModeFull].
  static const values = <DeviceModeFull>[
    ACCUMULATION,
    ACTIVATION_RECOMMENDATION,
    ACTIVE,
    AUTOMATIC,
    CHARGING,
    CHARGING_PV,
    CHARGING_GRID,
    DISCHARGING,
    DISCHARGING_GRID,
    DISCONNECTED,
    ERROR,
    EQUALIZING_CHARGE,
    OFF,
    ON,
    STANDBY,
    UTILITY_LOCK,
  ];

  static DeviceModeFull fromJson(dynamic value) =>
    DeviceModeFullTypeTransformer().decode(value);

  static List<DeviceModeFull>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <DeviceModeFull>[]
      : json
          .map((value) => DeviceModeFull.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [DeviceModeFull] to String,
/// and [decode] dynamic data back to [DeviceModeFull].
class DeviceModeFullTypeTransformer {
  const DeviceModeFullTypeTransformer._();

  factory DeviceModeFullTypeTransformer() => _instance;

  String encode(DeviceModeFull data) => data.value;

  /// Decodes a [dynamic value][data] to a DeviceModeFull.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  DeviceModeFull decode(dynamic data) {
    switch (data) {
      case r'ACCUMULATION': return DeviceModeFull.ACCUMULATION;
      case r'ACTIVATION_RECOMMENDATION': return DeviceModeFull.ACTIVATION_RECOMMENDATION;
      case r'ACTIVE': return DeviceModeFull.ACTIVE;
      case r'AUTOMATIC': return DeviceModeFull.AUTOMATIC;
      case r'CHARGING': return DeviceModeFull.CHARGING;
      case r'CHARGING_PV': return DeviceModeFull.CHARGING_PV;
      case r'CHARGING_GRID': return DeviceModeFull.CHARGING_GRID;
      case r'DISCHARGING': return DeviceModeFull.DISCHARGING;
      case r'DISCHARGING_GRID': return DeviceModeFull.DISCHARGING_GRID;
      case r'DISCONNECTED': return DeviceModeFull.DISCONNECTED;
      case r'ERROR': return DeviceModeFull.ERROR;
      case r'EQUALIZING_CHARGE': return DeviceModeFull.EQUALIZING_CHARGE;
      case r'OFF': return DeviceModeFull.OFF;
      case r'ON': return DeviceModeFull.ON;
      case r'STANDBY': return DeviceModeFull.STANDBY;
      case r'UTILITY_LOCK': return DeviceModeFull.UTILITY_LOCK;
      default:
          throw ArgumentError('Unknown enum value to decode: $data');
    }
  }

  /// Singleton [DeviceModeFullTypeTransformer] instance.
  static DeviceModeFullTypeTransformer _instance = DeviceModeFullTypeTransformer._();
}
