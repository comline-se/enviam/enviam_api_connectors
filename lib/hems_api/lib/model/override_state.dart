//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class OverrideState {
  /// Returns a new [OverrideState] instance.
  OverrideState({
    required this.targetState,
    required this.endOfOverride,
  });

  SwitchState targetState;

  /// The end of the override, delivered as UTC.
  DateTime? endOfOverride;

  @override
  bool operator ==(Object other) => identical(this, other) || other is OverrideState &&
     other.targetState == targetState &&
     other.endOfOverride == endOfOverride;

  @override
  int get hashCode =>
    (targetState == null ? 0 : targetState.hashCode) +
    (endOfOverride == null ? 0 : endOfOverride.hashCode);

  @override
  String toString() => 'OverrideState[targetState=$targetState, endOfOverride=$endOfOverride]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'targetState'] = targetState;
      json[r'endOfOverride'] = endOfOverride != null ? endOfOverride!.toUtc().toIso8601String() : null;
    return json;
  }

  /// Returns a new [OverrideState] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static OverrideState fromJson(Map<String, dynamic> json) =>
      OverrideState(
        targetState: SwitchState.fromJson(json[r'targetState']),
        endOfOverride: json[r'endOfOverride'] == null
          ? null
          : DateTime.parse(json[r'endOfOverride']),
    );

  static List<OverrideState> listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? [] : <OverrideState>[]
      : json.map((dynamic value) => OverrideState.fromJson(value)).toList(growable: true == growable);

  static Map<String, OverrideState> mapFromJson(Map<String, dynamic> json) {
    final map = <String, OverrideState>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = OverrideState.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of OverrideState-objects as value to a dart map
  static Map<String, List<OverrideState>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<OverrideState>>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = OverrideState.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

