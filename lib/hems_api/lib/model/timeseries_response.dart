//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class TimeseriesResponse {
  /// Returns a new [TimeseriesResponse] instance.
  TimeseriesResponse({
    this.timeseries = const [],
  });

  /// The queried timeseries
  List<Timeseries> timeseries;

  @override
  bool operator ==(Object other) => identical(this, other) || other is TimeseriesResponse &&
     other.timeseries == timeseries;

  @override
  int get hashCode =>
    (timeseries.hashCode);

  @override
  String toString() => 'TimeseriesResponse[timeseries=$timeseries]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'timeseries'] = timeseries;
    return json;
  }

  /// Returns a new [TimeseriesResponse] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static TimeseriesResponse fromJson(Map<String, dynamic> json) =>
      TimeseriesResponse(
        timeseries: Timeseries.listFromJson(json[r'timeseries']),
      );

  static List<TimeseriesResponse> listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? [] : <TimeseriesResponse>[]
      : json.map((dynamic value) => TimeseriesResponse.fromJson(value)).toList(growable: true == growable);

  static Map<String, TimeseriesResponse> mapFromJson(Map<String, dynamic> json) {
    final map = <String, TimeseriesResponse>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) => map[key] = TimeseriesResponse.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of TimeseriesResponse-objects as value to a dart map
  static Map<String, List<TimeseriesResponse>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<TimeseriesResponse>>{};
    if (json.isNotEmpty == true) {
      json.forEach((key, value) {
        map[key] = TimeseriesResponse.listFromJson(value, emptyIsNull: emptyIsNull, growable: growable,);
      });
    }
    return map;
  }
}

