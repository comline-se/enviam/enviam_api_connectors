//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class UserTemperature {
  /// Instantiate a new enum with the provided [value].
  const UserTemperature._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const CELCIUS = UserTemperature._(r'CELCIUS');
  static const FAHRENHEIT = UserTemperature._(r'FAHRENHEIT');

  /// List of all possible values in this [enum][UserTemperature].
  static const values = <UserTemperature>[
    CELCIUS,
    FAHRENHEIT,
  ];

  static UserTemperature fromJson(dynamic value) =>
    UserTemperatureTypeTransformer().decode(value);

  static List<UserTemperature>? listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? null : <UserTemperature>[]
      : json
          .map((value) => UserTemperature.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [UserTemperature] to String,
/// and [decode] dynamic data back to [UserTemperature].
class UserTemperatureTypeTransformer {
  const UserTemperatureTypeTransformer._();

  factory UserTemperatureTypeTransformer() => _instance;

  String encode(UserTemperature data) => data.value;

  /// Decodes a [dynamic value][data] to a UserTemperature.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  UserTemperature decode(dynamic data) {
    switch (data) {
      case r'CELCIUS': return UserTemperature.CELCIUS;
      case r'FAHRENHEIT': return UserTemperature.FAHRENHEIT;
      default:
        return UserTemperature.CELCIUS;
    }
  }

  /// Singleton [UserTemperatureTypeTransformer] instance.
  static UserTemperatureTypeTransformer _instance = UserTemperatureTypeTransformer._();
}
