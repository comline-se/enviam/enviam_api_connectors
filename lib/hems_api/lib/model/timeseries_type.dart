//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

/// The type of timeseries to query:  * WORK: The work timeseries  * POWER: The power timeseries 
class TimeseriesType {
  /// Instantiate a new enum with the provided [value].
  const TimeseriesType._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const WORK = TimeseriesType._(r'WORK');
  static const POWER = TimeseriesType._(r'POWER');

  /// List of all possible values in this [enum][TimeseriesType].
  static const values = <TimeseriesType>[
    WORK,
    POWER,
  ];

  static TimeseriesType fromJson(dynamic value) =>
    TimeseriesTypeTypeTransformer().decode(value);

  static List<TimeseriesType> listFromJson(List<dynamic> json, {bool emptyIsNull = false, bool growable = false,}) =>
    json.isEmpty
      ? true == emptyIsNull ? [] : <TimeseriesType>[]
      : json
          .map((value) => TimeseriesType.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [TimeseriesType] to String,
/// and [decode] dynamic data back to [TimeseriesType].
class TimeseriesTypeTypeTransformer {
  const TimeseriesTypeTypeTransformer._();

  factory TimeseriesTypeTypeTransformer() => _instance;

  String encode(TimeseriesType data) => data.value;

  /// Decodes a [dynamic value][data] to a TimeseriesType.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  TimeseriesType decode(dynamic data) {
    switch (data) {
      case r'WORK': return TimeseriesType.WORK;
      case r'POWER': return TimeseriesType.POWER;
      default:
        return TimeseriesType.POWER;
    }
  }

  /// Singleton [TimeseriesTypeTypeTransformer] instance.
  static TimeseriesTypeTypeTransformer _instance = TimeseriesTypeTypeTransformer._();
}
