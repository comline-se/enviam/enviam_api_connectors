//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class GridApi {
  GridApi([ApiClient? apiClient]) : apiClient = apiClient ?? defaultApiClient;

  final ApiClient apiClient;

  /// Grid consumption.
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [DateTime] from (required):
  ///   Timestamp for start of a range in [rfc3339](https://tools.ietf.org/html/rfc3339#section-5.6) format.
  ///
  /// * [DateTime] to (required):
  ///   Timestamp for end of a range in [rfc3339](https://tools.ietf.org/html/rfc3339#section-5.6) format.
  ///
  /// * [String] resolution (required):
  ///   Resolution for values using [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601#Durations) durations format.
  ///
  /// * [Fill] fill:
  ///   A filler used to handle missing values in time series.
  Future<Response> getGridConsumptionWithHttpInfo(DateTime from, DateTime to, String resolution, { Fill? fill}) async {
    // Verify required params are set.

    final path = r'/grid/consumption';

    Object? postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

      queryParams.addAll(_convertParametersForCollectionFormat('', 'from', from));
      queryParams.addAll(_convertParametersForCollectionFormat('', 'to', to));
      queryParams.addAll(_convertParametersForCollectionFormat('', 'resolution', resolution));
    if (fill != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'fill', fill));
    }

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Grid consumption.
  ///
  /// Parameters:
  ///
  /// * [DateTime] from (required):
  ///   Timestamp for start of a range in [rfc3339](https://tools.ietf.org/html/rfc3339#section-5.6) format.
  ///
  /// * [DateTime] to (required):
  ///   Timestamp for end of a range in [rfc3339](https://tools.ietf.org/html/rfc3339#section-5.6) format.
  ///
  /// * [String] resolution (required):
  ///   Resolution for values using [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601#Durations) durations format.
  ///
  /// * [Fill] fill:
  ///   A filler used to handle missing values in time series.
  Future<GridConsumptionResponse> getGridConsumption(DateTime from, DateTime to, String resolution, { Fill? fill }) async {
    final response = await getGridConsumptionWithHttpInfo(from, to, resolution,  fill: fill );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'GridConsumptionResponse',) as GridConsumptionResponse;
        }
    return Future<GridConsumptionResponse>.value(null);
  }
}
