//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//


// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class EvStationApi {
  EvStationApi([ApiClient? apiClient]) : apiClient = apiClient ?? defaultApiClient;

  final ApiClient apiClient;

  /// Get a single EV station.
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] deviceId (required):
  ///   Unique device ID for identifying the device inside the HEMS and Kiwigrid context.
  Future<Response> getEVStationWithHttpInfo(String deviceId) async {

    final path = r'/evstation/{deviceId}'
      .replaceAll('{' + 'deviceId' + '}', deviceId.toString());

    Object? postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final String? nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Get a single EV station.
  ///
  /// Parameters:
  ///
  /// * [String] deviceId (required):
  ///   Unique device ID for identifying the device inside the HEMS and Kiwigrid context.
  Future<EVStation> getEVStation(String deviceId) async {
    final response = await getEVStationWithHttpInfo(deviceId);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'EVStation',) as EVStation;
        }
    return Future<EVStation>.value(null);
  }

  /// Returns all available EV stations.
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> getEVStationsWithHttpInfo() async {
    final path = r'/evstation';

    Object? postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Returns all available EV stations.
  Future<List<EVStation>> getEVStations() async {
    final response = await getEVStationsWithHttpInfo();
    // Users may not have Authorization to request EVStations, default return an empty list
    if (response.statusCode >= HttpStatus.unauthorized) {
      return Future.value([]);
    }

    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.statusCode != HttpStatus.noContent) {
      return (await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'List<EVStation>') as List)
        .cast<EVStation>()
        .toList(growable: false);
    }
    return Future<List<EVStation>>.value(null);
  }

  /// Updates the values of an EV station.
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] deviceId (required):
  ///   Unique device ID for identifying the device inside the HEMS and Kiwigrid context.
  ///
  /// * [EVStationValues] eVStationValues:
  Future<Response> patchEVStationWithHttpInfo(String deviceId, { required EVStationValues eVStationValues }) async {

    final path = r'/evstation/{deviceId}'
      .replaceAll('{' + 'deviceId' + '}', deviceId.toString());

    Object postBody = eVStationValues;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'PATCH',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Updates the values of an EV station.
  ///
  /// Parameters:
  ///
  /// * [String] deviceId (required):
  ///   Unique device ID for identifying the device inside the HEMS and Kiwigrid context.
  ///
  /// * [EVStationValues] eVStationValues:
  Future<void> patchEVStation(String deviceId, { required EVStationValues eVStationValues }) async {
    final response = await patchEVStationWithHttpInfo(deviceId,  eVStationValues: eVStationValues );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
  }

  /// Updates the charging mode of an EV station.
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] deviceId (required):
  ///   Unique device ID for identifying the device inside the HEMS and Kiwigrid context.
  ///
  /// * [EVStationChargingModeRequest] eVStationChargingModeRequest:
  Future<Response> patchEVStationModeWithHttpInfo(String deviceId, { required EVStationChargingModeRequest eVStationChargingModeRequest }) async {

    final path = r'/evstation/{deviceId}/mode'
      .replaceAll('{' + 'deviceId' + '}', deviceId.toString());

    Object postBody = eVStationChargingModeRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['BearerAuth'];


    return await apiClient.invokeAPI(
      path,
      'PATCH',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Updates the charging mode of an EV station.
  ///
  /// Parameters:
  ///
  /// * [String] deviceId (required):
  ///   Unique device ID for identifying the device inside the HEMS and Kiwigrid context.
  ///
  /// * [EVStationChargingModeRequest] eVStationChargingModeRequest:
  Future<void> patchEVStationMode(String deviceId, { required EVStationChargingModeRequest eVStationChargingModeRequest }) async {
    final response = await patchEVStationModeWithHttpInfo(deviceId,  eVStationChargingModeRequest: eVStationChargingModeRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
  }
}
