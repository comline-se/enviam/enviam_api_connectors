# hems_api.model.EnergyFlowPowerBattery

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**soc** | **double** | Current state of charge. Will be empty if no SoC is available. If multiple batteries with SoC are present the overall SoC is calculated otherwise the SoC is empty. | [optional] 
**in_** | **int** | Power buffered in battery. | 
**out_** | **int** | Power released from battery. | 
**balance** | **int** | Power in - out. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


