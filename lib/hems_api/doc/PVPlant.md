# hems_api.model.PVPlant

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | A unique ID for the device. | 
**name** | **String** | The devices displayed name (changeable by the user). | 
**state** | [**DeviceState**](DeviceState.md) |  | 
**type** | [**DeviceType**](DeviceType.md) |  | 
**class_** | **String** | Device class defines the properties and handling of a device. | [optional] 
**stateDevice** | **String** | The device state indication proper behavior or potential problems. | 
**stateErrorList** | **List<String>** | A list of device errors if there is one. | [default to const []]
**dateInstallation** | **String** | Date the device has been installed at the location. | [optional] 
**degreeDirection** | **num** | The direction the device (mostly PV plants) has been installed in. | [optional] 
**degreeInclination** | **num** | The angle the device (mostly PV plants) has been installed in. | [optional] 
**deration** | **String** | The devices deration. | [optional] 
**idInverterList** | **List<String>** | A list of related inverters. | [optional] [default to const []]
**idManufacturer** | **String** | The device manufacturer id. | [optional] 
**idName** | **String** | The devices displayed name (changeable by the user). | [optional] 
**powerAcOutMax** | **num** | Maximum power AC output currently set for the device. | [optional] 
**powerInstalledPeak** | **num** | Maximum installed peak power output of the device. | [optional] 
**mountingType** | **String** | The mounting type of the device. | [optional] 
**orientation** | **String** | The devices orientation. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


