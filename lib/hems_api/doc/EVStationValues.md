# hems_api.model.EVStationValues

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**car** | [**Car**](Car.md) |  | 
**gridLimit** | **num** | Power limit (in watt) for use in grid optimized use case. | 
**lowestPower** | **num** | Lower threshold for excess charging in watt. When pv excess reaches this value, the wallbox is set to charging. | 
**maxPower** | **num** | Maximum charging power in watt. | 
**name** | **String** | The devices displayed name (changeable by the user). | [optional] 
**optimizationMode** | [**DeviceOptimizationMode**](DeviceOptimizationMode.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


