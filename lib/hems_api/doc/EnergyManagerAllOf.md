# hems_api.model.EnergyManagerAllOf

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dateCloudLastSeen** | **String** | Last contact to the device. | [optional] 
**idFirmware** | **String** | The devices firmware ID. | [optional] 
**idModelCode** | **String** | The devices ID model code. | [optional] 
**idName** | **String** | The devices displayed name (changeable by the user). | [optional] 
**idSerialNumber** | **String** | The devices serial number ID. | [optional] 
**idTimeZone** | **String** | The timezone the device is set up for. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


