# hems_api.model.HomeSelfSupplyResponse

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**selfSufficiency** | [**HomeSelfSupplyResponseSelfSufficiency**](HomeSelfSupplyResponseSelfSufficiency.md) |  | 
**selfSufficiencyComposition** | [**HomeSelfSupplyResponseSelfSufficiencyComposition**](HomeSelfSupplyResponseSelfSufficiencyComposition.md) |  | 
**selfConsumption** | [**HomeSelfSupplyResponseSelfSufficiency**](HomeSelfSupplyResponseSelfSufficiency.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


