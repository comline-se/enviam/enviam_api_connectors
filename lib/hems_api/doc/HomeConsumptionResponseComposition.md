# hems_api.model.HomeConsumptionResponseComposition

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**battery** | **double** |  | [optional] 
**grid** | **double** |  | [optional] 
**pv** | **double** |  | [optional] 
**consumers** | [**List<HomeConsumptionResponseCompositionConsumers>**](HomeConsumptionResponseCompositionConsumers.md) |  | [optional] [default to const []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


