# hems_api.model.InverterAllOf

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idFirmware** | **String** | The devices firmware ID. | [optional] 
**idManufacturer** | **String** | The device manufacturer id. | [optional] 
**idModelCode** | **String** | The devices ID model code. | [optional] 
**idName** | **String** | The devices displayed name (changeable by the user). | [optional] 
**idSerialNumber** | **String** | The devices serial number ID. | [optional] 
**powerAcOutMax** | **num** | Maximum power AC output currently set for the device. | [optional] 
**powerInstalledPeak** | **num** | Maximum installed peak power output of the device. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


