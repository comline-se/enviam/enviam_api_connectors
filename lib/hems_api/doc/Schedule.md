# hems_api.model.Schedule

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**zoneId** | **String** | Id of the timezone (e.g. Europe/Berlin, UTC+1 or UTC+01:00) that the schedule should use. | [default to 'Europe/Berlin']
**monday** | [**List<TimeSpan>**](TimeSpan.md) | A list of timespans for one day. The span entries are not allowed to overlap. | [optional] [default to const []]
**tuesday** | [**List<TimeSpan>**](TimeSpan.md) | A list of timespans for one day. The span entries are not allowed to overlap. | [optional] [default to const []]
**wednesday** | [**List<TimeSpan>**](TimeSpan.md) | A list of timespans for one day. The span entries are not allowed to overlap. | [optional] [default to const []]
**thursday** | [**List<TimeSpan>**](TimeSpan.md) | A list of timespans for one day. The span entries are not allowed to overlap. | [optional] [default to const []]
**friday** | [**List<TimeSpan>**](TimeSpan.md) | A list of timespans for one day. The span entries are not allowed to overlap. | [optional] [default to const []]
**saturday** | [**List<TimeSpan>**](TimeSpan.md) | A list of timespans for one day. The span entries are not allowed to overlap. | [optional] [default to const []]
**sunday** | [**List<TimeSpan>**](TimeSpan.md) | A list of timespans for one day. The span entries are not allowed to overlap. | [optional] [default to const []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


