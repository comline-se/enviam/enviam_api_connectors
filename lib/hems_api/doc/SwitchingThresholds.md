# hems_api.model.SwitchingThresholds

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**powerLevels** | **List<num>** | Sorted list. The accepted power levels of the device: * For switchable devices which can only be switched on an off, this value holds a single entry serving as   a threshold indicating how much power the device will consume, and at which excess level the device shall   be switched on. * For controllable devices which can be controlled in a range, this value holds two entries serving as the   minimum switch threshold and the maximum power which can be consumed by the device. Any power larger than   the maximum will be given to lower-prioritized devices (if any). * For controllable devices which accept distinct power levels (i.e. 500 - 1000 - 1500 - 2000 Watts), this   list contains all discrete power levels which are acceptable by the device, where the first entry is the   switch threshold, any the other values are the accepted levels. Any power larger than the maximum will be   given to lower-prioritized devices (if any). | [default to const []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


