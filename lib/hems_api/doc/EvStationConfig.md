# hems_api.model.EvStationConfig

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maximumCapacity** | **num** | The maximum capacity of the battery associated with this ev-station. | 
**maximumChargingPower** | **num** | The maximum charging power of the ev-station. | 
**maximumRange** | **num** | The maximum range power of the car associated with this ev-station. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


