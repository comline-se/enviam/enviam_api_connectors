# hems_api.model.SwitchOverride

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**targetState** | [**SwitchState**](SwitchState.md) |  | 
**durationOfOverride** | **num** | Duration of the override in minutes. After that timeframe, the device is pv optimized again. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


