# hems_api.model.SunAltitudes

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sunrise** | **String** | Time of the day in a hh:mm format. Does not include any timezone information. The time should be truncated by the seconds, e.g. 00:59:59 is included in 00:59.  | 
**sunhigh** | **String** | Time of the day in a hh:mm format. Does not include any timezone information. The time should be truncated by the seconds, e.g. 00:59:59 is included in 00:59.  | 
**sunset** | **String** | Time of the day in a hh:mm format. Does not include any timezone information. The time should be truncated by the seconds, e.g. 00:59:59 is included in 00:59.  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


