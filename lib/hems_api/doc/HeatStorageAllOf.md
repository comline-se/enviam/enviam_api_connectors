# hems_api.model.HeatStorageAllOf

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firmwareVersion** | **String** | The devices firmware version. | [optional] 
**name** | **String** | The devices displayed name (changeable by the user). | [optional] 
**manufacturer** | **String** | The device manufacturer id. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


