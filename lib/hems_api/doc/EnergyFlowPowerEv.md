# hems_api.model.EnergyFlowPowerEv

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | A unique ID for the device. | 
**soc** | **double** | Current state of charge. Will be empty if no SoC is available or multiple ev stations are available. | [optional] 
**in_** | **int** | Power consumed for charging by EV stations. | 
**out_** | **int** | Power discharged from EV stations. | 
**balance** | **int** | Balance in - out. | 
**bidirectional** | **bool** | Flag if this is a bidirectional buffer. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


