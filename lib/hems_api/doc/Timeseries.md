# hems_api.model.Timeseries

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The name of the timeseries | 
**aggregated** | **int** | The aggregated values of the timeseries | 
**guid** | **String** | The id of the related device | [optional] 
**id** | **String** | The id of the timeseries (<deviceGuid>~<tagName>) | 
**unit** | [**TimeseriesUnit**](TimeseriesUnit.md) |  | 
**values** | **Map<String, int>** | List of timeseries values | [default to const {}]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


