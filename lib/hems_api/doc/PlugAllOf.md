# hems_api.model.PlugAllOf

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idFirmware** | **String** | The devices firmware ID. | [optional] 
**idManufacturer** | **String** | The device manufacturer id. | [optional] 
**idModelCode** | **String** | The devices ID model code. | [optional] 
**idName** | **String** | The devices displayed name (changeable by the user). | [optional] 
**modeRelay** | **String** | Current relay mode for the device. | [optional] 
**stateSwitchable** | **bool** | The capability of the device (typically a plug) to be switchable. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


