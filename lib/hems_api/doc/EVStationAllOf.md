# hems_api.model.EVStationAllOf

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**availableModes** | [**List<DeviceModeEVStation>**](DeviceModeEVStation.md) | The modes that are available for the EV station device. | [optional] [default to const []]
**car** | [**Car**](Car.md) |  | [optional] 
**connectivityStatus** | **String** | Current connectivity status for the device. | [optional] 
**gridLimit** | **num** | Power limit (in watt) for use in grid optimized use case. | [optional] 
**firmware** | **String** | The devices firmware ID. | [optional] 
**manufacturer** | **String** | The device manufacturer id. | [optional] 
**modelCode** | **String** | The devices ID model code. | [optional] 
**serialNumber** | **String** | The devices serial number ID. | [optional] 
**lowestPower** | **num** | Lower threshold for excess charging in watt. When pv excess reaches this value, the wallbox is set to charging. | [optional] 
**maxPower** | **num** | Maximum charging power in watt. | [optional] 
**optimizationMode** | [**DeviceOptimizationMode**](DeviceOptimizationMode.md) |  | [optional] 
**powerAcInLimit** | **num** | Currently set power AC input limit set for the device. | [optional] 
**powerAcInMax** | **num** | Maximum power AC input currently set for the device. | [optional] 
**powerAcOutLimit** | **num** | Currently set power AC output value set for the device. | [optional] 
**powerAcOutMax** | **num** | Maximum power AC output currently set for the device. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


