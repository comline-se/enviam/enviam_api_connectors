# hems_api.model.RippleControlReceiver

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | A unique ID for the device. | 
**name** | **String** | The devices displayed name (changeable by the user). | 
**state** | [**DeviceState**](DeviceState.md) |  | 
**type** | [**DeviceType**](DeviceType.md) |  | 
**class_** | **String** | Device class defines the properties and handling of a device. | [optional] 
**stateDevice** | **String** | The device state indication proper behavior or potential problems. | 
**stateErrorList** | **List<String>** | A list of device errors if there is one. | [default to const []]
**fractionLimit** | **num** | The devices fraction limit. | [optional] 
**idManufacturer** | **String** | The device manufacturer id. | [optional] 
**idName** | **String** | The devices displayed name (changeable by the user). | [optional] 
**idPowerMeter** | **String** | The devices displayed power meter. | [optional] 
**idSerialNumber** | **String** | The devices serial number ID. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


