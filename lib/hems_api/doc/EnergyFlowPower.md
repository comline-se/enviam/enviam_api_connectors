# hems_api.model.EnergyFlowPower

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**consumption** | [**EnergyFlowPowerConsumption**](EnergyFlowPowerConsumption.md) |  | 
**grid** | [**EnergyFlowPowerGrid**](EnergyFlowPowerGrid.md) |  | 
**pv** | [**EnergyFlowPowerPv**](EnergyFlowPowerPv.md) |  | [optional] 
**battery** | [**EnergyFlowPowerBattery**](EnergyFlowPowerBattery.md) |  | [optional] 
**ev** | [**List<EnergyFlowPowerEv>**](EnergyFlowPowerEv.md) | EV stations information. Only present if a ev station exists. | [optional] [default to const []]
**hs** | [**EnergyFlowPowerHs**](EnergyFlowPowerHs.md) |  | [optional] 
**heatPump** | [**EnergyFlowPowerHeatPump**](EnergyFlowPowerHeatPump.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


