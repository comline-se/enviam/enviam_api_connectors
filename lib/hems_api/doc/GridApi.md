# hems_api.api.GridApi

## Load the API package
```dart
import 'package:hems_api/api.dart';
```

All URIs are relative to *https://hems.api.staging.kiwigrid.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getGridConsumption**](GridApi.md#getgridconsumption) | **GET** /grid/consumption | Grid consumption.


# **getGridConsumption**
> GridConsumptionResponse getGridConsumption(from, to, resolution, fill)

Grid consumption.

### Example 
```dart
import 'package:hems_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = GridApi();
final from = 2020-06-25T00:00:00+02:00; // DateTime | Timestamp for start of a range in [rfc3339](https://tools.ietf.org/html/rfc3339#section-5.6) format.
final to = 2020-06-25T23:59:59+02:00; // DateTime | Timestamp for end of a range in [rfc3339](https://tools.ietf.org/html/rfc3339#section-5.6) format.
final resolution = PT15M; // String | Resolution for values using [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601#Durations) durations format.
final fill = ; // Fill | A filler used to handle missing values in time series.

try { 
    final result = api_instance.getGridConsumption(from, to, resolution, fill);
    print(result);
} catch (e) {
    print('Exception when calling GridApi->getGridConsumption: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **from** | **DateTime**| Timestamp for start of a range in [rfc3339](https://tools.ietf.org/html/rfc3339#section-5.6) format. | 
 **to** | **DateTime**| Timestamp for end of a range in [rfc3339](https://tools.ietf.org/html/rfc3339#section-5.6) format. | 
 **resolution** | **String**| Resolution for values using [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601#Durations) durations format. | 
 **fill** | [**Fill**](.md)| A filler used to handle missing values in time series. | [optional] 

### Return type

[**GridConsumptionResponse**](GridConsumptionResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

