# hems_api.model.HeatStorage

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | A unique ID for the device. | 
**name** | **String** | The devices displayed name (changeable by the user). | 
**state** | [**DeviceState**](DeviceState.md) |  | 
**type** | [**DeviceType**](DeviceType.md) |  | 
**class_** | **String** | Device class defines the properties and handling of a device. | [optional] 
**stateDevice** | **String** | The device state indication proper behavior or potential problems. | 
**stateErrorList** | **List<String>** | A list of device errors if there is one. | [default to const []]
**firmwareVersion** | **String** | The devices firmware version. | [optional] 
**manufacturer** | **String** | The device manufacturer id. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


