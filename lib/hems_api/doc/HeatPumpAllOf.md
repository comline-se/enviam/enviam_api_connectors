# hems_api.model.HeatPumpAllOf

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idManufacturer** | **String** | The device manufacturer id. | [optional] 
**idName** | **String** | The devices displayed name (changeable by the user). | [optional] 
**idPowerMeter** | **String** | The devices displayed power meter. | [optional] 
**modeOperation** | **String** | The devices mode of operation. | [optional] 
**nominalPower** | **num** | The devices nominal power. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


