# hems_api.model.Device

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | A unique ID for the device. | 
**name** | **String** | The devices displayed name (changeable by the user). | 
**mode** | [**DeviceModeFull**](DeviceModeFull.md) |  | [optional] 
**stateDevice** | [**DeviceState**](DeviceState.md) |  | 
**stateErrorList** | [**DeviceStateErrorList**](DeviceStateErrorList.md) |  | 
**type** | [**DeviceType**](DeviceType.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


