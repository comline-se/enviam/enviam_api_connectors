# hems_api.model.DeviceSwitcher

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | A unique ID for the device. | 
**visibleName** | **String** | A human readable name for the device. | 
**description** | **String** | A human readable description for the device. E.g. the device that is served by a plug | 
**requiresOverride** | **bool** | If set, the device can only be switched with an additional override configuration. | [optional] [default to false]
**switchState** | [**VisibleSwitchState**](VisibleSwitchState.md) |  | 
**thresholdType** | [**ThresholdType**](ThresholdType.md) |  | 
**supportedModes** | [**List<ScheduledDeviceMode>**](ScheduledDeviceMode.md) | Schedule modes, that can be configured for the device. | [default to const []]
**supportedConfigs** | [**List<DeviceConfigParameters>**](DeviceConfigParameters.md) | Parameters of the device that can be configured. | [default to const []]
**category** | [**DeviceCategory**](DeviceCategory.md) |  | [optional] 
**overrideState** | [**OverrideState**](OverrideState.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


