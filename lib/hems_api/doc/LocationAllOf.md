# hems_api.model.LocationAllOf

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**addressLocation** | **String** | The devices physical address (typically the device is a location). | [optional] 
**idName** | **String** | The devices displayed name (changeable by the user). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


