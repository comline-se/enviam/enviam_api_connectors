# hems_api.model.EnergyFlowWorkGrid

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**in_** | **int** | Work in. | 
**out_** | **int** | Work out. | 
**balance** | **int** | Balance in - out. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


