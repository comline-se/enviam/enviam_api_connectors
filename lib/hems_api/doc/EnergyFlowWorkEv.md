# hems_api.model.EnergyFlowWorkEv

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | A unique ID for the device. | 
**in_** | **int** | Work consumed for charging by ev stations. | 
**out_** | **int** | Work discharged from ev stations. | 
**balance** | **int** | Balance in - out. | 
**bidirectional** | **bool** | Is bidirectional (buffer)? | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


