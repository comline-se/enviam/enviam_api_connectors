# hems_api.model.FinanceResponse

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cost** | **double** | Costs of pv plant. Only present if a pv plant exists. | 
**profit** | **double** | Profit of pv plant. Only present if a pv plant exists. | 
**balance** | **double** | Total financial balance. | 
**currencyCode** | **String** | Currency code based on [ISO_4217](https://en.wikipedia.org/wiki/ISO_4217). | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


