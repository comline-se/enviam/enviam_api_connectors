# hems_api.api.EvStationApi

## Load the API package
```dart
import 'package:hems_api/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getEVStation**](EvStationApi.md#getevstation) | **GET** /evstation/{deviceId} | Get a single EV station.
[**getEVStations**](EvStationApi.md#getevstations) | **GET** /evstation | Returns all available EV stations.
[**patchEVStation**](EvStationApi.md#patchevstation) | **PATCH** /evstation/{deviceId} | Updates the values of an EV station.


# **getEVStation**
> EVStation getEVStation(deviceId)

Get a single EV station.

### Example 
```dart
import 'package:hems_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = EvStationApi();
final deviceId = deviceId_example; // String | Unique device ID for identifying the device inside the HEMS and Kiwigrid context.

try { 
    final result = api_instance.getEVStation(deviceId);
    print(result);
} catch (e) {
    print('Exception when calling EvStationApi->getEVStation: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceId** | **String**| Unique device ID for identifying the device inside the HEMS and Kiwigrid context. | 

### Return type

[**EVStation**](EVStation.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getEVStations**
> List<EVStation> getEVStations()

Returns all available EV stations.

### Example 
```dart
import 'package:hems_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = EvStationApi();

try { 
    final result = api_instance.getEVStations();
    print(result);
} catch (e) {
    print('Exception when calling EvStationApi->getEVStations: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List<EVStation>**](EVStation.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patchEVStation**
> patchEVStation(deviceId, eVStationValues)

Updates the values of an EV station.

### Example 
```dart
import 'package:hems_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = EvStationApi();
final deviceId = deviceId_example; // String | Unique device ID for identifying the device inside the HEMS and Kiwigrid context.
final eVStationValues = EVStationValues(); // EVStationValues | 

try { 
    api_instance.patchEVStation(deviceId, eVStationValues);
} catch (e) {
    print('Exception when calling EvStationApi->patchEVStation: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceId** | **String**| Unique device ID for identifying the device inside the HEMS and Kiwigrid context. | 
 **eVStationValues** | [**EVStationValues**](EVStationValues.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

