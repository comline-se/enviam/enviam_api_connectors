# hems_api.model.Car

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The name of the car read by the wallbox. | [optional] 
**capacity** | **num** | The capacity if the cars battery. | [optional] 
**departureTime** | **num** | The set up departure time for the car. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


