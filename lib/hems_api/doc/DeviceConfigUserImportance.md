# hems_api.model.DeviceConfigUserImportance

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deviceId** | **String** | A unique ID for the device. | 
**userImportance** | **int** | Priority of the device during pv optimization, devices with higher numbers have precedence compares to other pv optimized devices with lower numbers. | [default to 0]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


