# hems_api.model.DeviceConfig

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**optimizationEnabled** | **bool** | Flag to indicate if the pv optimization is enabled at all. | 
**switchingThresholds** | [**SwitchingThresholds**](SwitchingThresholds.md) |  | 
**minRuntime** | **int** | Minimal duration in minutes a device needs to run after it was turned on. | [optional] [default to 0]
**mustRuntime** | **int** | Minimal duration in minutes a device needs to run over the time of one day. | [optional] [default to 0]
**minResttime** | **int** | Minimal duration in minutes a device needs to be off after it was turned off. | [optional] [default to 0]
**evStationConfig** | [**EvStationConfig**](EvStationConfig.md) |  | [optional] 
**userImportance** | **int** | Priority of the device during pv optimization, devices with higher numbers have precedence compares to other pv optimized devices with lower numbers. | [optional] [default to 0]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


