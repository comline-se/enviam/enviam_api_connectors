# hems_api.model.UserProfile

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | [**UserLanguage**](UserLanguage.md) |  | [optional] 
**currency** | [**UserCurrency**](UserCurrency.md) |  | [optional] 
**temperature** | [**UserTemperature**](UserTemperature.md) |  | [optional] 
**distances** | [**UserDistances**](UserDistances.md) |  | [optional] 
**feedInTariff** | **num** |  | [optional] 
**energyCostsPerKwh** | **num** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


