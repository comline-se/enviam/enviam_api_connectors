# hems_api.model.RippleControlReceiverAllOf

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fractionLimit** | **num** | The devices fraction limit. | [optional] 
**idManufacturer** | **String** | The device manufacturer id. | [optional] 
**idName** | **String** | The devices displayed name (changeable by the user). | [optional] 
**idPowerMeter** | **String** | The devices displayed power meter. | [optional] 
**idSerialNumber** | **String** | The devices serial number ID. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


