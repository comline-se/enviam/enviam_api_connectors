# hems_api.model.User

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstName** | **String** |  | [optional] 
**lastName** | **String** |  | [optional] 
**address** | [**UserAddress**](UserAddress.md) |  | [optional] 
**zipCode** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**country** | [**UserCountry**](UserCountry.md) |  | [optional] 
**email** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


