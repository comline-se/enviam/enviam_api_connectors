# hems_api.model.SmartHeaterAllOf

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idFirmware** | **String** | The devices firmware ID. | [optional] 
**idManufacturer** | **String** | The device manufacturer id. | [optional] 
**idModelMode** | **String** | The devices ID model code. | [optional] 
**idName** | **String** | The devices displayed name (changeable by the user). | [optional] 
**idSerialNumber** | **String** | The devices serial number ID. | [optional] 
**powerAcInLimit** | **num** | Currently set power AC input limit set for the device. | [optional] 
**powerAcInLimits** | **num** | Maximum power AC input limits currently set for the device. | [optional] 
**temperature** | **num** | Current temperature the device (typically a smart heater) is measuring. | [optional] 
**temperatureSet** | **num** | Temperature the device is targeting (typically a smart heater). | [optional] 
**temperatureSetMax** | **num** | Maximum temperature set for the device (typically a smart heater). | [optional] 
**temperatureSetMin** | **num** | Minimum temperature set for the device (typically a smart heater). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


