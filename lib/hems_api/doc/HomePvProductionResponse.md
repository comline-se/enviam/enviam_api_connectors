# hems_api.model.HomePvProductionResponse

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**graph** | [**Graph**](Graph.md) |  | 
**composition** | [**HomePvProductionResponseComposition**](HomePvProductionResponseComposition.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


