# hems_api.model.EnergyFlowWork

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**consumption** | [**EnergyFlowWorkConsumption**](EnergyFlowWorkConsumption.md) |  | 
**directConsumption** | [**EnergyFlowWorkDirectConsumption**](EnergyFlowWorkDirectConsumption.md) |  | [optional] 
**grid** | [**EnergyFlowWorkGrid**](EnergyFlowWorkGrid.md) |  | 
**pv** | [**EnergyFlowWorkPv**](EnergyFlowWorkPv.md) |  | [optional] 
**battery** | [**EnergyFlowWorkBattery**](EnergyFlowWorkBattery.md) |  | [optional] 
**ev** | [**List<EnergyFlowWorkEv>**](EnergyFlowWorkEv.md) | EV stations information. Only present if an ev station exists. | [optional] [default to const []]
**hs** | [**EnergyFlowWorkHs**](EnergyFlowWorkHs.md) |  | [optional] 
**heatPump** | [**EnergyFlowWorkHeatPump**](EnergyFlowWorkHeatPump.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


