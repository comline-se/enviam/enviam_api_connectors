# hems_api.model.Plug

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | A unique ID for the device. | 
**name** | **String** | The devices displayed name (changeable by the user). | 
**state** | [**DeviceState**](DeviceState.md) |  | 
**type** | [**DeviceType**](DeviceType.md) |  | 
**class_** | **String** | Device class defines the properties and handling of a device. | [optional] 
**stateDevice** | **String** | The device state indication proper behavior or potential problems. | 
**stateErrorList** | **List<String>** | A list of device errors if there is one. | [default to const []]
**idFirmware** | **String** | The devices firmware ID. | [optional] 
**idManufacturer** | **String** | The device manufacturer id. | [optional] 
**idModelCode** | **String** | The devices ID model code. | [optional] 
**idName** | **String** | The devices displayed name (changeable by the user). | [optional] 
**modeRelay** | **String** | Current relay mode for the device. | [optional] 
**stateSwitchable** | **bool** | The capability of the device (typically a plug) to be switchable. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


