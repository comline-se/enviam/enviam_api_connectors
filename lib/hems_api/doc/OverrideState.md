# hems_api.model.OverrideState

## Load the model package
```dart
import 'package:hems_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**targetState** | [**SwitchState**](SwitchState.md) |  | 
**endOfOverride** | [**DateTime**](DateTime.md) | The end of the override, delivered as UTC. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


