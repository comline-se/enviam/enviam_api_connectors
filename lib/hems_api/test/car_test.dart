//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//


// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:hems_api/api.dart';
import 'package:test/test.dart';

// tests for Car
void main() {
  final instance = Car();

  group('test Car', () {
    // The name of the car read by the wallbox.
    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // The capacity if the cars battery.
    // num capacity
    test('to test the property `capacity`', () async {
      // TODO
    });

    // The set up departure time for the car.
    // String departureTime
    test('to test the property `departureTime`', () async {
      // TODO
    });


  });

}
