//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//


// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'dart:developer';

import 'package:hems_api/api.dart';
import 'package:test/test.dart';


/// tests for EvStationApi
void main() {
  final instance = EvStationApi(ApiClient(basePath: "https://api.staging.kiwios.io/hems"));

  group('tests for EvStationApi', () {
    // Get a single EV station.
    //
    //Future<EVStation> getEVStation(String deviceId) async
    test('test getEVStation', () async {

      var apiClient = instance.apiClient;
      var authentication = apiClient.getAuthentication('BearerAuth') as HttpBearerAuth?;
      if (authentication != null) {
        authentication.accessToken = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJBWWpsQlgtc0p4MzQzZ25kTTJYTE1TSGh0QkJEQ3h2NmE3UTFNTWJaZ01FIn0.eyJleHAiOjE2Mzc5NDE3NzQsImlhdCI6MTYzNzkzOTk3MywianRpIjoiNTA4MDVlZWMtZTk5OC00NmU3LWI2NGMtYThmMTBmZWFiN2I1IiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5zdGFnaW5nLmtpd2lncmlkLmNvbS9hdXRoL3JlYWxtcy9lbnZpYSIsInN1YiI6ImY6OWExNWM3OTEtNjU0MS00NmUzLWI2MjctOWMwZTc2ZGViN2I3OmNvbnBsZW1lbnQuZmxleHVtZXIxIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiaW50ZWxsaWdlbnQtdmVybmV0enQtYXBwIiwic2Vzc2lvbl9zdGF0ZSI6IjNiYWMzYWRiLTVhZDgtNDU2ZS1hZTRjLWM2OWI5YTQzNjY4NCIsImFjciI6IjEiLCJzY29wZSI6IiIsInJlYWxtIjoiZW52aWEiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJjb25wbGVtZW50LmZsZXh1bWVyMSJ9.JLc5wiDGiNK7OHLW0ayyLwN3UtPekdFsd3fHECxXWuXhepFr7tXw_2sVdDoNuwK8tu6v6w7RWRjkcbHtc7qv3JCPKyszEuFmJGKmYvamjkU3-gkAjcXNu_mdag2K3gdtrrzKZxWeH0biMahqJDcz54HJfH16c7h4ftiBuFTsm5tufQ3zktaI3Hu7L-CdM3HyrhHgNSPPq2seVfjen19wzt_Cpy8ApDjFjGYorR2wllQQa58QcvY5Llao11PysR3DggZiz5HiHoD7WFH5JqAPvKb4gWa9YiGrK5vWgIAGN4VU3n0JltW7l_sAU3DCTO3EZXZd4Qee2Jrc6QW0TABe1w";
      }
      apiClient.addDefaultHeader("x-apikey","bkOPCuQZCi56jBlntK2SdpplYsjzdrG4eAaiAptGtjWgfFfG");

      var station = await instance.getEVStation("urn:com.kiwigrid.simulation:EnviaM:EVStation:ERB03-100002418");

      log(station.id);
    });

    // Returns all available EV stations.
    //
    //Future<List<EVStation>> getEVStations() async
    test('test getEVStations iwth unauthorizedUser', () async {
      var apiClient = instance.apiClient;
      var authentication = apiClient.getAuthentication('BearerAuth') as HttpBearerAuth?;
      if (authentication != null) {
        authentication.accessToken = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJBWWpsQlgtc0p4MzQzZ25kTTJYTE1TSGh0QkJEQ3h2NmE3UTFNTWJaZ01FIn0.eyJleHAiOjE2MzgxODUzODIsImlhdCI6MTYzODE4MzU4MiwianRpIjoiM2M3YzU2NTItNWIzMS00Y2I4LWJhZDAtZGQ3YTNlMjg5YzIxIiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5zdGFnaW5nLmtpd2lncmlkLmNvbS9hdXRoL3JlYWxtcy9lbnZpYSIsInN1YiI6ImY6OWExNWM3OTEtNjU0MS00NmUzLWI2MjctOWMwZTc2ZGViN2I3OmNvbnBsZW1lbnQuaGVhdHVzZXIxIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiaW50ZWxsaWdlbnQtdmVybmV0enQtYXBwIiwic2Vzc2lvbl9zdGF0ZSI6IjYyOGJiOGUyLWEwMjYtNDQ4MS1iM2RhLWIzM2I1NDAwY2ZkZSIsImFjciI6IjEiLCJzY29wZSI6IiIsInJlYWxtIjoiZW52aWEiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJjb25wbGVtZW50LmhlYXR1c2VyMSJ9.Xxfd7jkdzPwoEBI5dhhW9xEcuCB2oWhcTQd7ywVYd7j_LJkQHfdMBpOadVuzQhC1TDjcdD0wHMRpP9lOPYEsVDdnU3eEk6kH1NEHnjY60GkYDFhKCLFl27mSNArg993U81GGINWkk-zvGYVF6iJHKtTxBa9DAYdhylehNx9KbB_8bLwaCOpBtyR8B1Gq0o4uAF9FAUX0aS0Rj0tX1OHQt5tznIXuAtJHrUxAljp6K24-s9kR40p30S3ERnrwV6WdqIFq43NQXdD5Y0VNsVJIGyLojadWdmMTfnjbLvpU6ZcpCq1V61Wed1CxqpdAC7NFYezkxf_yJP2nKsxnUvbsTQ";
      }
      apiClient.addDefaultHeader("x-apikey","bkOPCuQZCi56jBlntK2SdpplYsjzdrG4eAaiAptGtjWgfFfG");

      var actual = await instance.getEVStations();

      expect(actual, equals([]));

    });

    // Updates the values of an EV station.
    //
    //Future patchEVStation(String deviceId, { EVStationValues eVStationValues }) async
    test('test patchEVStation', () async {
      final deviceId = "urn:com.kiwigrid.simulation:EnviaM:EVStation:ERB03-100002418";

      var apiClient = instance.apiClient;
      var authentication = apiClient.getAuthentication('BearerAuth') as HttpBearerAuth?;
      if (authentication != null) {
        authentication.accessToken = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJBWWpsQlgtc0p4MzQzZ25kTTJYTE1TSGh0QkJEQ3h2NmE3UTFNTWJaZ01FIn0.eyJleHAiOjE2MzgxODQyOTksImlhdCI6MTYzODE4MjQ5OSwianRpIjoiMmM2M2YwYWQtN2Q4MS00OGFkLWFiNTEtY2I3N2E0MWZmMjBkIiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5zdGFnaW5nLmtpd2lncmlkLmNvbS9hdXRoL3JlYWxtcy9lbnZpYSIsInN1YiI6ImY6OWExNWM3OTEtNjU0MS00NmUzLWI2MjctOWMwZTc2ZGViN2I3OmNvbnBsZW1lbnQuZmxleHVtZXIxIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiaW50ZWxsaWdlbnQtdmVybmV0enQtYXBwIiwic2Vzc2lvbl9zdGF0ZSI6Ijg4MTY2MGI0LTNhNzAtNDVjNi05OGMxLThiOTFmMTU5YzA0ZSIsImFjciI6IjEiLCJzY29wZSI6IiIsInJlYWxtIjoiZW52aWEiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJjb25wbGVtZW50LmZsZXh1bWVyMSJ9.YeUHipSewcq4M37dIaYkHeBO_Hk5TVz4j2KO_8x_XYxQqGnG_tcsvPIVUAqCTZCs4lnKaQh2m1oHgpKszVUAJzUVUAXoZz6b0ObWiadwH6S0QvrfB_i-dwp1yigKiVKG_SPYo0eA-CH0V3vjjEbECiI_hbFyHS3MokGUfYoSAHXmbytQckLP2L5w1bIoY36TNf_cdVxHvjh2zgJpmsTZYZcDCP553ZtShgnyFcEBdnDHi_POQ5N8oItZ3WiYiFJjBBcqs60AL2XGvA16IuqNMokHdH8-bVgO9r3VgL6C5AJt8REBWDdMf0fw0P-MXaHyOMlsXgTVXRqkoZgyKbpVoA";
      }
      apiClient.addDefaultHeader("x-apikey","bkOPCuQZCi56jBlntK2SdpplYsjzdrG4eAaiAptGtjWgfFfG");

      var stationBeforeUpdate = await instance.getEVStation(deviceId);

      log(stationBeforeUpdate.id);

      var changedCar = stationBeforeUpdate.car;

      expect(changedCar, isNotNull);
      var changedName = changedCar!.name! + ".changed";

      changedCar.name = changedName;

      await instance.patchEVStation(deviceId, eVStationValues: EVStationValues(car: changedCar, gridLimit: stationBeforeUpdate.gridLimit!, lowestPower: stationBeforeUpdate.lowestPower!, maxPower: stationBeforeUpdate.maxPower!, optimizationMode: DeviceOptimizationMode.DEPARTURE_TIME, ));

      var stationAfterUpdate = await instance.getEVStation(deviceId);

      expect(stationAfterUpdate.car!.name!, changedName);


    });

    // Updates the charging mode of an EV station.
    //
    //Future patchEVStationMode(String deviceId, { EVStationChargingModeRequest eVStationChargingModeRequest }) async
    test('test patchEVStationMode', () async {
      // TODO
    });

  });
}
