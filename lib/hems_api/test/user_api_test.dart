//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//


// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:hems_api/api.dart';
import 'package:test/test.dart';


/// tests for UserApi
void main() {
  final instance = UserApi();

  group('tests for UserApi', () {
    // Retrieves the current users information
    //
    //Future<User> getUser() async
    test('test getUser', () async {
      // TODO
    });

    // Retrieves the current users profile settings
    //
    //Future<UserProfile> getUserProfile() async
    test('test getUserProfile', () async {
      // TODO
    });

    // Updates the current users information
    //
    //Future patchUser({ User user }) async
    test('test patchUser', () async {
      // TODO
    });

    // Updates the current users information
    //
    //Future patchUserProfile({ UserProfile userProfile }) async
    test('test patchUserProfile', () async {
      // TODO
    });

  });
}
