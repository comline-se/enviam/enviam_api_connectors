//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//


// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:hems_api/api.dart';
import 'package:test/test.dart';

// tests for EVStationValuesAllOf
void main() {
  final instance = EVStationValuesAllOf(car: Car(), gridLimit: 0, lowestPower: 0, maxPower: 11, optimizationMode: DeviceOptimizationMode.DEPARTURE_TIME);

  group('test EVStationValuesAllOf', () {
    // Car car
    test('to test the property `car`', () async {
      // TODO
    });

    // Power limit (in watt) for use in grid optimized use case.
    // num gridLimit
    test('to test the property `gridLimit`', () async {
      // TODO
    });

    // Lower threshold for excess charging in watt. When pv excess reaches this value, the wallbox is set to charging.
    // num lowestPower
    test('to test the property `lowestPower`', () async {
      // TODO
    });

    // Maximum charging power in watt.
    // num maxPower
    test('to test the property `maxPower`', () async {
      // TODO
    });


  });

}
