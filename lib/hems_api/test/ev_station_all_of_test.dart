//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//


// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:hems_api/api.dart';
import 'package:test/test.dart';

// tests for EVStationAllOf
void main() {
  final instance = EVStationAllOf();

  group('test EVStationAllOf', () {
    // The modes that are available for the device.
    // String availableModes
    test('to test the property `availableModes`', () async {
      // TODO
    });

    // Car car
    test('to test the property `car`', () async {
      // TODO
    });

    // Current connectivity status for the device.
    // String connectivityStatus
    test('to test the property `connectivityStatus`', () async {
      // TODO
    });

    // Power limit (in watt) for use in grid optimized use case.
    // num gridLimit
    test('to test the property `gridLimit`', () async {
      // TODO
    });

    // The devices firmware ID.
    // String idFirmware
    test('to test the property `idFirmware`', () async {
      // TODO
    });

    // The device manufacturer id.
    // String idManufacturer
    test('to test the property `idManufacturer`', () async {
      // TODO
    });

    // The devices ID model code.
    // String idModelCode
    test('to test the property `idModelCode`', () async {
      // TODO
    });

    // The devices displayed name (changeable by the user).
    // String idName
    test('to test the property `idName`', () async {
      // TODO
    });

    // The devices serial number ID.
    // String idSerialNumber
    test('to test the property `idSerialNumber`', () async {
      // TODO
    });

    // Lower threshold for excess charging in watt. When pv excess reaches this value, the wallbox is set to charging.
    // num lowestPower
    test('to test the property `lowestPower`', () async {
      // TODO
    });

    // Maximum charging power in watt.
    // num maxPower
    test('to test the property `maxPower`', () async {
      // TODO
    });

    // Current mode for the device.
    // String modeStation
    test('to test the property `modeStation`', () async {
      // TODO
    });

    // Currently set power AC input limit set for the device.
    // num powerAcInLimit
    test('to test the property `powerAcInLimit`', () async {
      // TODO
    });

    // Maximum power AC input currently set for the device.
    // num powerAcInMax
    test('to test the property `powerAcInMax`', () async {
      // TODO
    });

    // Currently set power AC output value set for the device.
    // num powerAcOutLimit
    test('to test the property `powerAcOutLimit`', () async {
      // TODO
    });

    // Maximum power AC output currently set for the device.
    // num powerAcOutMax
    test('to test the property `powerAcOutMax`', () async {
      // TODO
    });


  });

}
