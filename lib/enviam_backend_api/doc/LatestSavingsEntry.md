# enviam_backend_api.model.LatestSavingsEntry

## Load the model package
```dart
import 'package:enviam_backend_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyDate** | **String** |  | [optional] 
**environmentPoints** | **num** |  | [optional] 
**epCurr** | **String** |  | [optional] 
**gridfee** | **String** |  | [optional] 
**gridfeeCurr** | **String** |  | [optional] 
**carbonUnit** | **String** |  | [optional] 
**carbonSavings** | **num** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


