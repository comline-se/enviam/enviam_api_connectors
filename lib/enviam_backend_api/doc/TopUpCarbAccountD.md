# enviam_backend_api.model.TopUpCarbAccountD

## Load the model package
```dart
import 'package:enviam_backend_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**topUpCarbAccount** | [**TopUpCarbAccountDTopUpCarbAccount**](TopUpCarbAccountDTopUpCarbAccount.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


