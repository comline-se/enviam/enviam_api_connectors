# enviam_backend_api.api.EnvironmentalBalanceApi

## Load the API package
```dart
import 'package:enviam_backend_api/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**environmentalPoints**](EnvironmentalBalanceApi.md#environmentalpoints) | **POST** /account/v1/environmentalPoints | 
[**getApiState**](EnvironmentalBalanceApi.md#getapistate) | **GET** /account/v1/environmentalPointsBalance | gets the current environmental points balance


# **environmentalPoints**
> TopUpCarbAccount environmentalPoints(prepaidAmount, encAccountId)



### Example
```dart
import 'package:enviam_backend_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = EnvironmentalBalanceApi();
final prepaidAmount = prepaidAmount_example; // String | amount to spend
final encAccountId = encAccountId_example; // String | encoded account id to authorize request

try {
    final result = api_instance.environmentalPoints(prepaidAmount, encAccountId);
    print(result);
} catch (e) {
    print('Exception when calling EnvironmentalBalanceApi->environmentalPoints: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **prepaidAmount** | **String**| amount to spend | 
 **encAccountId** | **String**| encoded account id to authorize request | 

### Return type

[**TopUpCarbAccount**](TopUpCarbAccount.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getApiState**
> EnvironmentalPointsBalance getApiState(encAccountId)

gets the current environmental points balance

 with this method the 

### Example
```dart
import 'package:enviam_backend_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = EnvironmentalBalanceApi();
final encAccountId = encAccountId_example; // String | encoded account id to authorize request

try {
    final result = api_instance.getApiState(encAccountId);
    print(result);
} catch (e) {
    print('Exception when calling EnvironmentalBalanceApi->getApiState: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **encAccountId** | **String**| encoded account id to authorize request | 

### Return type

[**EnvironmentalPointsBalance**](EnvironmentalPointsBalance.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

