# enviam_backend_api.model.MonthlySavingsEntry

## Load the model package
```dart
import 'package:enviam_backend_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**year** | **String** |  | [optional] 
**month** | **String** |  | [optional] 
**gridfeeSavings** | **String** |  | [optional] 
**gridfeeCurr** | **String** |  | [optional] 
**epSavings** | **String** |  | [optional] 
**epUnit** | **String** |  | [optional] 
**carbonUnit** | **String** |  | [optional] 
**carbonSavings** | **num** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


