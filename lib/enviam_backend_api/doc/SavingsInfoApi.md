# enviam_backend_api.api.SavingsInfoApi

## Load the API package
```dart
import 'package:enviam_backend_api/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getLatestSavings**](SavingsInfoApi.md#getlatestsavings) | **GET** /account/v1/latestSavings | get latest savings
[**getMonthlySavings**](SavingsInfoApi.md#getmonthlysavings) | **GET** /account/v1/monthlySavings | get monthly savings
[**getSavingsInfo**](SavingsInfoApi.md#getsavingsinfo) | **GET** /account/v1/gridfeeBalanceTotal | get all control units for the given user


# **getLatestSavings**
> LatestSavings getLatestSavings(encAccountId)

get latest savings

### Example
```dart
import 'package:enviam_backend_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = SavingsInfoApi();
final encAccountId = encAccountId_example; // String | encoded account id to authorize request

try {
    final result = api_instance.getLatestSavings(encAccountId);
    print(result);
} catch (e) {
    print('Exception when calling SavingsInfoApi->getLatestSavings: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **encAccountId** | **String**| encoded account id to authorize request | 

### Return type

[**LatestSavings**](LatestSavings.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getMonthlySavings**
> MonthlySavings getMonthlySavings(encAccountId)

get monthly savings

### Example
```dart
import 'package:enviam_backend_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = SavingsInfoApi();
final encAccountId = encAccountId_example; // String | encoded account id to authorize request

try {
    final result = api_instance.getMonthlySavings(encAccountId);
    print(result);
} catch (e) {
    print('Exception when calling SavingsInfoApi->getMonthlySavings: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **encAccountId** | **String**| encoded account id to authorize request | 

### Return type

[**MonthlySavings**](MonthlySavings.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getSavingsInfo**
> GridFeeBalanceTotal getSavingsInfo(encAccountId)

get all control units for the given user

### Example
```dart
import 'package:enviam_backend_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = SavingsInfoApi();
final encAccountId = encAccountId_example; // String | encoded account id to authorize request

try {
    final result = api_instance.getSavingsInfo(encAccountId);
    print(result);
} catch (e) {
    print('Exception when calling SavingsInfoApi->getSavingsInfo: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **encAccountId** | **String**| encoded account id to authorize request | 

### Return type

[**GridFeeBalanceTotal**](GridFeeBalanceTotal.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

