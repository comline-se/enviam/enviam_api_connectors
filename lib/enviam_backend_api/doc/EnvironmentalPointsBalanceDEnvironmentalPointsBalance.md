# enviam_backend_api.model.EnvironmentalPointsBalanceDEnvironmentalPointsBalance

## Load the model package
```dart
import 'package:enviam_backend_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**environmentalPoints** | **num** |  | [optional] 
**unitEp** | **String** |  | [optional] 
**cabonSavings** | **num** |  | [optional] 
**unitCarb** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


