# enviam_backend_api.model.TopUpCarbAccount

## Load the model package
```dart
import 'package:enviam_backend_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**d** | [**TopUpCarbAccountD**](TopUpCarbAccountD.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


