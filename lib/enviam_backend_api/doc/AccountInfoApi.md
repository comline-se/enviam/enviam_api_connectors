# enviam_backend_api.api.AccountInfoApi

## Load the API package
```dart
import 'package:enviam_backend_api/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAccountInfo**](AccountInfoApi.md#getaccountinfo) | **GET** /partner/v1/accounts | gets the current account id to add to requests


# **getAccountInfo**
> AccountInfos getAccountInfo()

gets the current account id to add to requests

### Example
```dart
import 'package:enviam_backend_api/api.dart';
// TODO Configure HTTP Bearer authorization: BearerAuth
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('BearerAuth').setAccessToken(yourTokenGeneratorFunction);

final api_instance = AccountInfoApi();

try {
    final result = api_instance.getAccountInfo();
    print(result);
} catch (e) {
    print('Exception when calling AccountInfoApi->getAccountInfo: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AccountInfos**](AccountInfos.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

