//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class EnvironmentalBalanceApi {
  EnvironmentalBalanceApi([ApiClient? apiClient]) : apiClient = apiClient ?? defaultApiClient;

  final ApiClient apiClient;

  /// Performs an HTTP 'POST /account/v1/environmentalPoints' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [String] prepaidAmount (required):
  ///   amount to spend
  ///
  /// * [String] encAccountId (required):
  ///   encoded account id to authorize request
  Future<Response> environmentalPointsWithHttpInfo(String prepaidAmount, String encAccountId,) async {
    // Verify required params are set.
    if (prepaidAmount == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: prepaidAmount');
    }
    if (encAccountId == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: encAccountId');
    }

    // ignore: prefer_const_declarations
    final path = r'/account/v1/topUpCarbAccount';

    // ignore: prefer_final_locals
    Object? postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

      queryParams.addAll(_convertParametersForCollectionFormat('', 'prepaidAmount', prepaidAmount));
      queryParams.addAll(_convertParametersForCollectionFormat('', 'enc_accountId', encAccountId));

    const authNames = <String>['BearerAuth'];
    const contentTypes = ["application/json"];


    return apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes[0],
      authNames,
    );
  }

  /// Parameters:
  ///
  /// * [String] prepaidAmount (required):
  ///   amount to spend
  ///
  /// * [String] encAccountId (required):
  ///   encoded account id to authorize request
  Future<TopUpCarbAccount> environmentalPoints(String prepaidAmount, String encAccountId,) async {
    final response = await environmentalPointsWithHttpInfo(prepaidAmount, encAccountId,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, (await _decodeBodyBytes(response))!);
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync((await _decodeBodyBytes(response))!, 'TopUpCarbAccount',) as TopUpCarbAccount;
    
    }
    return Future<TopUpCarbAccount>.value();
  }

  /// gets the current environmental points balance
  ///
  ///  with this method the 
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] encAccountId (required):
  ///   encoded account id to authorize request
  Future<Response> getApiStateWithHttpInfo(String encAccountId,) async {
    // Verify required params are set.
    if (encAccountId == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: encAccountId');
    }

    // ignore: prefer_const_declarations
    final path = r'/account/v1/environmentalPointsBalance';

    // ignore: prefer_final_locals
    Object? postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

      queryParams.addAll(_convertParametersForCollectionFormat('', 'enc_accountId', encAccountId));

    const authNames = <String>['BearerAuth'];
    const contentTypes = <String>[];


    return apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes[0],
      authNames,
    );
  }

  /// gets the current environmental points balance
  ///
  ///  with this method the 
  ///
  /// Parameters:
  ///
  /// * [String] encAccountId (required):
  ///   encoded account id to authorize request
  Future<EnvironmentalPointsBalance> getApiState(String encAccountId,) async {
    final response = await getApiStateWithHttpInfo(encAccountId,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, (await _decodeBodyBytes(response))!);
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync((await _decodeBodyBytes(response))!, 'EnvironmentalPointsBalance',) as EnvironmentalPointsBalance;
    
    }
    return Future<EnvironmentalPointsBalance>.value();
  }
}
