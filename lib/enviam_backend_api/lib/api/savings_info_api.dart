//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class SavingsInfoApi {
  SavingsInfoApi([ApiClient? apiClient]) : apiClient = apiClient ?? defaultApiClient;

  final ApiClient apiClient;

  /// get latest savings
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] encAccountId (required):
  ///   encoded account id to authorize request
  Future<Response> getLatestSavingsWithHttpInfo(String encAccountId,) async {
    // Verify required params are set.
    if (encAccountId == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: encAccountId');
    }

    // ignore: prefer_const_declarations
    final path = r'/account/v1/latestSavings';

    // ignore: prefer_final_locals
    Object? postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

      queryParams.addAll(_convertParametersForCollectionFormat('', 'enc_accountId', encAccountId));

    const authNames = <String>['BearerAuth'];
    const contentTypes = <String>[];


    return apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes[0],
      authNames,
    );
  }

  /// get latest savings
  ///
  /// Parameters:
  ///
  /// * [String] encAccountId (required):
  ///   encoded account id to authorize request
  Future<LatestSavings> getLatestSavings(String encAccountId,) async {
    final response = await getLatestSavingsWithHttpInfo(encAccountId,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, (await _decodeBodyBytes(response))!);
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync((await _decodeBodyBytes(response))!, 'LatestSavings',) as LatestSavings;
    
    }
    return Future<LatestSavings>.value();
  }

  /// get monthly savings
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] encAccountId (required):
  ///   encoded account id to authorize request
  Future<Response> getMonthlySavingsWithHttpInfo(String encAccountId,) async {
    // Verify required params are set.
    if (encAccountId == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: encAccountId');
    }

    // ignore: prefer_const_declarations
    final path = r'/account/v1/monthlySavings';

    // ignore: prefer_final_locals
    Object? postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

      queryParams.addAll(_convertParametersForCollectionFormat('', 'enc_accountId', encAccountId));

    const authNames = <String>['BearerAuth'];
    const contentTypes = <String>[];


    return apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes[0],
      authNames,
    );
  }

  /// get monthly savings
  ///
  /// Parameters:
  ///
  /// * [String] encAccountId (required):
  ///   encoded account id to authorize request
  Future<MonthlySavings> getMonthlySavings(String encAccountId,) async {
    final response = await getMonthlySavingsWithHttpInfo(encAccountId,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, (await _decodeBodyBytes(response))!);
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync((await _decodeBodyBytes(response))!, 'MonthlySavings',) as MonthlySavings;
    
    }
    return Future<MonthlySavings>.value();
  }

  /// get all control units for the given user
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] encAccountId (required):
  ///   encoded account id to authorize request
  Future<Response> getSavingsInfoWithHttpInfo(String encAccountId,) async {
    // Verify required params are set.
    if (encAccountId == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: encAccountId');
    }

    // ignore: prefer_const_declarations
    final path = r'/account/v1/gridfeeBalanceTotal';

    // ignore: prefer_final_locals
    Object? postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

      queryParams.addAll(_convertParametersForCollectionFormat('', 'enc_accountId', encAccountId));

    const authNames = <String>['BearerAuth'];
    const contentTypes = <String>[];


    return apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes[0],
      authNames,
    );
  }

  /// get all control units for the given user
  ///
  /// Parameters:
  ///
  /// * [String] encAccountId (required):
  ///   encoded account id to authorize request
  Future<GridFeeBalanceTotal> getSavingsInfo(String encAccountId,) async {
    final response = await getSavingsInfoWithHttpInfo(encAccountId,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, (await _decodeBodyBytes(response))!);
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync((await _decodeBodyBytes(response))!, 'GridFeeBalanceTotal',) as GridFeeBalanceTotal;
    
    }
    return Future<GridFeeBalanceTotal>.value();
  }
}
