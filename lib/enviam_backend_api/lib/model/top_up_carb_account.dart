//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class TopUpCarbAccount {
  /// Returns a new [TopUpCarbAccount] instance.
  TopUpCarbAccount({
    this.d,
  });

  TopUpCarbAccountD? d;

  @override
  bool operator ==(Object other) => identical(this, other) || other is TopUpCarbAccount &&
     other.d == d;

  @override
  int get hashCode =>
  // ignore: unnecessary_parenthesis
    (d == null ? 0 : d.hashCode);

  @override
  String toString() => 'TopUpCarbAccount[d=$d]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (d != null) {
      json[r'd'] = d;
    }
    return json;
  }

  /// Returns a new [TopUpCarbAccount] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static TopUpCarbAccount? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();
      return TopUpCarbAccount(
        d: TopUpCarbAccountD.fromJson(json[r'd']),
      );
    }
    return null;
  }

  static List<TopUpCarbAccount?>? listFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) =>
    json is List && json.isNotEmpty
      ? json.map(TopUpCarbAccount.fromJson).toList(growable: true == growable)
      : true == emptyIsNull ? null : <TopUpCarbAccount>[];

  static Map<String, TopUpCarbAccount?> mapFromJson(dynamic json) {
    final map = <String, TopUpCarbAccount?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) => map[key] = TopUpCarbAccount.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of TopUpCarbAccount-objects as value to a dart map
  static Map<String, List<TopUpCarbAccount?>?> mapListFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<TopUpCarbAccount?>?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) {
          map[key] = TopUpCarbAccount.listFromJson(
            value,
            emptyIsNull: emptyIsNull,
            growable: growable,
          );
        });
    }
    return map;
  }
}

