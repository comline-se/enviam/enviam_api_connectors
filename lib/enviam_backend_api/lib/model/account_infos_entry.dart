//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class AccountInfosEntry {
  /// Returns a new [AccountInfosEntry] instance.
  AccountInfosEntry({
    this.encAccountId,
    this.accountId,
  });

  String? encAccountId;

  String? accountId;

  @override
  bool operator ==(Object other) => identical(this, other) || other is AccountInfosEntry &&
     other.encAccountId == encAccountId &&
     other.accountId == accountId;

  @override
  int get hashCode =>
  // ignore: unnecessary_parenthesis
    (encAccountId == null ? 0 : encAccountId.hashCode) +
    (accountId == null ? 0 : accountId.hashCode);

  @override
  String toString() => 'AccountInfosEntry[encAccountId=$encAccountId, accountId=$accountId]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (encAccountId != null) {
      json[r'enc_accountId'] = encAccountId;
    }
    if (accountId != null) {
      json[r'accountId'] = accountId;
    }
    return json;
  }

  /// Returns a new [AccountInfosEntry] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static AccountInfosEntry? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();
      return AccountInfosEntry(
        encAccountId: mapValueOfType<String>(json, r'enc_accountId'),
        accountId: mapValueOfType<String>(json, r'accountId'),
      );
    }
    return null;
  }

  static List<AccountInfosEntry?>? listFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) =>
    json is List && json.isNotEmpty
      ? json.map(AccountInfosEntry.fromJson).toList(growable: true == growable)
      : true == emptyIsNull ? null : <AccountInfosEntry>[];

  static Map<String, AccountInfosEntry?> mapFromJson(dynamic json) {
    final map = <String, AccountInfosEntry?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) => map[key] = AccountInfosEntry.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of AccountInfosEntry-objects as value to a dart map
  static Map<String, List<AccountInfosEntry?>?> mapListFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<AccountInfosEntry?>?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) {
          map[key] = AccountInfosEntry.listFromJson(
            value,
            emptyIsNull: emptyIsNull,
            growable: growable,
          );
        });
    }
    return map;
  }
}

