//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class EnvironmentalPointsBalanceDEnvironmentalPointsBalance {
  /// Returns a new [EnvironmentalPointsBalanceDEnvironmentalPointsBalance] instance.
  EnvironmentalPointsBalanceDEnvironmentalPointsBalance({
    this.environmentalPoints,
    this.unitEp,
    this.cabonSavings,
    this.unitCarb,
  });

  num? environmentalPoints;

  String? unitEp;

  num? cabonSavings;

  String? unitCarb;

  @override
  bool operator ==(Object other) => identical(this, other) || other is EnvironmentalPointsBalanceDEnvironmentalPointsBalance &&
     other.environmentalPoints == environmentalPoints &&
     other.unitEp == unitEp &&
     other.cabonSavings == cabonSavings &&
     other.unitCarb == unitCarb;

  @override
  int get hashCode =>
  // ignore: unnecessary_parenthesis
    (environmentalPoints == null ? 0 : environmentalPoints.hashCode) +
    (unitEp == null ? 0 : unitEp.hashCode) +
    (cabonSavings == null ? 0 : cabonSavings.hashCode) +
    (unitCarb == null ? 0 : unitCarb.hashCode);

  @override
  String toString() => 'EnvironmentalPointsBalanceDEnvironmentalPointsBalance[environmentalPoints=$environmentalPoints, unitEp=$unitEp, cabonSavings=$cabonSavings, unitCarb=$unitCarb]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (environmentalPoints != null) {
      json[r'environmentalPoints'] = environmentalPoints;
    }
    if (unitEp != null) {
      json[r'unitEp'] = unitEp;
    }
    if (cabonSavings != null) {
      json[r'cabonSavings'] = cabonSavings;
    }
    if (unitCarb != null) {
      json[r'unitCarb'] = unitCarb;
    }
    return json;
  }

  /// Returns a new [EnvironmentalPointsBalanceDEnvironmentalPointsBalance] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static EnvironmentalPointsBalanceDEnvironmentalPointsBalance? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();
      return EnvironmentalPointsBalanceDEnvironmentalPointsBalance(
        environmentalPoints: json[r'environmentalPoints'] == null
          ? null
          : num.parse(json[r'environmentalPoints'].toString()),
        unitEp: mapValueOfType<String>(json, r'unitEp'),
        cabonSavings: json[r'cabonSavings'] == null
          ? null
          : num.parse(json[r'cabonSavings'].toString()),
        unitCarb: mapValueOfType<String>(json, r'unitCarb'),
      );
    }
    return null;
  }

  static List<EnvironmentalPointsBalanceDEnvironmentalPointsBalance?>? listFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) =>
    json is List && json.isNotEmpty
      ? json.map(EnvironmentalPointsBalanceDEnvironmentalPointsBalance.fromJson).toList(growable: true == growable)
      : true == emptyIsNull ? null : <EnvironmentalPointsBalanceDEnvironmentalPointsBalance>[];

  static Map<String, EnvironmentalPointsBalanceDEnvironmentalPointsBalance?> mapFromJson(dynamic json) {
    final map = <String, EnvironmentalPointsBalanceDEnvironmentalPointsBalance?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) => map[key] = EnvironmentalPointsBalanceDEnvironmentalPointsBalance.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of EnvironmentalPointsBalanceDEnvironmentalPointsBalance-objects as value to a dart map
  static Map<String, List<EnvironmentalPointsBalanceDEnvironmentalPointsBalance?>?> mapListFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<EnvironmentalPointsBalanceDEnvironmentalPointsBalance?>?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) {
          map[key] = EnvironmentalPointsBalanceDEnvironmentalPointsBalance.listFromJson(
            value,
            emptyIsNull: emptyIsNull,
            growable: growable,
          );
        });
    }
    return map;
  }
}

