//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class EnvironmentalPointsBalanceD {
  /// Returns a new [EnvironmentalPointsBalanceD] instance.
  EnvironmentalPointsBalanceD({
    this.environmentalPointsBalance,
  });

  EnvironmentalPointsBalanceDEnvironmentalPointsBalance? environmentalPointsBalance;

  @override
  bool operator ==(Object other) => identical(this, other) || other is EnvironmentalPointsBalanceD &&
     other.environmentalPointsBalance == environmentalPointsBalance;

  @override
  int get hashCode =>
  // ignore: unnecessary_parenthesis
    (environmentalPointsBalance == null ? 0 : environmentalPointsBalance.hashCode);

  @override
  String toString() => 'EnvironmentalPointsBalanceD[environmentalPointsBalance=$environmentalPointsBalance]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (environmentalPointsBalance != null) {
      json[r'environmentalPointsBalance'] = environmentalPointsBalance;
    }
    return json;
  }

  /// Returns a new [EnvironmentalPointsBalanceD] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static EnvironmentalPointsBalanceD? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();
      return EnvironmentalPointsBalanceD(
        environmentalPointsBalance: EnvironmentalPointsBalanceDEnvironmentalPointsBalance.fromJson(json[r'environmentalPointsBalance']),
      );
    }
    return null;
  }

  static List<EnvironmentalPointsBalanceD?>? listFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) =>
    json is List && json.isNotEmpty
      ? json.map(EnvironmentalPointsBalanceD.fromJson).toList(growable: true == growable)
      : true == emptyIsNull ? null : <EnvironmentalPointsBalanceD>[];

  static Map<String, EnvironmentalPointsBalanceD?> mapFromJson(dynamic json) {
    final map = <String, EnvironmentalPointsBalanceD?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) => map[key] = EnvironmentalPointsBalanceD.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of EnvironmentalPointsBalanceD-objects as value to a dart map
  static Map<String, List<EnvironmentalPointsBalanceD?>?> mapListFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<EnvironmentalPointsBalanceD?>?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) {
          map[key] = EnvironmentalPointsBalanceD.listFromJson(
            value,
            emptyIsNull: emptyIsNull,
            growable: growable,
          );
        });
    }
    return map;
  }
}

