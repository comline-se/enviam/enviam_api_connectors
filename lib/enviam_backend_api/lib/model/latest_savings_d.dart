//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class LatestSavingsD {
  /// Returns a new [LatestSavingsD] instance.
  LatestSavingsD({
    this.results = const [],
  });

  List<LatestSavingsEntry?>? results;

  @override
  bool operator ==(Object other) => identical(this, other) || other is LatestSavingsD &&
     other.results == results;

  @override
  int get hashCode =>
  // ignore: unnecessary_parenthesis
    (results == null ? 0 : results.hashCode);

  @override
  String toString() => 'LatestSavingsD[results=$results]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (results != null) {
      json[r'results'] = results;
    }
    return json;
  }

  /// Returns a new [LatestSavingsD] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static LatestSavingsD? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();
      return LatestSavingsD(
        results: LatestSavingsEntry.listFromJson(json[r'results']),
      );
    }
    return null;
  }

  static List<LatestSavingsD?>? listFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) =>
    json is List && json.isNotEmpty
      ? json.map(LatestSavingsD.fromJson).toList(growable: true == growable)
      : true == emptyIsNull ? null : <LatestSavingsD>[];

  static Map<String, LatestSavingsD?> mapFromJson(dynamic json) {
    final map = <String, LatestSavingsD?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) => map[key] = LatestSavingsD.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of LatestSavingsD-objects as value to a dart map
  static Map<String, List<LatestSavingsD?>?> mapListFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<LatestSavingsD?>?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) {
          map[key] = LatestSavingsD.listFromJson(
            value,
            emptyIsNull: emptyIsNull,
            growable: growable,
          );
        });
    }
    return map;
  }
}

