//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class GridFeeBalanceTotal {
  /// Returns a new [GridFeeBalanceTotal] instance.
  GridFeeBalanceTotal({
    this.d,
  });

  GridFeeBalanceTotalD? d;

  @override
  bool operator ==(Object other) => identical(this, other) || other is GridFeeBalanceTotal &&
     other.d == d;

  @override
  int get hashCode =>
  // ignore: unnecessary_parenthesis
    (d == null ? 0 : d.hashCode);

  @override
  String toString() => 'GridFeeBalanceTotal[d=$d]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (d != null) {
      json[r'd'] = d;
    }
    return json;
  }

  /// Returns a new [GridFeeBalanceTotal] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static GridFeeBalanceTotal? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();
      return GridFeeBalanceTotal(
        d: GridFeeBalanceTotalD.fromJson(json[r'd']),
      );
    }
    return null;
  }

  static List<GridFeeBalanceTotal?>? listFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) =>
    json is List && json.isNotEmpty
      ? json.map(GridFeeBalanceTotal.fromJson).toList(growable: true == growable)
      : true == emptyIsNull ? null : <GridFeeBalanceTotal>[];

  static Map<String, GridFeeBalanceTotal?> mapFromJson(dynamic json) {
    final map = <String, GridFeeBalanceTotal?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) => map[key] = GridFeeBalanceTotal.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of GridFeeBalanceTotal-objects as value to a dart map
  static Map<String, List<GridFeeBalanceTotal?>?> mapListFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<GridFeeBalanceTotal?>?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) {
          map[key] = GridFeeBalanceTotal.listFromJson(
            value,
            emptyIsNull: emptyIsNull,
            growable: growable,
          );
        });
    }
    return map;
  }
}

