//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class LatestSavingsEntry {
  /// Returns a new [LatestSavingsEntry] instance.
  LatestSavingsEntry({
    this.keyDate,
    this.environmentPoints,
    this.epCurr,
    this.gridfee,
    this.gridfeeCurr,
    this.carbonUnit,
    this.carbonSavings,
  });

  String? keyDate;

  num? environmentPoints;

  String? epCurr;

  String? gridfee;

  String? gridfeeCurr;

  String? carbonUnit;

  num? carbonSavings;

  @override
  bool operator ==(Object other) => identical(this, other) || other is LatestSavingsEntry &&
     other.keyDate == keyDate &&
     other.environmentPoints == environmentPoints &&
     other.epCurr == epCurr &&
     other.gridfee == gridfee &&
     other.gridfeeCurr == gridfeeCurr &&
     other.carbonUnit == carbonUnit &&
     other.carbonSavings == carbonSavings;

  @override
  int get hashCode =>
  // ignore: unnecessary_parenthesis
    (keyDate == null ? 0 : keyDate.hashCode) +
    (environmentPoints == null ? 0 : environmentPoints.hashCode) +
    (epCurr == null ? 0 : epCurr.hashCode) +
    (gridfee == null ? 0 : gridfee.hashCode) +
    (gridfeeCurr == null ? 0 : gridfeeCurr.hashCode) +
    (carbonUnit == null ? 0 : carbonUnit.hashCode) +
    (carbonSavings == null ? 0 : carbonSavings.hashCode);

  @override
  String toString() => 'LatestSavingsEntry[keyDate=$keyDate, environmentPoints=$environmentPoints, epCurr=$epCurr, gridfee=$gridfee, gridfeeCurr=$gridfeeCurr, carbonUnit=$carbonUnit, carbonSavings=$carbonSavings]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (keyDate != null) {
      json[r'keyDate'] = keyDate;
    }
    if (environmentPoints != null) {
      json[r'EnvironmentPoints'] = environmentPoints;
    }
    if (epCurr != null) {
      json[r'EpCurr'] = epCurr;
    }
    if (gridfee != null) {
      json[r'gridfee'] = gridfee;
    }
    if (gridfeeCurr != null) {
      json[r'gridfeeCurr'] = gridfeeCurr;
    }
    if (carbonUnit != null) {
      json[r'carbonUnit'] = carbonUnit;
    }
    if (carbonSavings != null) {
      json[r'carbonSavings'] = carbonSavings;
    }
    return json;
  }

  /// Returns a new [LatestSavingsEntry] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static LatestSavingsEntry? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();
      return LatestSavingsEntry(
        keyDate: mapValueOfType<String>(json, r'keyDate'),
        environmentPoints: json[r'EnvironmentPoints'] == null
          ? null
          : num.parse(json[r'EnvironmentPoints'].toString()),
        epCurr: mapValueOfType<String>(json, r'EpCurr'),
        gridfee: mapValueOfType<String>(json, r'gridfee'),
        gridfeeCurr: mapValueOfType<String>(json, r'gridfeeCurr'),
        carbonUnit: mapValueOfType<String>(json, r'carbonUnit'),
        carbonSavings: json[r'carbonSavings'] == null
          ? null
          : num.parse(json[r'carbonSavings'].toString()),
      );
    }
    return null;
  }

  static List<LatestSavingsEntry?>? listFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) =>
    json is List && json.isNotEmpty
      ? json.map(LatestSavingsEntry.fromJson).toList(growable: true == growable)
      : true == emptyIsNull ? null : <LatestSavingsEntry>[];

  static Map<String, LatestSavingsEntry?> mapFromJson(dynamic json) {
    final map = <String, LatestSavingsEntry?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) => map[key] = LatestSavingsEntry.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of LatestSavingsEntry-objects as value to a dart map
  static Map<String, List<LatestSavingsEntry?>?> mapListFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<LatestSavingsEntry?>?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) {
          map[key] = LatestSavingsEntry.listFromJson(
            value,
            emptyIsNull: emptyIsNull,
            growable: growable,
          );
        });
    }
    return map;
  }
}

