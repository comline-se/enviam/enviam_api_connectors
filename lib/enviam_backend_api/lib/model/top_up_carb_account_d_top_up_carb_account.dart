//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class TopUpCarbAccountDTopUpCarbAccount {
  /// Returns a new [TopUpCarbAccountDTopUpCarbAccount] instance.
  TopUpCarbAccountDTopUpCarbAccount({
    this.return_,
  });

  num? return_;

  @override
  bool operator ==(Object other) => identical(this, other) || other is TopUpCarbAccountDTopUpCarbAccount &&
     other.return_ == return_;

  @override
  int get hashCode =>
  // ignore: unnecessary_parenthesis
    (return_ == null ? 0 : return_.hashCode);

  @override
  String toString() => 'TopUpCarbAccountDTopUpCarbAccount[return_=$return_]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (return_ != null) {
      json[r'return'] = return_;
    }
    return json;
  }

  /// Returns a new [TopUpCarbAccountDTopUpCarbAccount] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static TopUpCarbAccountDTopUpCarbAccount? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();
      return TopUpCarbAccountDTopUpCarbAccount(
        return_: json[r'return'] == null
          ? null
          : num.parse(json[r'return'].toString()),
      );
    }
    return null;
  }

  static List<TopUpCarbAccountDTopUpCarbAccount?>? listFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) =>
    json is List && json.isNotEmpty
      ? json.map(TopUpCarbAccountDTopUpCarbAccount.fromJson).toList(growable: true == growable)
      : true == emptyIsNull ? null : <TopUpCarbAccountDTopUpCarbAccount>[];

  static Map<String, TopUpCarbAccountDTopUpCarbAccount?> mapFromJson(dynamic json) {
    final map = <String, TopUpCarbAccountDTopUpCarbAccount?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) => map[key] = TopUpCarbAccountDTopUpCarbAccount.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of TopUpCarbAccountDTopUpCarbAccount-objects as value to a dart map
  static Map<String, List<TopUpCarbAccountDTopUpCarbAccount?>?> mapListFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<TopUpCarbAccountDTopUpCarbAccount?>?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) {
          map[key] = TopUpCarbAccountDTopUpCarbAccount.listFromJson(
            value,
            emptyIsNull: emptyIsNull,
            growable: growable,
          );
        });
    }
    return map;
  }
}

