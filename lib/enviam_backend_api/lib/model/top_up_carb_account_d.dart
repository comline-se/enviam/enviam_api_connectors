//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class TopUpCarbAccountD {
  /// Returns a new [TopUpCarbAccountD] instance.
  TopUpCarbAccountD({
    this.topUpCarbAccount,
  });

  TopUpCarbAccountDTopUpCarbAccount? topUpCarbAccount;

  @override
  bool operator ==(Object other) => identical(this, other) || other is TopUpCarbAccountD &&
     other.topUpCarbAccount == topUpCarbAccount;

  @override
  int get hashCode =>
  // ignore: unnecessary_parenthesis
    (topUpCarbAccount == null ? 0 : topUpCarbAccount.hashCode);

  @override
  String toString() => 'TopUpCarbAccountD[topUpCarbAccount=$topUpCarbAccount]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (topUpCarbAccount != null) {
      json[r'topUpCarbAccount'] = topUpCarbAccount;
    }
    return json;
  }

  /// Returns a new [TopUpCarbAccountD] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static TopUpCarbAccountD? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();
      return TopUpCarbAccountD(
        topUpCarbAccount: TopUpCarbAccountDTopUpCarbAccount.fromJson(json[r'topUpCarbAccount']),
      );
    }
    return null;
  }

  static List<TopUpCarbAccountD?>? listFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) =>
    json is List && json.isNotEmpty
      ? json.map(TopUpCarbAccountD.fromJson).toList(growable: true == growable)
      : true == emptyIsNull ? null : <TopUpCarbAccountD>[];

  static Map<String, TopUpCarbAccountD?> mapFromJson(dynamic json) {
    final map = <String, TopUpCarbAccountD?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) => map[key] = TopUpCarbAccountD.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of TopUpCarbAccountD-objects as value to a dart map
  static Map<String, List<TopUpCarbAccountD?>?> mapListFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<TopUpCarbAccountD?>?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) {
          map[key] = TopUpCarbAccountD.listFromJson(
            value,
            emptyIsNull: emptyIsNull,
            growable: growable,
          );
        });
    }
    return map;
  }
}

