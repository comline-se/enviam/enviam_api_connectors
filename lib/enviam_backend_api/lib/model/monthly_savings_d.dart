//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class MonthlySavingsD {
  /// Returns a new [MonthlySavingsD] instance.
  MonthlySavingsD({
    this.results = const [],
  });

  List<MonthlySavingsEntry?>? results;

  @override
  bool operator ==(Object other) => identical(this, other) || other is MonthlySavingsD &&
     other.results == results;

  @override
  int get hashCode =>
  // ignore: unnecessary_parenthesis
    (results == null ? 0 : results.hashCode);

  @override
  String toString() => 'MonthlySavingsD[results=$results]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (results != null) {
      json[r'results'] = results;
    }
    return json;
  }

  /// Returns a new [MonthlySavingsD] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static MonthlySavingsD? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();
      return MonthlySavingsD(
        results: MonthlySavingsEntry.listFromJson(json[r'results']),
      );
    }
    return null;
  }

  static List<MonthlySavingsD?>? listFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) =>
    json is List && json.isNotEmpty
      ? json.map(MonthlySavingsD.fromJson).toList(growable: true == growable)
      : true == emptyIsNull ? null : <MonthlySavingsD>[];

  static Map<String, MonthlySavingsD?> mapFromJson(dynamic json) {
    final map = <String, MonthlySavingsD?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) => map[key] = MonthlySavingsD.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of MonthlySavingsD-objects as value to a dart map
  static Map<String, List<MonthlySavingsD?>?> mapListFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<MonthlySavingsD?>?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) {
          map[key] = MonthlySavingsD.listFromJson(
            value,
            emptyIsNull: emptyIsNull,
            growable: growable,
          );
        });
    }
    return map;
  }
}

