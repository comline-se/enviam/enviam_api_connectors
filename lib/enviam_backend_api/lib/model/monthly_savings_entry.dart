//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class MonthlySavingsEntry {
  /// Returns a new [MonthlySavingsEntry] instance.
  MonthlySavingsEntry({
    this.year,
    this.month,
    this.gridfeeSavings,
    this.gridfeeCurr,
    this.epSavings,
    this.epUnit,
    this.carbonUnit,
    this.carbonSavings,
  });

  String? year;

  String? month;

  String? gridfeeSavings;

  String? gridfeeCurr;

  String? epSavings;

  String? epUnit;

  String? carbonUnit;

  num? carbonSavings;

  @override
  bool operator ==(Object other) => identical(this, other) || other is MonthlySavingsEntry &&
     other.year == year &&
     other.month == month &&
     other.gridfeeSavings == gridfeeSavings &&
     other.gridfeeCurr == gridfeeCurr &&
     other.epSavings == epSavings &&
     other.epUnit == epUnit &&
     other.carbonUnit == carbonUnit &&
     other.carbonSavings == carbonSavings;

  @override
  int get hashCode =>
  // ignore: unnecessary_parenthesis
    (year == null ? 0 : year.hashCode) +
    (month == null ? 0 : month.hashCode) +
    (gridfeeSavings == null ? 0 : gridfeeSavings.hashCode) +
    (gridfeeCurr == null ? 0 : gridfeeCurr.hashCode) +
    (epSavings == null ? 0 : epSavings.hashCode) +
    (epUnit == null ? 0 : epUnit.hashCode) +
    (carbonUnit == null ? 0 : carbonUnit.hashCode) +
    (carbonSavings == null ? 0 : carbonSavings.hashCode);

  @override
  String toString() => 'MonthlySavingsEntry[year=$year, month=$month, gridfeeSavings=$gridfeeSavings, gridfeeCurr=$gridfeeCurr, epSavings=$epSavings, epUnit=$epUnit, carbonUnit=$carbonUnit, carbonSavings=$carbonSavings]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (year != null) {
      json[r'year'] = year;
    }
    if (month != null) {
      json[r'month'] = month;
    }
    if (gridfeeSavings != null) {
      json[r'gridfeeSavings'] = gridfeeSavings;
    }
    if (gridfeeCurr != null) {
      json[r'gridfeeCurr'] = gridfeeCurr;
    }
    if (epSavings != null) {
      json[r'epSavings'] = epSavings;
    }
    if (epUnit != null) {
      json[r'epUnit'] = epUnit;
    }
    if (carbonUnit != null) {
      json[r'carbonUnit'] = carbonUnit;
    }
    if (carbonSavings != null) {
      json[r'carbonSavings'] = carbonSavings;
    }
    return json;
  }

  /// Returns a new [MonthlySavingsEntry] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static MonthlySavingsEntry? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();
      return MonthlySavingsEntry(
        year: mapValueOfType<String>(json, r'year'),
        month: mapValueOfType<String>(json, r'month'),
        gridfeeSavings: mapValueOfType<String>(json, r'gridfeeSavings'),
        gridfeeCurr: mapValueOfType<String>(json, r'gridfeeCurr'),
        epSavings: mapValueOfType<String>(json, r'epSavings'),
        epUnit: mapValueOfType<String>(json, r'epUnit'),
        carbonUnit: mapValueOfType<String>(json, r'carbonUnit'),
        carbonSavings: json[r'carbonSavings'] == null
          ? null
          : num.parse(json[r'carbonSavings'].toString()),
      );
    }
    return null;
  }

  static List<MonthlySavingsEntry?>? listFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) =>
    json is List && json.isNotEmpty
      ? json.map(MonthlySavingsEntry.fromJson).toList(growable: true == growable)
      : true == emptyIsNull ? null : <MonthlySavingsEntry>[];

  static Map<String, MonthlySavingsEntry?> mapFromJson(dynamic json) {
    final map = <String, MonthlySavingsEntry?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) => map[key] = MonthlySavingsEntry.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of MonthlySavingsEntry-objects as value to a dart map
  static Map<String, List<MonthlySavingsEntry?>?> mapListFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<MonthlySavingsEntry?>?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) {
          map[key] = MonthlySavingsEntry.listFromJson(
            value,
            emptyIsNull: emptyIsNull,
            growable: growable,
          );
        });
    }
    return map;
  }
}

