//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class GridFeeBalanceTotalDGridfeeBalanceTotal {
  /// Returns a new [GridFeeBalanceTotalDGridfeeBalanceTotal] instance.
  GridFeeBalanceTotalDGridfeeBalanceTotal({
    this.gridfeeCurr,
    this.gridfeeBalancing,
  });

  String? gridfeeCurr;

  String? gridfeeBalancing;

  @override
  bool operator ==(Object other) => identical(this, other) || other is GridFeeBalanceTotalDGridfeeBalanceTotal &&
     other.gridfeeCurr == gridfeeCurr &&
     other.gridfeeBalancing == gridfeeBalancing;

  @override
  int get hashCode =>
  // ignore: unnecessary_parenthesis
    (gridfeeCurr == null ? 0 : gridfeeCurr.hashCode) +
    (gridfeeBalancing == null ? 0 : gridfeeBalancing.hashCode);

  @override
  String toString() => 'GridFeeBalanceTotalDGridfeeBalanceTotal[gridfeeCurr=$gridfeeCurr, gridfeeBalancing=$gridfeeBalancing]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (gridfeeCurr != null) {
      json[r'gridfeeCurr'] = gridfeeCurr;
    }
    if (gridfeeBalancing != null) {
      json[r'gridfeeBalancing'] = gridfeeBalancing;
    }
    return json;
  }

  /// Returns a new [GridFeeBalanceTotalDGridfeeBalanceTotal] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static GridFeeBalanceTotalDGridfeeBalanceTotal? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();
      return GridFeeBalanceTotalDGridfeeBalanceTotal(
        gridfeeCurr: mapValueOfType<String>(json, r'gridfeeCurr'),
        gridfeeBalancing: mapValueOfType<String>(json, r'gridfeeBalancing'),
      );
    }
    return null;
  }

  static List<GridFeeBalanceTotalDGridfeeBalanceTotal?>? listFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) =>
    json is List && json.isNotEmpty
      ? json.map(GridFeeBalanceTotalDGridfeeBalanceTotal.fromJson).toList(growable: true == growable)
      : true == emptyIsNull ? null : <GridFeeBalanceTotalDGridfeeBalanceTotal>[];

  static Map<String, GridFeeBalanceTotalDGridfeeBalanceTotal?> mapFromJson(dynamic json) {
    final map = <String, GridFeeBalanceTotalDGridfeeBalanceTotal?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) => map[key] = GridFeeBalanceTotalDGridfeeBalanceTotal.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of GridFeeBalanceTotalDGridfeeBalanceTotal-objects as value to a dart map
  static Map<String, List<GridFeeBalanceTotalDGridfeeBalanceTotal?>?> mapListFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<GridFeeBalanceTotalDGridfeeBalanceTotal?>?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) {
          map[key] = GridFeeBalanceTotalDGridfeeBalanceTotal.listFromJson(
            value,
            emptyIsNull: emptyIsNull,
            growable: growable,
          );
        });
    }
    return map;
  }
}

