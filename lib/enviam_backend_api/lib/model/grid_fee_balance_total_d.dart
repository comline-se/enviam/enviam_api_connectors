//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class GridFeeBalanceTotalD {
  /// Returns a new [GridFeeBalanceTotalD] instance.
  GridFeeBalanceTotalD({
    this.gridfeeBalanceTotal,
  });

  GridFeeBalanceTotalDGridfeeBalanceTotal? gridfeeBalanceTotal;

  @override
  bool operator ==(Object other) => identical(this, other) || other is GridFeeBalanceTotalD &&
     other.gridfeeBalanceTotal == gridfeeBalanceTotal;

  @override
  int get hashCode =>
  // ignore: unnecessary_parenthesis
    (gridfeeBalanceTotal == null ? 0 : gridfeeBalanceTotal.hashCode);

  @override
  String toString() => 'GridFeeBalanceTotalD[gridfeeBalanceTotal=$gridfeeBalanceTotal]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (gridfeeBalanceTotal != null) {
      json[r'gridfeeBalanceTotal'] = gridfeeBalanceTotal;
    }
    return json;
  }

  /// Returns a new [GridFeeBalanceTotalD] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static GridFeeBalanceTotalD? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();
      return GridFeeBalanceTotalD(
        gridfeeBalanceTotal: GridFeeBalanceTotalDGridfeeBalanceTotal.fromJson(json[r'gridfeeBalanceTotal']),
      );
    }
    return null;
  }

  static List<GridFeeBalanceTotalD?>? listFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) =>
    json is List && json.isNotEmpty
      ? json.map(GridFeeBalanceTotalD.fromJson).toList(growable: true == growable)
      : true == emptyIsNull ? null : <GridFeeBalanceTotalD>[];

  static Map<String, GridFeeBalanceTotalD?> mapFromJson(dynamic json) {
    final map = <String, GridFeeBalanceTotalD?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) => map[key] = GridFeeBalanceTotalD.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of GridFeeBalanceTotalD-objects as value to a dart map
  static Map<String, List<GridFeeBalanceTotalD?>?> mapListFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<GridFeeBalanceTotalD?>?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) {
          map[key] = GridFeeBalanceTotalD.listFromJson(
            value,
            emptyIsNull: emptyIsNull,
            growable: growable,
          );
        });
    }
    return map;
  }
}

