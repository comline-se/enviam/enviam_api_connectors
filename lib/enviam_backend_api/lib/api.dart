//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

library openapi.api;

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';

part 'api_client.dart';
part 'api_helper.dart';
part 'api_exception.dart';
part 'auth/authentication.dart';
part 'auth/api_key_auth.dart';
part 'auth/oauth.dart';
part 'auth/http_basic_auth.dart';
part 'auth/http_bearer_auth.dart';

part 'api/account_info_api.dart';
part 'api/environmental_balance_api.dart';
part 'api/savings_info_api.dart';

part 'model/account_infos.dart';
part 'model/account_infos_d.dart';
part 'model/account_infos_entry.dart';
part 'model/environmental_points_balance.dart';
part 'model/environmental_points_balance_d.dart';
part 'model/environmental_points_balance_d_environmental_points_balance.dart';
part 'model/grid_fee_balance_total.dart';
part 'model/grid_fee_balance_total_d.dart';
part 'model/grid_fee_balance_total_d_gridfee_balance_total.dart';
part 'model/latest_savings.dart';
part 'model/latest_savings_d.dart';
part 'model/latest_savings_entry.dart';
part 'model/monthly_savings.dart';
part 'model/monthly_savings_d.dart';
part 'model/monthly_savings_entry.dart';
part 'model/top_up_carb_account.dart';
part 'model/top_up_carb_account_d.dart';
part 'model/top_up_carb_account_d_top_up_carb_account.dart';


const _delimiters = {'csv': ',', 'ssv': ' ', 'tsv': '\t', 'pipes': '|'};
const _dateEpochMarker = 'epoch';
final _dateFormatter = DateFormat('yyyy-MM-dd');
final _regList = RegExp(r'^List<(.*)>$');
final _regSet = RegExp(r'^Set<(.*)>$');
final _regMap = RegExp(r'^Map<String,(.*)>$');

ApiClient defaultApiClient = ApiClient();
