//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

import 'package:enviam_backend_api/api.dart';
import 'package:test/test.dart';


/// tests for SavingsInfoApi
void main() {
  final instance = SavingsInfoApi();

  group('tests for SavingsInfoApi', () {
    // get latest savings
    //
    //Future<LatestSavings> getLatestSavings(String encAccountId) async
    test('test getLatestSavings', () async {
      // TODO
    });

    // get monthly savings
    //
    //Future<MonthlySavings> getMonthlySavings(String encAccountId) async
    test('test getMonthlySavings', () async {
      // TODO
    });

    // get all control units for the given user
    //
    //Future<GridFeeBalanceTotal> getSavingsInfo(String encAccountId) async
    test('test getSavingsInfo', () async {
      // TODO
    });

  });
}
