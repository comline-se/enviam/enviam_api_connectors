# incentives_api.model.IncentiveSeries

## Load the model package
```dart
import 'package:incentives_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Eindeutige ID eines Anreizes | [optional] 
**name** | **String** | Name des Anreizes | [optional] 
**type** | **String** | Type of the incentive. Valid vaues are:  - Bonus: Der Anreiz ist ein Bonus. Das bedeutet je höher desto besser.  - Price: Der Anreiz ist ein Preis. Das bedeutet je niedriger desto besser.  - Percentage: Der Anreiz wird angeboten als Prozentwert 0 bis 100%.  - Other: Der Anreiz ist eine Ziffer ohne vordefinierte Semantik. | [optional] 
**duration** | **int** | Dauer des genutzten Intervalls in Sekunden. Dieser Wert ist optional. Wenn nicht vorhanden, wird der Standardwert 900s benutzt. | [optional] 
**unit** | **String** | Einheit des Anreizwertes. | [optional] 
**meta** | [**IncentiveMetaProperties**](IncentiveMetaProperties.md) |  | [optional] 
**items** | [**List<IncentiveItem>**](IncentiveItem.md) | Elemente der Anreizserie | [optional] [default to const []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


