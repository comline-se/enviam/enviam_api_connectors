# incentives_api.api.IncentivesApi

## Load the API package
```dart
import 'package:incentives_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAllIncentives**](IncentivesApi.md#getallincentives) | **GET** /incentive | 
[**getIncentives**](IncentivesApi.md#getincentives) | **GET** /incentive/{incentiveID}/{fromTS}/{toTS} | 
[**getIncentivesForToday**](IncentivesApi.md#getincentivesfortoday) | **GET** /incentive/{incentiveID} | 


# **getAllIncentives**
> List<IncentiveMetaData> getAllIncentives()



Gibt Metadaten für alle verfügbaren Anreize zurück.

### Example
```dart
import 'package:incentives_api/api.dart';

final api_instance = IncentivesApi();

try {
    final result = api_instance.getAllIncentives();
    print(result);
} catch (e) {
    print('Exception when calling IncentivesApi->getAllIncentives: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List<IncentiveMetaData>**](IncentiveMetaData.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getIncentives**
> IncentiveSeries getIncentives(incentiveID, fromTS, toTS)



Gibt den Anreiz für eine Zeitspanne zurück identifiziert durch dessen ID.

### Example
```dart
import 'package:incentives_api/api.dart';

final api_instance = IncentivesApi();
final incentiveID = incentiveID_example; // String | ID eines aktuellen Anreizes
final fromTS = 789; // int | Startzeitpunkt des Anreizes
final toTS = 789; // int | Endzeitpunkt des Anreizes

try {
    final result = api_instance.getIncentives(incentiveID, fromTS, toTS);
    print(result);
} catch (e) {
    print('Exception when calling IncentivesApi->getIncentives: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **incentiveID** | **String**| ID eines aktuellen Anreizes | 
 **fromTS** | **int**| Startzeitpunkt des Anreizes | 
 **toTS** | **int**| Endzeitpunkt des Anreizes | 

### Return type

[**IncentiveSeries**](IncentiveSeries.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getIncentivesForToday**
> IncentiveSeries getIncentivesForToday(incentiveID)



Gibt den am heutigen Tage gültigen Anreiz zurück, identifiziert durch dessen ID.

### Example
```dart
import 'package:incentives_api/api.dart';

final api_instance = IncentivesApi();
final incentiveID = incentiveID_example; // String | ID eines aktiven Anreizes

try {
    final result = api_instance.getIncentivesForToday(incentiveID);
    print(result);
} catch (e) {
    print('Exception when calling IncentivesApi->getIncentivesForToday: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **incentiveID** | **String**| ID eines aktiven Anreizes | 

### Return type

[**IncentiveSeries**](IncentiveSeries.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

