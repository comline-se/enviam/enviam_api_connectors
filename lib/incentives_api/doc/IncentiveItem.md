# incentives_api.model.IncentiveItem

## Load the model package
```dart
import 'package:incentives_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startTime** | [**DateTime**](DateTime.md) | Startzeitpunkt der Gültigkeit des Anreizes. | 
**value** | **num** | Wert des Anreizes. | 
**score** | **num** | Bewertung des Anreiz Wertes. Dies ist ein Wert zwischen -1 und +1 wobei +1 die höchste und -1 die niedrigste Bewertung ist. | [optional] 
**zone** | **int** | Zonenanzeige (z.B. für Anzeigezwecke). Valid values are:  - +1: 'Gut' Wert, zb. kann in grüner Farbe gezeigt werden  - ±0: 'Durchschnitt' Wert, zb. kann in gelber Farbe gezeigt werden  - -1: 'Schlecht' value, Wert, zb. kann in roter Farbe gezeigt werden | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


