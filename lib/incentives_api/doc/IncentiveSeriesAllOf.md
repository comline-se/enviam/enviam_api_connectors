# incentives_api.model.IncentiveSeriesAllOf

## Load the model package
```dart
import 'package:incentives_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List<IncentiveItem>**](IncentiveItem.md) | Elemente der Anreizserie | [optional] [default to const []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


