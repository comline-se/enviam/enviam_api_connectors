# incentives_api.model.IncentiveMetaProperties

## Load the model package
```dart
import 'package:incentives_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**valueMinimum** | **num** | Minimaler Wert auf der y-Achse. Normalerweise wird ein Wert von -1 (oder 1, wenn der Anreizwert nachteilig wirken soll) angenommen.  | [optional] 
**valueMaximum** | **num** | Maximaler Wert auf der y-Achse. Normalerweise wird ein Wert von 1 (oder -1, wenn der Anreizwert nachteilig wirken soll) angenommen.  | [optional] 
**valueNeutral** | **num** | Neutraler Wert, wenn ein solcher Wert existiert. In der Regel wird solch einen Wert mit 0 bewertet.  | [optional] 
**valueNegative** | **num** | Untere Grenze des neutralen Bereichs, d.h. der Übergang zwischen neutral und minimal.   In der Regel wird ein Wert von -1/3 (oder 1/3, wenn der Anreizwert nachteilig wirken soll) angenommen.  | [optional] 
**valuePositive** | **num** | Obere Grenze des neutralen Bereichs, d.h. des Übergangs zwischen Neutral und Maximum. In der Regel wird ein Wert von 1/3 (oder -1/3, wenn der Anreizwert nachteilig wirken soll) zugrunde gelegt. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


