//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class IncentivesApi {
  IncentivesApi([ApiClient? apiClient]) : apiClient = apiClient ?? defaultApiClient;

  final ApiClient apiClient;

  /// Gibt Metadaten für alle verfügbaren Anreize zurück.
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> getAllIncentivesWithHttpInfo() async {
    // ignore: prefer_const_declarations
    final path = r'/incentive';

    // ignore: prefer_final_locals
    Object? postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    const authNames = <String>[];
    const contentTypes = <String>[];


    return apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes[0],
      authNames,
    );
  }

  /// Gibt Metadaten für alle verfügbaren Anreize zurück.
  Future<List<IncentiveMetaData>> getAllIncentives() async {
    final response = await getAllIncentivesWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, (await _decodeBodyBytes(response))!);
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      final responseBody = await _decodeBodyBytes(response);
      return (await apiClient.deserializeAsync(responseBody!, 'List<IncentiveMetaData>') as List)
        .cast<IncentiveMetaData>()
        .toList(growable: false);

    }
    return Future<List<IncentiveMetaData>>.value();
  }

  /// Gibt den Anreiz für eine Zeitspanne zurück identifiziert durch dessen ID.
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] incentiveID (required):
  ///   ID eines aktuellen Anreizes
  ///
  /// * [int] fromTS (required):
  ///   Startzeitpunkt des Anreizes
  ///
  /// * [int] toTS (required):
  ///   Endzeitpunkt des Anreizes
  Future<Response> getIncentivesWithHttpInfo(String incentiveID, int fromTS, int toTS,) async {
    // Verify required params are set.
    if (incentiveID == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: incentiveID');
    }
    if (fromTS == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: fromTS');
    }
    if (toTS == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: toTS');
    }

    // ignore: prefer_const_declarations
    final path = r'/incentive/{incentiveID}/{fromTS}/{toTS}'
      .replaceAll('{incentiveID}', incentiveID)
      .replaceAll('{fromTS}', fromTS.toString())
      .replaceAll('{toTS}', toTS.toString());

    // ignore: prefer_final_locals
    Object? postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    const authNames = <String>[];
    const contentTypes = <String>[];


    return apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes[0],
      authNames,
    );
  }

  /// Gibt den Anreiz für eine Zeitspanne zurück identifiziert durch dessen ID.
  ///
  /// Parameters:
  ///
  /// * [String] incentiveID (required):
  ///   ID eines aktuellen Anreizes
  ///
  /// * [int] fromTS (required):
  ///   Startzeitpunkt des Anreizes
  ///
  /// * [int] toTS (required):
  ///   Endzeitpunkt des Anreizes
  Future<IncentiveSeries> getIncentives(String incentiveID, int fromTS, int toTS,) async {
    final response = await getIncentivesWithHttpInfo(incentiveID, fromTS, toTS,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, (await _decodeBodyBytes(response))!);
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync((await _decodeBodyBytes(response))!, 'IncentiveSeries',) as IncentiveSeries;
    
    }
    return Future<IncentiveSeries>.value();
  }

  /// Gibt den am heutigen Tage gültigen Anreiz zurück, identifiziert durch dessen ID.
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] incentiveID (required):
  ///   ID eines aktiven Anreizes
  Future<Response> getIncentivesForTodayWithHttpInfo(String incentiveID,) async {
    // Verify required params are set.
    if (incentiveID == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: incentiveID');
    }

    // ignore: prefer_const_declarations
    final path = r'/incentive/{incentiveID}'
      .replaceAll('{incentiveID}', incentiveID);

    // ignore: prefer_final_locals
    Object? postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    const authNames = <String>[];
    const contentTypes = <String>[];


    return apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes[0],
      authNames,
    );
  }

  /// Gibt den am heutigen Tage gültigen Anreiz zurück, identifiziert durch dessen ID.
  ///
  /// Parameters:
  ///
  /// * [String] incentiveID (required):
  ///   ID eines aktiven Anreizes
  Future<IncentiveSeries> getIncentivesForToday(String incentiveID,) async {
    final response = await getIncentivesForTodayWithHttpInfo(incentiveID,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, (await _decodeBodyBytes(response))!);
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync((await _decodeBodyBytes(response))!, 'IncentiveSeries',) as IncentiveSeries;
    
    }
    return Future<IncentiveSeries>.value();
  }
}
