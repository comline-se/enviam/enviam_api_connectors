//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class IncentiveItem {
  /// Returns a new [IncentiveItem] instance.
  IncentiveItem({
    this.startTime,
    this.value,
    this.score,
    this.zone,
  });

  /// Startzeitpunkt der Gültigkeit des Anreizes.
  DateTime? startTime;

  /// Wert des Anreizes.
  num? value;

  /// Bewertung des Anreiz Wertes. Dies ist ein Wert zwischen -1 und +1 wobei +1 die höchste und -1 die niedrigste Bewertung ist.
  num? score;

  /// Zonenanzeige (z.B. für Anzeigezwecke). Valid values are:  - +1: 'Gut' Wert, zb. kann in grüner Farbe gezeigt werden  - ±0: 'Durchschnitt' Wert, zb. kann in gelber Farbe gezeigt werden  - -1: 'Schlecht' value, Wert, zb. kann in roter Farbe gezeigt werden
  // minimum: -1
  // maximum: 1
  int? zone;

  @override
  bool operator ==(Object other) => identical(this, other) || other is IncentiveItem &&
     other.startTime == startTime &&
     other.value == value &&
     other.score == score &&
     other.zone == zone;

  @override
  int get hashCode =>
  // ignore: unnecessary_parenthesis
    (startTime == null ? 0 : startTime.hashCode) +
    (value == null ? 0 : value.hashCode) +
    (score == null ? 0 : score.hashCode) +
    (zone == null ? 0 : zone.hashCode);

  @override
  String toString() => 'IncentiveItem[startTime=$startTime, value=$value, score=$score, zone=$zone]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'startTime'] = startTime!.toUtc().toIso8601String();
      json[r'value'] = value;
    if (score != null) {
      json[r'score'] = score;
    }
    if (zone != null) {
      json[r'zone'] = zone;
    }
    return json;
  }

  /// Returns a new [IncentiveItem] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static IncentiveItem? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();
      return IncentiveItem(
        startTime: mapDateTime(json, r'startTime', ''),
        value: json[r'value'] == null
          ? null
          : num.parse(json[r'value'].toString()),
        score: json[r'score'] == null
          ? null
          : num.parse(json[r'score'].toString()),
        zone: mapValueOfType<int>(json, r'zone'),
      );
    }
    return null;
  }

  static List<IncentiveItem?>? listFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) =>
    json is List && json.isNotEmpty
      ? json.map(IncentiveItem.fromJson).toList(growable: true == growable)
      : true == emptyIsNull ? null : <IncentiveItem>[];

  static Map<String, IncentiveItem?> mapFromJson(dynamic json) {
    final map = <String, IncentiveItem?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) => map[key] = IncentiveItem.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of IncentiveItem-objects as value to a dart map
  static Map<String, List<IncentiveItem?>?> mapListFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<IncentiveItem?>?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) {
          map[key] = IncentiveItem.listFromJson(
            value,
            emptyIsNull: emptyIsNull,
            growable: growable,
          );
        });
    }
    return map;
  }
}

