//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class IncentiveMetaProperties {
  /// Returns a new [IncentiveMetaProperties] instance.
  IncentiveMetaProperties({
    this.valueMinimum,
    this.valueMaximum,
    this.valueNeutral,
    this.valueNegative,
    this.valuePositive,
  });

  /// Minimaler Wert auf der y-Achse. Normalerweise wird ein Wert von -1 (oder 1, wenn der Anreizwert nachteilig wirken soll) angenommen. 
  num? valueMinimum;

  /// Maximaler Wert auf der y-Achse. Normalerweise wird ein Wert von 1 (oder -1, wenn der Anreizwert nachteilig wirken soll) angenommen. 
  num? valueMaximum;

  /// Neutraler Wert, wenn ein solcher Wert existiert. In der Regel wird solch einen Wert mit 0 bewertet. 
  num? valueNeutral;

  /// Untere Grenze des neutralen Bereichs, d.h. der Übergang zwischen neutral und minimal.   In der Regel wird ein Wert von -1/3 (oder 1/3, wenn der Anreizwert nachteilig wirken soll) angenommen. 
  num? valueNegative;

  /// Obere Grenze des neutralen Bereichs, d.h. des Übergangs zwischen Neutral und Maximum. In der Regel wird ein Wert von 1/3 (oder -1/3, wenn der Anreizwert nachteilig wirken soll) zugrunde gelegt.
  num? valuePositive;

  @override
  bool operator ==(Object other) => identical(this, other) || other is IncentiveMetaProperties &&
     other.valueMinimum == valueMinimum &&
     other.valueMaximum == valueMaximum &&
     other.valueNeutral == valueNeutral &&
     other.valueNegative == valueNegative &&
     other.valuePositive == valuePositive;

  @override
  int get hashCode =>
  // ignore: unnecessary_parenthesis
    (valueMinimum == null ? 0 : valueMinimum.hashCode) +
    (valueMaximum == null ? 0 : valueMaximum.hashCode) +
    (valueNeutral == null ? 0 : valueNeutral.hashCode) +
    (valueNegative == null ? 0 : valueNegative.hashCode) +
    (valuePositive == null ? 0 : valuePositive.hashCode);

  @override
  String toString() => 'IncentiveMetaProperties[valueMinimum=$valueMinimum, valueMaximum=$valueMaximum, valueNeutral=$valueNeutral, valueNegative=$valueNegative, valuePositive=$valuePositive]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (valueMinimum != null) {
      json[r'value_minimum'] = valueMinimum;
    }
    if (valueMaximum != null) {
      json[r'value_maximum'] = valueMaximum;
    }
    if (valueNeutral != null) {
      json[r'value_neutral'] = valueNeutral;
    }
    if (valueNegative != null) {
      json[r'value_negative'] = valueNegative;
    }
    if (valuePositive != null) {
      json[r'value_positive'] = valuePositive;
    }
    return json;
  }

  /// Returns a new [IncentiveMetaProperties] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static IncentiveMetaProperties? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();
      return IncentiveMetaProperties(
        valueMinimum: json[r'value_minimum'] == null
          ? null
          : num.parse(json[r'value_minimum'].toString()),
        valueMaximum: json[r'value_maximum'] == null
          ? null
          : num.parse(json[r'value_maximum'].toString()),
        valueNeutral: json[r'value_neutral'] == null
          ? null
          : num.parse(json[r'value_neutral'].toString()),
        valueNegative: json[r'value_negative'] == null
          ? null
          : num.parse(json[r'value_negative'].toString()),
        valuePositive: json[r'value_positive'] == null
          ? null
          : num.parse(json[r'value_positive'].toString()),
      );
    }
    return null;
  }

  static List<IncentiveMetaProperties?>? listFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) =>
    json is List && json.isNotEmpty
      ? json.map(IncentiveMetaProperties.fromJson).toList(growable: true == growable)
      : true == emptyIsNull ? null : <IncentiveMetaProperties>[];

  static Map<String, IncentiveMetaProperties?> mapFromJson(dynamic json) {
    final map = <String, IncentiveMetaProperties?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) => map[key] = IncentiveMetaProperties.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of IncentiveMetaProperties-objects as value to a dart map
  static Map<String, List<IncentiveMetaProperties?>?> mapListFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<IncentiveMetaProperties?>?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) {
          map[key] = IncentiveMetaProperties.listFromJson(
            value,
            emptyIsNull: emptyIsNull,
            growable: growable,
          );
        });
    }
    return map;
  }
}

