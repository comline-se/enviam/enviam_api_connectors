//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class IncentiveSeriesAllOf {
  /// Returns a new [IncentiveSeriesAllOf] instance.
  IncentiveSeriesAllOf({
    this.items = const [],
  });

  /// Elemente der Anreizserie
  List<IncentiveItem?>? items;

  @override
  bool operator ==(Object other) => identical(this, other) || other is IncentiveSeriesAllOf &&
     other.items == items;

  @override
  int get hashCode =>
  // ignore: unnecessary_parenthesis
    (items == null ? 0 : items.hashCode);

  @override
  String toString() => 'IncentiveSeriesAllOf[items=$items]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (items != null) {
      json[r'items'] = items;
    }
    return json;
  }

  /// Returns a new [IncentiveSeriesAllOf] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static IncentiveSeriesAllOf? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();
      return IncentiveSeriesAllOf(
        items: IncentiveItem.listFromJson(json[r'items']),
      );
    }
    return null;
  }

  static List<IncentiveSeriesAllOf?>? listFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) =>
    json is List && json.isNotEmpty
      ? json.map(IncentiveSeriesAllOf.fromJson).toList(growable: true == growable)
      : true == emptyIsNull ? null : <IncentiveSeriesAllOf>[];

  static Map<String, IncentiveSeriesAllOf?> mapFromJson(dynamic json) {
    final map = <String, IncentiveSeriesAllOf?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) => map[key] = IncentiveSeriesAllOf.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of IncentiveSeriesAllOf-objects as value to a dart map
  static Map<String, List<IncentiveSeriesAllOf?>?> mapListFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<IncentiveSeriesAllOf?>?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) {
          map[key] = IncentiveSeriesAllOf.listFromJson(
            value,
            emptyIsNull: emptyIsNull,
            growable: growable,
          );
        });
    }
    return map;
  }
}

