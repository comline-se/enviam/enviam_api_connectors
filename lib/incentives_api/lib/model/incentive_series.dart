//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class IncentiveSeries {
  /// Returns a new [IncentiveSeries] instance.
  IncentiveSeries({
    this.id,
    this.name,
    this.type,
    this.duration,
    this.unit,
    this.meta,
    this.items = const [],
  });

  /// Eindeutige ID eines Anreizes
  String? id;

  /// Name des Anreizes
  String? name;

  /// Type of the incentive. Valid vaues are:  - Bonus: Der Anreiz ist ein Bonus. Das bedeutet je höher desto besser.  - Price: Der Anreiz ist ein Preis. Das bedeutet je niedriger desto besser.  - Percentage: Der Anreiz wird angeboten als Prozentwert 0 bis 100%.  - Other: Der Anreiz ist eine Ziffer ohne vordefinierte Semantik.
  IncentiveSeriesTypeEnum? type;

  /// Dauer des genutzten Intervalls in Sekunden. Dieser Wert ist optional. Wenn nicht vorhanden, wird der Standardwert 900s benutzt.
  int? duration;

  /// Einheit des Anreizwertes.
  String? unit;

  IncentiveMetaProperties? meta;

  /// Elemente der Anreizserie
  List<IncentiveItem?>? items;

  @override
  bool operator ==(Object other) => identical(this, other) || other is IncentiveSeries &&
     other.id == id &&
     other.name == name &&
     other.type == type &&
     other.duration == duration &&
     other.unit == unit &&
     other.meta == meta &&
     other.items == items;

  @override
  int get hashCode =>
  // ignore: unnecessary_parenthesis
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (type == null ? 0 : type.hashCode) +
    (duration == null ? 0 : duration.hashCode) +
    (unit == null ? 0 : unit.hashCode) +
    (meta == null ? 0 : meta.hashCode) +
    (items == null ? 0 : items.hashCode);

  @override
  String toString() => 'IncentiveSeries[id=$id, name=$name, type=$type, duration=$duration, unit=$unit, meta=$meta, items=$items]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    if (type != null) {
      json[r'type'] = type;
    }
    if (duration != null) {
      json[r'duration'] = duration;
    }
    if (unit != null) {
      json[r'unit'] = unit;
    }
    if (meta != null) {
      json[r'meta'] = meta;
    }
    if (items != null) {
      json[r'items'] = items;
    }
    return json;
  }

  /// Returns a new [IncentiveSeries] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static IncentiveSeries? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();
      return IncentiveSeries(
        id: mapValueOfType<String>(json, r'id'),
        name: mapValueOfType<String>(json, r'name'),
        type: IncentiveSeriesTypeEnum.fromJson(json[r'type']),
        duration: mapValueOfType<int>(json, r'duration'),
        unit: mapValueOfType<String>(json, r'unit'),
        meta: IncentiveMetaProperties.fromJson(json[r'meta']),
        items: IncentiveItem.listFromJson(json[r'items']),
      );
    }
    return null;
  }

  static List<IncentiveSeries?>? listFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) =>
    json is List && json.isNotEmpty
      ? json.map(IncentiveSeries.fromJson).toList(growable: true == growable)
      : true == emptyIsNull ? null : <IncentiveSeries>[];

  static Map<String, IncentiveSeries?> mapFromJson(dynamic json) {
    final map = <String, IncentiveSeries?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) => map[key] = IncentiveSeries.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of IncentiveSeries-objects as value to a dart map
  static Map<String, List<IncentiveSeries?>?> mapListFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<IncentiveSeries?>?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) {
          map[key] = IncentiveSeries.listFromJson(
            value,
            emptyIsNull: emptyIsNull,
            growable: growable,
          );
        });
    }
    return map;
  }
}

/// Type of the incentive. Valid vaues are:  - Bonus: Der Anreiz ist ein Bonus. Das bedeutet je höher desto besser.  - Price: Der Anreiz ist ein Preis. Das bedeutet je niedriger desto besser.  - Percentage: Der Anreiz wird angeboten als Prozentwert 0 bis 100%.  - Other: Der Anreiz ist eine Ziffer ohne vordefinierte Semantik.
class IncentiveSeriesTypeEnum {
  /// Instantiate a new enum with the provided [value].
  const IncentiveSeriesTypeEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const bonus = IncentiveSeriesTypeEnum._(r'Bonus');
  static const price = IncentiveSeriesTypeEnum._(r'Price');
  static const percentage = IncentiveSeriesTypeEnum._(r'Percentage');
  static const other = IncentiveSeriesTypeEnum._(r'Other');

  /// List of all possible values in this [enum][IncentiveSeriesTypeEnum].
  static const values = <IncentiveSeriesTypeEnum>[
    bonus,
    price,
    percentage,
    other,
  ];

  static IncentiveSeriesTypeEnum? fromJson(dynamic value) =>
    IncentiveSeriesTypeEnumTypeTransformer().decode(value);

  static List<IncentiveSeriesTypeEnum?>? listFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) =>
    json is List && json.isNotEmpty
      ? json.map(IncentiveSeriesTypeEnum.fromJson).toList(growable: true == growable)
      : true == emptyIsNull ? null : <IncentiveSeriesTypeEnum>[];
}

/// Transformation class that can [encode] an instance of [IncentiveSeriesTypeEnum] to String,
/// and [decode] dynamic data back to [IncentiveSeriesTypeEnum].
class IncentiveSeriesTypeEnumTypeTransformer {
  factory IncentiveSeriesTypeEnumTypeTransformer() => _instance ??= const IncentiveSeriesTypeEnumTypeTransformer._();

  const IncentiveSeriesTypeEnumTypeTransformer._();

  String encode(IncentiveSeriesTypeEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a IncentiveSeriesTypeEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  IncentiveSeriesTypeEnum decode(dynamic data) {
      switch (data.toString()) {
        case r'Bonus': return IncentiveSeriesTypeEnum.bonus;
        case r'Price': return IncentiveSeriesTypeEnum.price;
        case r'Percentage': return IncentiveSeriesTypeEnum.percentage;
        case r'Other': return IncentiveSeriesTypeEnum.other;
        default: throw ArgumentError('Unknown enum value to decode: $data');
      }
  }

  /// Singleton [IncentiveSeriesTypeEnumTypeTransformer] instance.
  static IncentiveSeriesTypeEnumTypeTransformer? _instance;
}


