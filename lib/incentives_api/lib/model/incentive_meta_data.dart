//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class IncentiveMetaData {
  /// Returns a new [IncentiveMetaData] instance.
  IncentiveMetaData({
    this.id,
    this.name,
    this.type,
    this.duration,
    this.unit,
    this.meta,
  });

  /// Eindeutige ID eines Anreizes
  String? id;

  /// Name des Anreizes
  String? name;

  /// Type of the incentive. Valid vaues are:  - Bonus: Der Anreiz ist ein Bonus. Das bedeutet je höher desto besser.  - Price: Der Anreiz ist ein Preis. Das bedeutet je niedriger desto besser.  - Percentage: Der Anreiz wird angeboten als Prozentwert 0 bis 100%.  - Other: Der Anreiz ist eine Ziffer ohne vordefinierte Semantik.
  IncentiveMetaDataTypeEnum? type;

  /// Dauer des genutzten Intervalls in Sekunden. Dieser Wert ist optional. Wenn nicht vorhanden, wird der Standardwert 900s benutzt.
  int? duration;

  /// Einheit des Anreizwertes.
  String? unit;

  IncentiveMetaProperties? meta;

  @override
  bool operator ==(Object other) => identical(this, other) || other is IncentiveMetaData &&
     other.id == id &&
     other.name == name &&
     other.type == type &&
     other.duration == duration &&
     other.unit == unit &&
     other.meta == meta;

  @override
  int get hashCode =>
  // ignore: unnecessary_parenthesis
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (type == null ? 0 : type.hashCode) +
    (duration == null ? 0 : duration.hashCode) +
    (unit == null ? 0 : unit.hashCode) +
    (meta == null ? 0 : meta.hashCode);

  @override
  String toString() => 'IncentiveMetaData[id=$id, name=$name, type=$type, duration=$duration, unit=$unit, meta=$meta]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    if (type != null) {
      json[r'type'] = type;
    }
    if (duration != null) {
      json[r'duration'] = duration;
    }
    if (unit != null) {
      json[r'unit'] = unit;
    }
    if (meta != null) {
      json[r'meta'] = meta;
    }
    return json;
  }

  /// Returns a new [IncentiveMetaData] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static IncentiveMetaData? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();
      return IncentiveMetaData(
        id: mapValueOfType<String>(json, r'id'),
        name: mapValueOfType<String>(json, r'name'),
        type: IncentiveMetaDataTypeEnum.fromJson(json[r'type']),
        duration: mapValueOfType<int>(json, r'duration'),
        unit: mapValueOfType<String>(json, r'unit'),
        meta: IncentiveMetaProperties.fromJson(json[r'meta']),
      );
    }
    return null;
  }

  static List<IncentiveMetaData?>? listFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) =>
    json is List && json.isNotEmpty
      ? json.map(IncentiveMetaData.fromJson).toList(growable: true == growable)
      : true == emptyIsNull ? null : <IncentiveMetaData>[];

  static Map<String, IncentiveMetaData?> mapFromJson(dynamic json) {
    final map = <String, IncentiveMetaData?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) => map[key] = IncentiveMetaData.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of IncentiveMetaData-objects as value to a dart map
  static Map<String, List<IncentiveMetaData?>?> mapListFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) {
    final map = <String, List<IncentiveMetaData?>?>{};
    if (json is Map && json.isNotEmpty) {
      json
        .cast<String, dynamic>()
        .forEach((key, dynamic value) {
          map[key] = IncentiveMetaData.listFromJson(
            value,
            emptyIsNull: emptyIsNull,
            growable: growable,
          );
        });
    }
    return map;
  }
}

/// Type of the incentive. Valid vaues are:  - Bonus: Der Anreiz ist ein Bonus. Das bedeutet je höher desto besser.  - Price: Der Anreiz ist ein Preis. Das bedeutet je niedriger desto besser.  - Percentage: Der Anreiz wird angeboten als Prozentwert 0 bis 100%.  - Other: Der Anreiz ist eine Ziffer ohne vordefinierte Semantik.
class IncentiveMetaDataTypeEnum {
  /// Instantiate a new enum with the provided [value].
  const IncentiveMetaDataTypeEnum._(this.value);

  /// The underlying value of this enum member.
  final String? value;

  @override
  String toString() => value ?? '';

  String? toJson() => value;

  static const bonus = IncentiveMetaDataTypeEnum._(r'Bonus');
  static const price = IncentiveMetaDataTypeEnum._(r'Price');
  static const percentage = IncentiveMetaDataTypeEnum._(r'Percentage');
  static const other = IncentiveMetaDataTypeEnum._(r'Other');

  /// List of all possible values in this [enum][IncentiveMetaDataTypeEnum].
  static const values = <IncentiveMetaDataTypeEnum>[
    bonus,
    price,
    percentage,
    other,
  ];

  static IncentiveMetaDataTypeEnum? fromJson(dynamic value) =>
    IncentiveMetaDataTypeEnumTypeTransformer().decode(value);

  static List<IncentiveMetaDataTypeEnum?>? listFromJson(dynamic json, {bool emptyIsNull = false, bool growable = false,}) =>
    json is List && json.isNotEmpty
      ? json.map(IncentiveMetaDataTypeEnum.fromJson).toList(growable: true == growable)
      : true == emptyIsNull ? null : <IncentiveMetaDataTypeEnum>[];
}

/// Transformation class that can [encode] an instance of [IncentiveMetaDataTypeEnum] to String,
/// and [decode] dynamic data back to [IncentiveMetaDataTypeEnum].
class IncentiveMetaDataTypeEnumTypeTransformer {
  factory IncentiveMetaDataTypeEnumTypeTransformer() => _instance ??= const IncentiveMetaDataTypeEnumTypeTransformer._();

  const IncentiveMetaDataTypeEnumTypeTransformer._();

  String? encode(IncentiveMetaDataTypeEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a IncentiveMetaDataTypeEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  IncentiveMetaDataTypeEnum? decode(dynamic data, {bool allowNull = false}) {
    if (data != null) {
      switch (data.toString()) {
        case r'Bonus': return IncentiveMetaDataTypeEnum.bonus;
        case r'Price': return IncentiveMetaDataTypeEnum.price;
        case r'Percentage': return IncentiveMetaDataTypeEnum.percentage;
        case r'Other': return IncentiveMetaDataTypeEnum.other;
        default:
          if (allowNull == false) {
            throw ArgumentError('Unknown enum value to decode: $data');
          }
      }
    }
    return null;
  }

  /// Singleton [IncentiveMetaDataTypeEnumTypeTransformer] instance.
  static IncentiveMetaDataTypeEnumTypeTransformer? _instance;
}


