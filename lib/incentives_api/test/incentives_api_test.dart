//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

import 'package:incentives_api/api.dart';
import 'package:test/test.dart';


/// tests for IncentivesApi
void main() {
  final instance = IncentivesApi(ApiClient(basePath: 'https://api.staging.koordinierungsfunktion-mitnetz.de'));

  group('tests for IncentivesApi', () {
    // Gibt Metadaten für alle verfügbaren Anreize zurück.
    //
    //Future<List<IncentiveMetaData>> getAllIncentives() async
    test('test getAllIncentives', () async {
      var apiClient = instance.apiClient;
      var authentication = apiClient.getAuthentication('BearerAuth') as HttpBearerAuth?;
      if (authentication != null) {
        authentication.accessToken = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJBWWpsQlgtc0p4MzQzZ25kTTJYTE1TSGh0QkJEQ3h2NmE3UTFNTWJaZ01FIn0.eyJleHAiOjE2Mzg1MzE4MjIsImlhdCI6MTYzODUzMDAyMiwianRpIjoiYmQ2NDU1NGQtYjNmYy00OTM1LThjY2MtMWVlMjdhN2Q5ZjdlIiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5zdGFnaW5nLmtpd2lncmlkLmNvbS9hdXRoL3JlYWxtcy9lbnZpYSIsInN1YiI6ImY6OWExNWM3OTEtNjU0MS00NmUzLWI2MjctOWMwZTc2ZGViN2I3OmNvbnBsZW1lbnQuZmxleHVtZXIxIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiaW50ZWxsaWdlbnQtdmVybmV0enQtYXBwIiwic2Vzc2lvbl9zdGF0ZSI6IjMzYzhhODdmLWQwMDMtNDRhNi1iYTg5LWYwYWVhNjVlODgxZCIsImFjciI6IjEiLCJzY29wZSI6Im9wZW5pZCIsInJlYWxtIjoiZW52aWEiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJjb25wbGVtZW50LmZsZXh1bWVyMSJ9.gt-zl6LqToQj_QyCxqZJdrzHindvyMJHmLPr3eEsZ9WFnHjsltnvzU9UsX9nWdhqvltPlU-knmAVOTNvpCcKqP1It4cS0KaqI0Fr8AE-c_FEG8X57dDsXMHHEnYzPj13lVoixSgWWWxHSX_iIbnWqp5WPX5VCvM6TkgMkpkYSgi5NMxLmq8S8K6lt2NeofLz0E8mPwkIoiHL8uom3zYmu5p6ecsOlaxZZ_tKE3kEAh80AQ-8sK56Sk39YUxvkcBii34ChKvcq0rs_6or0zgrH-KyJEhDT_JVB4Xxa31LwK-4WJvZvG6wUoyTPuuRikAFLDaGozfCykU54fHOfZg5NA";
      }
      apiClient.addDefaultHeader("x-apikey","bkOPCuQZCi56jBlntK2SdpplYsjzdrG4eAaiAptGtjWgfFfG");

      var incentives = await instance.getAllIncentives();

      print(incentives);
    });

    // Gibt den Anreiz für eine Zeitspanne zurück identifiziert durch dessen ID.
    //
    //Future<IncentiveSeries> getIncentives(String incentiveID, int fromTS, int toTS) async
    test('test getIncentives', () async {
      // TODO
    });

    // Gibt den am heutigen Tage gültigen Anreiz zurück, identifiziert durch dessen ID.
    //
    //Future<IncentiveSeries> getIncentivesForToday(String incentiveID) async
    test('test getIncentivesForToday', () async {
      // TODO
    });

  });
}
