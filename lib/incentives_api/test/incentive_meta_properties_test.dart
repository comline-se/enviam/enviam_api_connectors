//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

import 'package:incentives_api/api.dart';
import 'package:test/test.dart';

// tests for IncentiveMetaProperties
void main() {
  final instance = IncentiveMetaProperties();

  group('test IncentiveMetaProperties', () {
    // Minimaler Wert auf der y-Achse. Normalerweise wird ein Wert von -1 (oder 1, wenn der Anreizwert nachteilig wirken soll) angenommen. 
    // num valueMinimum
    test('to test the property `valueMinimum`', () async {
      // TODO
    });

    // Maximaler Wert auf der y-Achse. Normalerweise wird ein Wert von 1 (oder -1, wenn der Anreizwert nachteilig wirken soll) angenommen. 
    // num valueMaximum
    test('to test the property `valueMaximum`', () async {
      // TODO
    });

    // Neutraler Wert, wenn ein solcher Wert existiert. In der Regel wird solch einen Wert mit 0 bewertet. 
    // num valueNeutral
    test('to test the property `valueNeutral`', () async {
      // TODO
    });

    // Untere Grenze des neutralen Bereichs, d.h. der Übergang zwischen neutral und minimal.   In der Regel wird ein Wert von -1/3 (oder 1/3, wenn der Anreizwert nachteilig wirken soll) angenommen. 
    // num valueNegative
    test('to test the property `valueNegative`', () async {
      // TODO
    });

    // Obere Grenze des neutralen Bereichs, d.h. des Übergangs zwischen Neutral und Maximum. In der Regel wird ein Wert von 1/3 (oder -1/3, wenn der Anreizwert nachteilig wirken soll) zugrunde gelegt.
    // num valuePositive
    test('to test the property `valuePositive`', () async {
      // TODO
    });


  });

}
