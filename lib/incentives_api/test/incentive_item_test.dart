//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

import 'package:incentives_api/api.dart';
import 'package:test/test.dart';

// tests for IncentiveItem
void main() {
  final instance = IncentiveItem();

  group('test IncentiveItem', () {
    // Startzeitpunkt der Gültigkeit des Anreizes.
    // DateTime startTime
    test('to test the property `startTime`', () async {
      // TODO
    });

    // Wert des Anreizes.
    // num value
    test('to test the property `value`', () async {
      // TODO
    });

    // Bewertung des Anreiz Wertes. Dies ist ein Wert zwischen -1 und +1 wobei +1 die höchste und -1 die niedrigste Bewertung ist.
    // num score
    test('to test the property `score`', () async {
      // TODO
    });

    // Zonenanzeige (z.B. für Anzeigezwecke). Valid values are:  - +1: 'Gut' Wert, zb. kann in grüner Farbe gezeigt werden  - ±0: 'Durchschnitt' Wert, zb. kann in gelber Farbe gezeigt werden  - -1: 'Schlecht' value, Wert, zb. kann in roter Farbe gezeigt werden
    // int zone
    test('to test the property `zone`', () async {
      // TODO
    });


  });

}
