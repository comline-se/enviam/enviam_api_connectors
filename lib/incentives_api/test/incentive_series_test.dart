//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// 

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

import 'package:incentives_api/api.dart';
import 'package:test/test.dart';

// tests for IncentiveSeries
void main() {
  final instance = IncentiveSeries();

  group('test IncentiveSeries', () {
    // Eindeutige ID eines Anreizes
    // String id
    test('to test the property `id`', () async {
      // TODO
    });

    // Name des Anreizes
    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // Type of the incentive. Valid vaues are:  - Bonus: Der Anreiz ist ein Bonus. Das bedeutet je höher desto besser.  - Price: Der Anreiz ist ein Preis. Das bedeutet je niedriger desto besser.  - Percentage: Der Anreiz wird angeboten als Prozentwert 0 bis 100%.  - Other: Der Anreiz ist eine Ziffer ohne vordefinierte Semantik.
    // String type
    test('to test the property `type`', () async {
      // TODO
    });

    // Dauer des genutzten Intervalls in Sekunden. Dieser Wert ist optional. Wenn nicht vorhanden, wird der Standardwert 900s benutzt.
    // int duration
    test('to test the property `duration`', () async {
      // TODO
    });

    // Einheit des Anreizwertes.
    // String unit
    test('to test the property `unit`', () async {
      // TODO
    });

    // IncentiveMetaProperties meta
    test('to test the property `meta`', () async {
      // TODO
    });

    // Elemente der Anreizserie
    // List<IncentiveItem> items (default value: const [])
    test('to test the property `items`', () async {
      // TODO
    });


  });

}
